﻿<%@ WebHandler Language="C#" Class="rapor" %>

using System;
using System.Web;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Data.OleDb;
using System.IO;
using System.Data;
using DbClass;
using System.Linq;
using System.Text;
using OfficeOpenXml.Drawing;
using System.Web.UI.DataVisualization.Charting;
public class rapor : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{
    DbClassMysql db = new DbClassMysql();
    ExcelPackage pck;
    FileInfo file;
    string newFile;
    GetChart getChart = new GetChart();
    
    public void ProcessRequest (HttpContext context) {

        string sqlMeasureRange = "";
        
         
        Random rnd = new Random();
        string random = "";
        for (int i = 0; i < 10; i++)
        {
            random += rnd.Next(0, 10);
        }
        string sql;
        
        if (context.Request.QueryString["tur"]=="genel")
        {
            if (context.Session["activePrj"] == null)
            {
                context.Response.Redirect("Kapi/Default.aspx");
            }
            sql = "select prj.proje_adi,p.pin_id,p.sira,son.filter,son.day,h.device_type,h.original_file_name,son.elapsed_time,son.detector,son.leq,son.max,son.min,l.ln_10,l.ln_90,p.enlem,p.boylam,ha.temp,ha.ruzgar,ha.nem from tbl_sonuc son";
            sql += " right join tbl_projedetay p on son.proje_detay_id=p.id ";
            sql += " left join tbl_ln l on l.proje_detay_id=p.id ";
            sql += " left join tbl_proje prj on prj.id=p.proje_id";
            sql += " left join tbl_header h on h.proje_detay_id=p.id ";
            sql += " left join tbl_hava ha on ha.proje_detay_id=p.id ";
            sql += " where p.proje_id=" + context.Session["activePrj"] + " order by p.tarih asc";

            DataTable dtRapor = db.Fill(sql);

            newFile = HttpContext.Current.Server.MapPath("~/TempFiles/") + @"rapor_" + random + ".xlsx";

            //sablonu kopyala
            File.Copy(HttpContext.Current.Server.MapPath("~/sablon/") + @"rapor.xlsx", newFile);
            
            using (var fs = new FileStream(newFile, FileMode.Open))
            {
                pck = new ExcelPackage(fs);
                
                var ws = pck.Workbook.Worksheets[1];
                
                
                
                int sabit = 4;

                

                int satir = 0;

                for (int i = 0; i < dtRapor.Rows.Count; )
                {
                    ws.Cells[1, 2].Value = dtRapor.Rows[i]["proje_adi"].ToString(); // proje adı    

                    if (string.IsNullOrEmpty(dtRapor.Rows[i]["filter"].ToString()))
                    {
                        
                        i++;
                        continue;
                    }
                    else
                    {

                        ws.Cells[sabit + satir, 1].Value = dtRapor.Rows[i]["sira"].ToString();
                        ws.Cells[sabit + satir, 2].Value = dtRapor.Rows[i]["day"].ToString(); // day
                        ws.Cells[sabit + satir, 3].Value = dtRapor.Rows[i]["device_type"].ToString(); // cihaz_adi
                        ws.Cells[sabit + satir, 4].Value = dtRapor.Rows[i]["original_file_name"].ToString(); // cihaz_adi
                        ws.Cells[sabit + satir, 5].Value = dtRapor.Rows[i]["elapsed_time"].ToString(); // ölçüm süresi

                        ws.Cells[sabit + satir, 6].Value = dtRapor.Rows[i]["enlem"].ToString(); // detector
                        ws.Cells[sabit + satir, 7].Value = dtRapor.Rows[i]["boylam"].ToString(); // Aleq
                        ws.Cells[sabit + satir, 8].Value = dtRapor.Rows[i]["temp"].ToString() + " °C"; // Aleq
                        ws.Cells[sabit + satir, 9].Value = dtRapor.Rows[i]["nem"].ToString() + " mm/s"; // Aleq
                        ws.Cells[sabit + satir, 10].Value = dtRapor.Rows[i]["ruzgar"].ToString() + " %"; // Aleq
                        ws.Cells[sabit + satir, 11].Value = dtRapor.Rows[i]["detector"].ToString(); // Aleq

                        ws.Cells[sabit + satir, 12].Value = dtRapor.Rows[i]["leq"].ToString(); // Aleq
                        ws.Cells[sabit + satir, 13].Value = dtRapor.Rows[i]["max"].ToString(); // Aleq
                        ws.Cells[sabit + satir, 14].Value = dtRapor.Rows[i]["min"].ToString(); // Aleq
                        ws.Cells[sabit + satir, 15].Value = dtRapor.Rows[i]["ln_10"].ToString(); // Aleq
                        ws.Cells[sabit + satir, 16].Value = dtRapor.Rows[i]["ln_90"].ToString(); // Aleq

                        ws.Cells[sabit + satir, 17].Value = dtRapor.Rows[i + 1]["detector"].ToString(); // detector
                        ws.Cells[sabit + satir, 18].Value = dtRapor.Rows[i + 1]["leq"].ToString(); // Cleq
                        ws.Cells[sabit + satir, 19].Value = dtRapor.Rows[i + 1]["max"].ToString(); // Aleq
                        ws.Cells[sabit + satir, 20].Value = dtRapor.Rows[i + 1]["min"].ToString(); // Aleq
                        ws.Cells[sabit + satir, 21].Value = dtRapor.Rows[i + 1]["ln_10"].ToString(); // Aleq
                        ws.Cells[sabit + satir, 22].Value = dtRapor.Rows[i + 1]["ln_90"].ToString(); // Aleq
                        i += 3;
                        satir++;
                    }

                }
            }

            
        }
        else
        {

            string aralik = context.Request.QueryString["interval"];

            string[] gelenler;

            string aralikSozlu = "";
            
            int tekSayi;
            
           if (aralik.IndexOf(',')!=-1)
            {
                 gelenler= aralik.Split(',');
                 string aralik2Sozlu = "";
                
                 for (int i = 0; i < gelenler.Length; i++)
                 {
                     if (gelenler.Length-1>i)
                     {
                         aralik2Sozlu += gelenler[i] + ",";    
                     }
                     else
                     {
                         aralik2Sozlu += gelenler[i];    
                     }



                     aralikSozlu = " and sira in(" + aralik2Sozlu + ")";
                 } 
                 
            }
            else if (aralik.IndexOf('-')!=-1)
            {
                gelenler = aralik.Split('-');

                if (gelenler.Length <= 1)
                {

                    aralikSozlu = " and sira between " + gelenler[0] + " and " + gelenler[0];
                }
                else
                {
                    aralikSozlu = " and sira between " + gelenler[0] + " and " + gelenler[1];
                } 
            }
            else if (int.TryParse(aralik,out tekSayi))
            {
                aralikSozlu = " and sira in(" + tekSayi + ")";
            }
            else if (aralik=="")
            {
                aralikSozlu = " ";
            }
            else
            {
                context.Response.Redirect("Default.aspx");
            }

           if (context.Session["activePrj"]==null)
           {
               context.Response.Redirect("Default.aspx");
           }

            sql = "select * from tbl_slm son right join tbl_projedetay p on son.ProjeDetayID=p.id  OUTER APPLY ( SELECT  TOP 1 *  FROM    tbl_foto foto  WHERE   foto.proje_detay_id = p.id  ) foto  ";
            sql+=" left join tbl_proje prj on prj.id=p.proje_id ";
            sql+=" left join tbl_hava ha on ha.proje_detay_id=p.id";
            sql+=" left join tbl_cihaz ci on ci.ID=son.CihazID   ";
            sql+=" left join tbl_oktav_sonuc o_sonuc on o_sonuc.ProjeDetayID=son.ProjeDetayID";
            sql += " where p.proje_id=" + context.Session["activePrj"] + ""+aralikSozlu+" order by sira asc";


            DataTable dtRapor = db.Fill(sql);

            
            
            System.Data.SqlClient.SqlParameter prm1 = new System.Data.SqlClient.SqlParameter("@proje_detay_id", SqlDbType.Int);
            

            newFile = HttpContext.Current.Server.MapPath("~/TempFiles/") + @"rapor_" + random + ".xlsx";

            

            if (true)
            {
                File.Copy(HttpContext.Current.Server.MapPath("~/sablon/") + @"report_temp_single.xlsx", newFile);
            }
            

            using (var fs = new FileStream(newFile, FileMode.Open))
            {
                pck = new ExcelPackage(fs);

                var ws = pck.Workbook.Worksheets[1];

                for (int i = 0; i < dtRapor.Rows.Count; )
                {
                    prm1.Value = Convert.ToInt32(dtRapor.Rows[i]["ProjeDetayID"]);
                    sqlMeasureRange = "SELECT slm.profil, DATEADD(hh,DATEDIFF(hh,0,SonucDate),0) AS start_time, DATEADD(hh,DATEDIFF(hh,0,SonucDate),0) + '01:00:00' AS end_time, Round(AVG(slm.LEQ),2) AS average FROM tbl_slm_history  slm inner join tbl_projedetay as detay on slm.ProjeDetayID=detay.id where slm.ProjeDetayID=" + dtRapor.Rows[i]["ProjeDetayID"] + "  and slm.Profil in(1,3)  GROUP BY slm.Profil, DATEADD(hh,DATEDIFF(hh,0,SonucDate),0) ORDER BY start_time,slm.profil";
                    DataTable dtElapsedTime = db.sp_Fill("calc_elapsed_time", prm1);        

                    string enlem = dtRapor.Rows[i]["enlem"].ToString().Replace(",", ".");

                    string boylam = dtRapor.Rows[i]["boylam"].ToString().Replace(",", ".");

                    string color = "1dd3fa";

                        if (Convert.ToSingle(dtRapor.Rows[i]["LEQ"].ToString()) < 35)
                        {
                            color = "bfe3b4";
                        }
                        else if (Convert.ToSingle(dtRapor.Rows[i]["LEQ"].ToString()) < 40 && Convert.ToSingle(dtRapor.Rows[i]["LEQ"].ToString()) >= 35)
                        {

                            color = "4fa833";
                        }
                        else if (Convert.ToSingle(dtRapor.Rows[i]["LEQ"].ToString()) < 45 && Convert.ToSingle(dtRapor.Rows[i]["LEQ"].ToString()) >= 40)
                        {

                            color = "0f7033";
                        }
                        else if (Convert.ToSingle(dtRapor.Rows[i]["LEQ"].ToString()) < 50 && Convert.ToSingle(dtRapor.Rows[i]["LEQ"].ToString()) >= 45)
                        {

                            color = "fff53c";
                        }
                        else if (Convert.ToSingle(dtRapor.Rows[i]["LEQ"].ToString()) < 55 && Convert.ToSingle(dtRapor.Rows[i]["LEQ"].ToString()) >= 50)
                        {

                            color = "ad7a4f";
                        }
                        else if (Convert.ToSingle(dtRapor.Rows[i]["LEQ"].ToString()) < 60 && Convert.ToSingle(dtRapor.Rows[i]["LEQ"].ToString()) >= 55)
                        {

                            color = "ff6336";
                        }
                        else if (Convert.ToSingle(dtRapor.Rows[i]["LEQ"].ToString()) < 65 && Convert.ToSingle(dtRapor.Rows[i]["LEQ"].ToString()) >= 60)
                        {

                            color = "c71712";
                        }
                        else if (Convert.ToSingle(dtRapor.Rows[i]["LEQ"].ToString()) < 70 && Convert.ToSingle(dtRapor.Rows[i]["LEQ"].ToString()) >= 65)
                        {

                            color = "8a1214";
                        }

                        else if (Convert.ToSingle(dtRapor.Rows[i]["LEQ"].ToString()) < 75 && Convert.ToSingle(dtRapor.Rows[i]["LEQ"].ToString()) >= 70)
                        {

                            color = "910f66";
                        }
                        else if (Convert.ToSingle(dtRapor.Rows[i]["LEQ"].ToString()) < 80 && Convert.ToSingle(dtRapor.Rows[i]["LEQ"].ToString()) >= 75)
                        {

                            color = "2973b8";
                        }
                        else if (Convert.ToSingle(dtRapor.Rows[i]["LEQ"].ToString()) > 80)
                        {
                            color = "0a4278";
                        }
                        else
                        {

                            color = "1dd3fa";
                        }

                        System.Drawing.Color col = System.Drawing.ColorTranslator.FromHtml("#"+color);

 
                        string getUrl = "http://maps.googleapis.com/maps/api/staticmap?center=" + enlem + "," + boylam + "&zoom=" + dtRapor.Rows[i]["zoom"].ToString() + "&size=275x237&scale=2&maptype=hybrid&markers=color:|" + enlem + "," + boylam + "&sensor=false";

                        ws.Cells[2, 7].Value = dtRapor.Rows[i]["proje_adi"].ToString(); // proje adı    
                        
                        
                        ws.Workbook.Worksheets.Copy("Form", dtRapor.Rows[i]["sira"].ToString());

                        ws = pck.Workbook.Worksheets[dtRapor.Rows[i]["sira"].ToString()];

                        //FileInfo img = new FileInfo(HttpContext.Current.Server.MapPath("~/kapi/UploadFiles/" + dtRapor.Rows[i]["fotograf"].ToString() + ""));                      
                                                                        
                        byte[] ArrayIn = (byte[])imageToByteArray(getUrl);

                        string filePath = "";
                        
                        if (File.Exists(HttpContext.Current.Server.MapPath("~/kapi/UploadFiles/" + dtRapor.Rows[i]["fotograf"].ToString() + "")))
                        {
                            filePath=HttpContext.Current.Server.MapPath("~/kapi/UploadFiles/" + dtRapor.Rows[i]["fotograf"].ToString());
                        }
                        else
                        {
                        
                            filePath=HttpContext.Current.Server.MapPath("~/image/no_foto.png");
                        
                        }
                        
                        byte[] ArrayIn2 = (byte[])imageToByteArray(filePath);

                        ExcelPicture pic = ws.Drawings.AddPicture(Convert.ToString((i+3)*2), byteArrayToImage(ArrayIn));
                                                
                        // ExcelPicture pic = ws.Drawings.AddPicture("PictureUniqueName", img2);

                        ExcelPicture pic2 = ws.Drawings.AddPicture(Convert.ToString((i + 2) * 9999), byteArrayToImage(ArrayIn2));

                        ExcelPicture pic3 = ws.Drawings.AddPicture(Convert.ToString((i + 2582) * 9999), getChart.GetChartLogger(Convert.ToInt32(dtRapor.Rows[i]["ProjeDetayID"])));

                        ExcelPicture picOctav1_1 = ws.Drawings.AddPicture(Convert.ToString((i + 7415) * 9999), getChart.GetChart1_1Octav(Convert.ToInt32(dtRapor.Rows[i]["ProjeDetayID"])));

                        ExcelPicture picOctav1_3 = ws.Drawings.AddPicture(Convert.ToString((i + 4178) * 9999), getChart.GetChart1_3Octav(Convert.ToInt32(dtRapor.Rows[i]["ProjeDetayID"])));
                        
                        pic3.SetPosition(36, 1, 1, 1);

                        picOctav1_1.SetPosition(139, 1, 1, 1);

                        picOctav1_1.SetSize(551, 237);

                        picOctav1_3.SetPosition(115, 1, 1, 1);

                        picOctav1_3.SetSize(551, 237);
                        
                        pic3.SetSize(554, 317);
                        
                        pic.SetPosition(8, 1, 6, 2);

                        pic.SetSize(275, 237);

                        pic2.SetPosition(8, 1, 1, 1);

                        pic2.SetSize(275, 237);
                        DateTime dtTime = Convert.ToDateTime(db.ScalarQuery("select min(SonucDate) as tarih from tbl_slm_history son right join tbl_projedetay p on son.ProjeDetayID=p.id where p.proje_id=" + context.Session["activePrj"] + "" + aralikSozlu + ""));
                        ws.Cells[6, 5].Value = "olcum"+dtRapor.Rows[i]["olcum_yeri"].ToString();
                        ws.Cells[6, 10].Value = dtRapor.Rows[i]["sira"].ToString();
                        ws.Cells[7, 5].Value = dtRapor.Rows[i]["standart"].ToString();
                        ws.Cells[7, 10].Value = dtTime.ToString("dd/MM/yyyy");
                        ws.Cells[23, 6].Value = dtRapor.Rows[i]["Model"].ToString();
                        ws.Cells[23, 10].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["boylam"]),7).ToString().Replace(',','.') + " D.";
                        ws.Cells[24, 10].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["enlem"]), 7).ToString().Replace(',', '.') + " K.";
                        ws.Cells[24, 5].Value ="slmno"+ dtRapor.Rows[i]["SlmNo"].ToString();
                        ws.Cells[25, 10].Value = dtRapor.Rows[i]["k_mesafe"].ToString();
                        ws.Cells[25, 5].Value = dtRapor.Rows[i]["DosyaAdi"].ToString();
                        ws.Cells[26, 10].Value = dtRapor.Rows[i]["lokasyon"].ToString();
                        ws.Cells[26, 5].Value = dtTime.ToString("HH:mm:ss");

                        
                        
                        ws.Cells[27, 5].Value =dtElapsedTime.Rows[0]["elapsed_time"];
                        ws.Cells[27, 10].Value = dtRapor.Rows[i]["yukseklik"].ToString();
                        ws.Cells[30, 5].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["LEQ"]),2).ToString();
                        ws.Cells[31, 5].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["MaxValue"]), 2).ToString();
                        ws.Cells[32, 5].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["MinValue"]), 2).ToString();
                        ws.Cells[33, 5].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["Lof90"]), 2).ToString();
                        ws.Cells[34, 5].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["Lof10"]), 2).ToString();
                        if (dtRapor.Rows.Count==3)
                        {
                            ws.Cells[30, 10].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i + 2]["LEQ"]), 2).ToString();
                            ws.Cells[31, 10].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i + 2]["MaxValue"]), 2).ToString();
                            ws.Cells[32, 10].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i + 2]["MinValue"]), 2).ToString();
                            ws.Cells[33, 10].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i + 2]["Lof90"]), 2).ToString();
                            ws.Cells[34, 10].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i + 2]["Lof10"]), 2).ToString(); 
                        }

                        ws.Cells[129, 4].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz20"]), 2).ToString();
                        ws.Cells[130, 4].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz25"]), 2).ToString();
                        ws.Cells[131, 4].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz31Half"]), 2).ToString();
                        ws.Cells[132, 4].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz40"]), 2).ToString();
                        ws.Cells[133, 4].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz50"]), 2).ToString();
                        ws.Cells[134, 4].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz63"]), 2).ToString();
                        ws.Cells[135, 4].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz80"]), 2).ToString();
                        ws.Cells[136, 4].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz100"]), 2).ToString();
                        ws.Cells[137, 4].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz125"]), 2).ToString();
                        ws.Cells[129, 6].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz160"]), 2).ToString();
                        ws.Cells[130, 6].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz200"]), 2).ToString();
                        ws.Cells[131, 6].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz250"]), 2).ToString();
                        ws.Cells[132, 6].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz315"]), 2).ToString();
                        ws.Cells[133, 6].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz400"]), 2).ToString();
                        ws.Cells[134, 6].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz500"]), 2).ToString();
                        ws.Cells[135, 6].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz630"]), 2).ToString();
                        ws.Cells[136, 6].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz800"]), 2).ToString();
                        ws.Cells[137, 6].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz1000"]), 2).ToString();
                        ws.Cells[129, 9].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz1250"]), 2).ToString();
                        ws.Cells[130, 9].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz1600"]), 2).ToString();
                        ws.Cells[131, 9].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz2000"]), 2).ToString();
                        ws.Cells[132, 9].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz2500"]), 2).ToString();
                        ws.Cells[133, 9].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz3150"]), 2).ToString();
                        ws.Cells[134, 9].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz4000"]), 2).ToString();
                        ws.Cells[135, 9].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz5000"]), 2).ToString();
                        ws.Cells[136, 9].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz6300"]), 2).ToString();
                        ws.Cells[137, 9].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz8000"]), 2).ToString();
                        ws.Cells[129, 11].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz10000"]), 2).ToString();
                        ws.Cells[130, 11].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz12500"]), 2).ToString();
                        ws.Cells[131, 11].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz16000"]), 2).ToString();
                        ws.Cells[132, 11].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["DbHz20000"]), 2).ToString();
                        ws.Cells[135, 11].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["TotalA"]), 2).ToString();
                        ws.Cells[136, 11].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["TotalC"]), 2).ToString();
                        ws.Cells[137, 11].Value = Math.Round(Convert.ToSingle(dtRapor.Rows[i]["TotalZ"]), 2).ToString();

                                                
                        
                        ws.Cells[153, 4].Value =Math.Round((10*(Math.Log(Math.Pow(10, (Convert.ToDouble(dtRapor.Rows[i]["DbHz25"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtRapor.Rows[i]["DbHz31Half"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtRapor.Rows[i]["DbHz40"]) / 10)),10))),2);
                        ws.Cells[154, 4].Value = Math.Round((10 * (Math.Log(Math.Pow(10, (Convert.ToDouble(dtRapor.Rows[i]["DbHz50"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtRapor.Rows[i]["DbHz63"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtRapor.Rows[i]["DbHz80"]) / 10)),10))),2);
                        ws.Cells[155, 4].Value =Math.Round((10 * (Math.Log(Math.Pow(10, (Convert.ToDouble(dtRapor.Rows[i]["DbHz100"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtRapor.Rows[i]["DbHz125"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtRapor.Rows[i]["DbHz160"]) / 10)),10))),2);
                        ws.Cells[153, 6].Value = Math.Round((10 * (Math.Log(Math.Pow(10, (Convert.ToDouble(dtRapor.Rows[i]["DbHz200"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtRapor.Rows[i]["DbHz250"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtRapor.Rows[i]["DbHz315"]) / 10)),10))),2);
                        ws.Cells[154, 6].Value =Math.Round((10 * (Math.Log(Math.Pow(10, (Convert.ToDouble(dtRapor.Rows[i]["DbHz400"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtRapor.Rows[i]["DbHz500"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtRapor.Rows[i]["DbHz630"]) / 10)),10))),2);
                        ws.Cells[155, 6].Value =Math.Round((10 * (Math.Log(Math.Pow(10, (Convert.ToDouble(dtRapor.Rows[i]["DbHz800"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtRapor.Rows[i]["DbHz1000"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtRapor.Rows[i]["DbHz1250"]) / 10)),10))),2);
                        ws.Cells[153, 9].Value =Math.Round((10 * (Math.Log(Math.Pow(10, (Convert.ToDouble(dtRapor.Rows[i]["DbHz1600"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtRapor.Rows[i]["DbHz2000"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtRapor.Rows[i]["DbHz3150"]) / 10)),10))),2);
                        ws.Cells[154, 9].Value =Math.Round((10 * (Math.Log(Math.Pow(10, (Convert.ToDouble(dtRapor.Rows[i]["DbHz3150"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtRapor.Rows[i]["DbHz4000"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtRapor.Rows[i]["DbHz5000"]) / 10)),10))),2);
                        ws.Cells[155, 9].Value = Math.Round((10 * (Math.Log(Math.Pow(10, (Convert.ToDouble(dtRapor.Rows[i]["DbHz6300"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtRapor.Rows[i]["DbHz8000"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtRapor.Rows[i]["DbHz10000"]) / 10)), 10))), 2);
                        
                        
                        i += 3;
                        
                        
                    

                }

                DataTable dtHourly = db.Fill(sqlMeasureRange);
                int column = 63;
                for (int i = 0; i < dtHourly.Rows.Count; i++)
                {
                    if (column >= 75)
                    {
                        ws.Cells[column-12, 7].Value = dtHourly.Rows[i]["start_time"];
                        ws.Cells[column-12, 9].Value = dtHourly.Rows[i]["end_time"];
                        ws.Cells[column-12, 10].Value = dtHourly.Rows[i]["average"];
                        
                        if (dtHourly.Rows.Count > 2)
                        {
                            ws.Cells[column-12, 11].Value = dtHourly.Rows[i + 1]["average"];
                        }
                    }
                    else if(column==63||column<75) {

                        ws.Cells[column, 2].Value = dtHourly.Rows[i]["start_time"];
                        ws.Cells[column, 4].Value = dtHourly.Rows[i]["end_time"];
                        ws.Cells[column, 5].Value = dtHourly.Rows[i]["average"];

                        if (dtHourly.Rows.Count > 2)
                        {
                            ws.Cells[column, 6].Value = dtHourly.Rows[i + 1]["average"];
                        }
                    }
                    else if (column >= 87) {

                        ws.Cells[column-5, 2].Value = dtHourly.Rows[i]["start_time"];
                        ws.Cells[column-5, 4].Value = dtHourly.Rows[i]["end_time"];
                        ws.Cells[column-5, 5].Value = dtHourly.Rows[i]["average"];

                        if (dtHourly.Rows.Count > 2)
                        {
                            ws.Cells[column-5, 6].Value = dtHourly.Rows[i + 1]["average"];
                        }
                    
                    }
                    column++;
                    ++i;
                }
                
                ws = pck.Workbook.Worksheets["Form"];
                ws.Workbook.Worksheets.Delete("Form");
            }

        }
        
        
        
        file = new FileInfo(newFile);
        pck.SaveAs(context.Response.OutputStream);
        context.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        context.Response.AddHeader("content-disposition", "attachment;  filename=" + file.Name);
        context.Response.End();  
        
    }

    public static string hex2binary(string hexvalue)
    {
        // Convert.ToUInt32 this is an unsigned int
        // so no negative numbers but it gives you one more bit
        // it much of a muchness 
        // Uint MAX is 4,294,967,295 and MIN is 0
        // this padds to 4 bits so 0x5 = "0101"
        return String.Join(String.Empty, hexvalue.Select(c => Convert.ToString(Convert.ToUInt32(c.ToString(), 16), 2).PadLeft(4, '0')));
    }

    public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
    }

    public static string ByteArrayToString(byte[] ba)
    {
        System.Text.StringBuilder hex = new System.Text.StringBuilder(ba.Length * 2);
        foreach (byte b in ba)
            hex.AppendFormat("{0:x2}", b);
        return hex.ToString();
    }

    public string RandomUnique()
    {
        Random rnd = new Random();

        string sayi = "";

        for (int i = 0; i < 10; i++)
        {
            sayi += rnd.Next(1, 10);
        }
     
        return sayi;
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

    public string ConvertBytesToBase64(byte[] imageBytes)
    {
        return Convert.ToBase64String(imageBytes);
    }

    public System.Drawing.Image byteArrayToImage(byte[] byteArrayIn)
    {
        MemoryStream ms = new MemoryStream(byteArrayIn);
        System.Drawing.Image returnImage = System.Drawing.Image.FromStream(ms);
        return returnImage;
    }

    public static byte[] imageToByteArray(string sPath)
    {
        System.Net.WebClient client = new System.Net.WebClient();
        byte[] imageData = client.DownloadData(sPath);
        System.IO.MemoryStream stream = new System.IO.MemoryStream(imageData);
        System.Drawing.Image img = System.Drawing.Image.FromStream(stream);
        MemoryStream ms = new MemoryStream();
        img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
        return ms.ToArray();
    }

}