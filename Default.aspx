<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-9" />
    <meta HTTP-EQUIV="Content-language" CONTENT="tr" />
    <script src="js/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="js/General.js" type="text/javascript"></script>
    <link href="css/Main.css" rel="stylesheet" type="text/css" />
    <link href="css/styles.css" rel="stylesheet" type="text/css" />
    <script src="js/General.js" type="text/javascript"></script>
    <link href="_assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="_assets/css/gridAdmin.css" rel="stylesheet" type="text/css" />
    <link href="_assets/css/round.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery-ui-1.8rc3.custom.css" rel="stylesheet" type="text/css" />
    <script src="kapi/js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.blImageCenter.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=true&key=AIzaSyAdZUkU6PQfRwEUfVxgpFkaRjKRdnAGrlE"></script>
    <script src="kapi/js/StyledMarker.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/canvasjs.js"></script>
    
    
    
 <script type="text/javascript">
     window.onload = function () {

         CanvasJS.addColorSet("greenShades",
                [//colorSet Array

                "#C6D9F1"
                ]);

         CanvasJS.addCultureInfo("tr",
                {
                    decimalSeparator: ",",
                    digitGroupSeparator: ".",
                    days: ["P.tesi", "Sal�", "�ar�amba", "Per�embe", "Cuma", "C.tesi", "Pazar"],
                    shortMonths: ["Oca", "�ub", "Mar", "Nis", "May", "Haz", "Tem", "A�u", "Eyl", "Eki", "Kas", "Ara"],
                    months: ["Ocak", "�ubat", "Mart", "Nisan", "May�s", "Haziran", "Temmuz", "A�ustos", "Eyl�l", "Ekim", "Kas�m", "Aral�k"]
                });

         //var runButton = document.getElementById("test");
         //runButton.addEventListener("click", makeChart);         

     }

     $(function () {
         
         

         $("#dialog").dialog({
             autoOpen: false,
             modal: true,
             
             position: [(($(window).width() / 2)-100) - (150), ($(window).height()/2)-100],
             show: {
                 effect: "fade",
                 duration: 100
             },
             hide: {
                 effect: "explode",
                 duration: 1000
             }
         });

         $("#getChartImage").click(function () {

             //var canvas = $("div[id^='Chart_']").find("canvas");
                                       
            // getChartImg();

             

             /*$.ajax({

                 type: 'POST',

                 url: 'Default.aspx/UploadImage',

                 data: '{ "imageData" : "' + image + '" }',

                 contentType: 'application/json; charset=utf-8',

                 dataType: 'json',

                 success: function (msg) {

                     alert('Image saved successfully !');

                 }

             });*/

         });

         var sayi = 0;

         $(".detail tr").click(function () {

             var selected = $(this).hasClass("highlight");

             $(".detail tr").removeClass("highlight");

             if (!selected)
                 $(this).addClass("highlight");
         });


         $("#detail_visible").click(function () {

             //alert(.

             if (sayi == 0) {

                 $("div#sol").css("width", "65%");

                 $("div#sag").css("width", "35%");

                 $(".detail th:nth-child(n+1):not(:nth-child(1)):not(:nth-child(2))").show();

                 $(".detail td:nth-child(n+1):not(:nth-child(1)):not(:nth-child(2))").show();

                 $("div#pnlPin").css("width", "80%");

                 $("div#pinlegand").css("width", "20%");

                 $(this).val("Detay Gizle");

                 sayi = 1;

             } else {

                 sayi = 0;

                 $("div#sol").css("width", "90%");

                 $("div#sag").css("width", "10%");

                 $(".detail th:nth-child(n+1):not(:nth-child(1)):not(:nth-child(2))").hide();

                 $(".detail td:nth-child(n+1):not(:nth-child(1)):not(:nth-child(2))").hide();

                 $("div#pnlPin").css("width", "55%");

                 $("div#pinlegand").css("width", "45%");

                 $(this).val("Detay G�ster");


                 
             }
             

         });
 
     });


     

  </script>

    <%--<script type="text/javascript">
        window.onload = function () {

            var dps = [{ x: 1, y: 10 }, { x: 2, y: 13 }, { x: 3, y: 18 }, { x: 4, y: 20 }, { x: 5, y: 17 }, { x: 6, y: 10 }, { x: 7, y: 13 }, { x: 8, y: 18 }, { x: 9, y: 20 }, { x: 10, y: 17 }];   //dataPoints. 

            var chart = new CanvasJS.Chart("chartContainer", {
                title: {
                    text: "Live Data"
                },
                axisX: {
                    title: "Axis X Title"
                },
                axisY: {
                    title: "Units"
                },
                data: [{
                    type: "line",
                    dataPoints: dps
                }]
            });

            chart.render();
            var xVal = dps.length + 1;
            var yVal = 15;
            var updateInterval = 3000;

            var updateChart = function () {


                yVal = yVal + Math.round(5 + Math.random() * (-5 - 5));
                dps.push({ x: xVal, y: yVal });

                xVal++;
                if (dps.length > 10) {
                    dps.shift();
                }

                chart.render();

                // update chart after specified time. 

            };

            setInterval(function () { updateChart() }, updateInterval);
        }
</script>--%>
    <style type="text/css">

    div#tabs
    {
        width:auto;
        min-width:300px;
        height:230px;
        
    }
.imageCapsul
{
    width:110px;
    margin:2px 5px 2px 5px;
    float:left;

}
 #alan
 {
        width:400px;
        height:auto;
        overflow:auto;
        }
        .olcum
        {
            text-align: center;
            font-size:10px;
            
        }
        
        .imagelar
        {
            width:100px;
        }
    </style>

 <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    
    <link href="fancybox/jquery.fancybox-1.3.4.css" rel="stylesheet" type="text/css" />
    <script src="fancybox/jquery.fancybox-1.3.4.pack.js" type="text/javascript"></script>
    <script src="fancybox/jquery.mousewheel-3.0.4.pack.js" type="text/javascript"></script>
    
    <link rel="stylesheet" type="text/css" href="css/style.css" />
		<script type="text/javascript" src="js/modernizr.custom.29473.js"></script>
    
  </head>
  <body style="font-size:62.5%;" >
  <div id="dialog" class="dlgfixed" title="L�tfen Bekleyiniz ...">
  <p style="padding:10px;margin:0 auto">Grafikler y�kleniyor ...</p>
</div>
      
      
  <div id="main">
  <div id="sol">
  <div id="header">
  <div id="logo">
  <img alt="frekans_akustis" src="image/logo.png" />
  </div>

  <div id="menu">
  <ul>
  <li style="width: 150px;max-height:40px; overflow: auto">
      <asp:HyperLink ID="hybMer" runat="server" Font-Size="11px"></asp:HyperLink>
  </li>
  <li style="width: 250px;max-height:40px; overflow: auto">
      <asp:HyperLink ID="hypPro" runat="server">[hypPro]</asp:HyperLink>
  </li>
   <li class='active has-sub'><a href='#'><span>Proje Se�</span></a>
      <ul style="overflow: auto; max-height: 400px;">
          <asp:Panel ID="pnlprj" runat="server">
          </asp:Panel>
      </ul>
   </li>
      <li class='active has-sub'><a href='#'><span>Raporlar</span></a>
      <ul style="overflow: auto; max-height: 400px;">
          <li class='has-sub'><a id="#" href="rapor.ashx?tur=genel"><span>Genel</span></a></li>
          <li class='has-sub'><a id="setInterval" href="#"><span>Tek Tek</span></a></li>
          
      </ul>
   </li>
   <li><a href="kapi/Default.aspx"><span>��k��</span></a></li> 
</ul>
  
  </div>
  </div>
  
      <%--<input type="button" name="test" id="test" value="Data Reload" />--%>
      
  <div class="container">
      
      
			<section class="ac-container">
				<div>
					<input id="ac-1" name="accordion-1" type="checkbox" checked  />
                    
					<label for="ac-1">
                        Harita
                        
					</label>
                    
					<article class="ac-large">
					
                        <div id="map" style="width:100%;height:100%"></div>
                        
                          
                          
					</article>
				</div>
				<div>
					<input id="ac-2" name="accordion-1" type="checkbox"  />
					<label for="ac-2">
                        LOGGER GRAPH (dBA, dBC, dB)
                        <%--<a href="#" id="pause" style="float:right;margin-right:30px;" for="ac-2" >Duraklat</a>--%>
					</label>
					<article class="ac-large">
						<div id="chartLogger" style="width:99%;height:500px" >
                        <span style="font-size:25px;margin:0 auto;position:relative;top:20px;left:40%;">Grafi�i g�rmek i�in herhangi bir pin in �zerine t�klay�n�z.</span>    
						</div>
                        
					</article>
				</div>
                <div>
					<input id="ac-3" name="accordion-1" type="checkbox"  />
					<label for="ac-3">HOURLY AVARAGE</label>
					<article class="ac-mediumx" >
                        <div id="Div1" style="width:98%;height:300px;padding:10px;overflow:auto" >
                            <span id="tarih" style="font-size:16px;font-weight:bold">Tarih</span>
                            <table class="hourly">
                                <th>Start Time</th><th></th><th>End Time</th><th>P1</th><th>P3</th>
                                
                                
                            </table>
                        </div>
						
					</article>
				</div>
				<div>
					<input id="ac-4" name="accordion-1" type="checkbox" />
					<label for="ac-4">1/3 OCTAV BAND</label>
					<article class="ac-mediumx">
                        <div id="chart1_3" style="width:99%;height:300px" ></div>
						
					</article>
				</div>
				<div>
					<input id="ac-5" name="accordion-1" type="checkbox"  />
					<label for="ac-5">1/1 OCTAV BAND</label>
					<article class="ac-mediumx" >
                        <div id="chart1_1" style="width:98%;height:300px" ></div>
						
					</article>
				</div>
                
			</section>
        </div>

  
  </div>
      
  <div id="sag">
<h2>�l��m Noktalar�</h2>
      
<%--<asp:Button ID="Button1" runat="server" Text="Rapor Olu�tur" onclick="Button1_Click" 
          Width="100%" />--%>
  <ul >
  
       
      <li>
          <div id="pinlegand" style="float:left;width:50%">
              <table class="detail2">
                  
                  <tr>
                      <td>Lday</td><td><span id="lday"></span></td>
                      
                      </tr>
          
          <tr>
                      <td>Leve</td> <td><span id="leve"></span></td>
                      
                      </tr>
                  
                  <tr>
                      <td>Lnight</td> <td><span id="lnight"></span></td>
                      
                      </tr>
          <tr>
                      <td>Lden</td> <td><span id="lden"></span></td>
                      
                      </tr>
                  
                  <tr>
                      <td colspan="2"> > .. - 35 </td> 
                      
                      </tr>
                  <tr>
                      <td colspan="2"> > 35 - 40 </td> 
                      
                      </tr>
                  <tr>
                      <td colspan="2"> > 40 - 45 </td> 
                      
                      </tr>
                  <tr>
                      <td colspan="2"> > 45 - 50 </td> 
                      
                      </tr>
                  <tr>
                      <td colspan="2"> > 50 - 55 </td> 
                      
                      </tr>
                  <tr>
                      <td colspan="2"> > 55 - 60 </td> 
                      
                      </tr>
                  <tr>
                      <td colspan="2"> > 60 - 65 </td> 
                      
                      </tr>
                  <tr>
                      <td colspan="2"> > 65 - 70 </td> 
                      
                      </tr>
                  <tr>
                      <td colspan="2"> > 70 - 75 </td> 
                      
                      </tr>
                  <tr>
                      <td colspan="2"> > 75 - 80 </td> 
                      
                      </tr>
                  <tr>
                      <td colspan="2"> > 80 - .. </td> 
                      
                      </tr>
          </table>
          </div>
          <asp:Panel ID="pnlPin" runat="server" style="float:right;width:50%;">
      
              
              

          </asp:Panel>   
          
      </li>
    
    
  </ul>
      
  
  
  </div>
  </div>
          
    <input type="hidden" id="kulRef" runat="server" />
      
  </body>
    <script src="js/map.js"></script>
</html>

