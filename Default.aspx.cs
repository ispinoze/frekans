﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DbClass;
using System.Web.Services;
using System.Data;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Data.OleDb;
using System.IO;
using System.Collections;
using System.Web.Script.Serialization;
using System.Web.UI.DataVisualization.Charting;
public partial class _Default : System.Web.UI.Page
{
    DbClassMysql db = new DbClassMysql();

    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["yetki"]!=null)
        {

            if (Session["KullaniciRefno"]!=null)
            {

                string sqlFirmaadi = Convert.ToString(db.ScalarQuery("select isnull(NULLIF(musteri_adi,''),kullanici_adi) as ad from tbl_kullanici where id=" + Session["KullaniciRefno"].ToString()));

                hybMer.Text = "Hoşgeldiniz <b>" + sqlFirmaadi + "</b>";
                string sqlPrjler;

                switch (Convert.ToInt32(Session["yetki"]))
                {
                    case 1:
                        sqlPrjler = "select * from tbl_proje where durum=1 order by tarih desc";
                        break;
                    case 2:
                        sqlPrjler = "select * from tbl_proje where durum=1 and firma_id=" + Session["KullaniciRefno"].ToString() + "order by tarih desc";
                        break;
                    case 3:
                        sqlPrjler = "select * from tbl_proje where durum=1 order by tarih desc";
                        break;
                    default:
                        sqlPrjler = "select * from tbl_proje where durum=1 order by tarih desc";
                        break;
                }
                
                

                DataTable dtPrj = db.Fill(sqlPrjler);
                                
                for (int i = 0; i < dtPrj.Rows.Count; i++)
                {
                    pnlprj.Controls.Add(new LiteralControl("<li class='has-sub'><a id=" + dtPrj.Rows[i]["id"].ToString() + "_" + i + " href='Default.aspx?prjrefno=" + dtPrj.Rows[i]["id"].ToString() + "'><span>" + dtPrj.Rows[i]["proje_adi"].ToString() + "</span></a></li>"));

                }
                
                if (Request["prjrefno"] == null)
                {
                    
                    string sqlPrjAdi = "";
                    DataTable dtPin=new DataTable();
                    if (dtPrj.Rows.Count != 0)
                    {
                    
                    Session["activePrj"] = dtPrj.Rows[0]["id"].ToString();

                    Session["prjrefno"] = dtPrj.Rows[0]["id"].ToString();
                    
                    string sqlNoktalar = "select * from tbl_projedetay where proje_id=" + dtPrj.Rows[0]["id"].ToString() + " order by sira asc";

                    dtPin = db.Fill(sqlNoktalar);

                    int detayID = Convert.ToInt32(dtPin.Rows[0]["id"]);
                    
                    sqlPrjAdi = Convert.ToString(db.ScalarQuery("select proje_adi from tbl_proje where id=" + dtPrj.Rows[0]["id"].ToString()));
                    }


                    if (sqlPrjAdi!="")
                    {
                        hypPro.Text = "Seçilen proje | <b>" + sqlPrjAdi + "</b> |"; 
                    }
                    else
                    {
                        hypPro.Text = "Herhangi bir proje bulunamadı"; 
                    }
                    pnlPin.Controls.Add(new LiteralControl("<input type='button' value='Detay Göster' id='detail_visible' style='font-size: 12.5px;' />"));
                    pnlPin.Controls.Add(new LiteralControl("<table class='detail'><th>Pin No</th><th>LAeq</th><th>Lamax</th><th>Lamin</th><th>LA90</th><th>LA50</th><th>LA10</th><th>LCeq</th><th>Lcmax</th><th>Lcmin</th><th>LC90</th><th>LC50</th><th>LC10</th>"));

                    for (int k = 0; k < dtPin.Rows.Count; k++)
                    {

                        string sqlSlm = "select Profil,Round(MaxValue,2) as MaxValue,Round(MinValue,2) as MinValue,Round(LEQ,2) as LEQ,Round(Lof90,2) as Lof90,Round(Lof50,2) as Lof50,Round(Lof10,2) as Lof10 from tbl_slm where ProjeDetayID=" + dtPin.Rows[k]["id"] + " order by Profil";
                        
                        DataTable dtSlm = db.Fill(sqlSlm);

                        if (dtSlm.Rows.Count<=0)
                        {
                            return;
                        }

                        if (dtSlm.Rows.Count == 3)
                        {
                            pnlPin.Controls.Add(new LiteralControl("<tr><td><a class='pins' id=p_" + dtPin.Rows[k]["pin_id"] + " href='#'>" + dtPin.Rows[k]["sira"] + "</a></td><td>" + dtSlm.Rows[0]["LEQ"] + "</td><td>" + dtSlm.Rows[0]["MaxValue"] + "</td><td>" + dtSlm.Rows[0]["MinValue"] + "</td><td>" + dtSlm.Rows[0]["Lof90"] + "</td><td>" + dtSlm.Rows[0]["Lof50"] + "</td><td>" + dtSlm.Rows[0]["Lof10"] + "</td><td>" + dtSlm.Rows[2]["LEQ"] + "</td><td>" + dtSlm.Rows[2]["MaxValue"] + "</td><td>" + dtSlm.Rows[2]["MinValue"] + "</td><td>" + dtSlm.Rows[2]["Lof90"] + "</td><td>" + dtSlm.Rows[2]["Lof50"] + "</td><td>" + dtSlm.Rows[2]["Lof10"] + "</td></tr>"));
                        }
                        else
                        {
                            pnlPin.Controls.Add(new LiteralControl("<tr><td><a class='pins' id=p_" + dtPin.Rows[k]["pin_id"] + " href='#'>" + dtPin.Rows[k]["sira"] + "</a></td><td>" + dtSlm.Rows[0]["LEQ"] + "</td><td>" + dtSlm.Rows[0]["MaxValue"] + "</td><td>" + dtSlm.Rows[0]["MinValue"] + "</td><td>" + dtSlm.Rows[0]["Lof90"] + "</td><td>" + dtSlm.Rows[0]["Lof50"] + "</td><td>" + dtSlm.Rows[0]["Lof10"] + "</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>"));
                        }

                    }

                    pnlPin.Controls.Add(new LiteralControl("</table>"));
                }
                else
                {
                    Session["prjrefno"] = Request["prjrefno"];
                    Session["activePrj"] = Request["prjrefno"];
                    int control = Convert.ToInt32(db.ScalarQuery("select * from tbl_proje where id=" + Convert.ToInt32(Request["prjrefno"]) + " and firma_id=" + Convert.ToInt32(Session["KullaniciRefno"])));
                    switch (Convert.ToInt32(HttpContext.Current.Session["yetki"]))
                    {
                        case 1:
                            control = 10;
                            break;
                        case 2:

                            break;
                        case 3:
                            control = 10;
                            break;
                        default:
                            control = 10;
                            break;
                    }
                    if (control>0)
                    {
                        string sqlNoktalar = "select * from tbl_projedetay where proje_id=" + Request["prjrefno"] + " order by sira asc";

                        string sqlPrjAdi = Convert.ToString(db.ScalarQuery("select proje_adi from tbl_proje where id=" + Request["prjrefno"].ToString()));

                        int test=Convert.ToInt32(db.ScalarQuery("select max(id) from tbl_projedetay where proje_id=" + Request["prjrefno"].ToString()));
                        --test;

                        Session["maxDetayID"] = test;
                        
                        hypPro.Text = "Seçilen proje | <b>" + sqlPrjAdi + "</b> |";

                        DataTable dtPin = db.Fill(sqlNoktalar);

                        int detayID = Convert.ToInt32(dtPin.Rows[0]["id"]);

                        pnlPin.Controls.Add(new LiteralControl("<input type='button' value='Detay Göster' id='detail_visible' style='font-size: 12.5px;' />"));

                        pnlPin.Controls.Add(new LiteralControl("<table class='detail'><th>Pin No</th><th>LAeq</th><th>Lamax</th><th>Lamin</th><th>LA90</th><th>LA50</th><th>LA10</th><th>LCeq</th><th>Lcmax</th><th>Lcmin</th><th>LC90</th><th>LC50</th><th>LC10</th>"));

                        for (int k = 0; k < dtPin.Rows.Count; k++)
                        {

                            //string sqlSlm = "select SPL,SonucDate,LEQ,Profil from tbl_slm_history slm inner join tbl_projedetay as detay on slm.ProjeDetayID=detay.id where detay.pin_id=" + dtPin.Rows[k]["pin_id"] + " order by SonucDate asc";

                            string sqlSlm = "select Profil,Round(MaxValue,2) as MaxValue,Round(MinValue,2) as MinValue,Round(LEQ,2) as LEQ,Round(Lof90,2) as Lof90,Round(Lof50,2) as Lof50,Round(Lof10,2) as Lof10 from tbl_slm_history where ProjeDetayID=" + dtPin.Rows[k]["id"] + " order by Profil";

                            DataTable dtSlm = db.Fill(sqlSlm);

                            if (dtSlm.Rows.Count==3)
                            {
                                pnlPin.Controls.Add(new LiteralControl("<tr><td><a class='pins' id=p_" + dtPin.Rows[k]["pin_id"] + " href='#'>" + dtPin.Rows[k]["sira"] + "</a></td><td>" + dtSlm.Rows[0]["LEQ"] + "</td><td>" + dtSlm.Rows[0]["MaxValue"] + "</td><td>" + dtSlm.Rows[0]["MinValue"] + "</td><td>" + dtSlm.Rows[0]["Lof90"] + "</td><td>" + dtSlm.Rows[0]["Lof50"] + "</td><td>" + dtSlm.Rows[0]["Lof10"] + "</td><td>" + dtSlm.Rows[2]["LEQ"] + "</td><td>" + dtSlm.Rows[2]["MaxValue"] + "</td><td>" + dtSlm.Rows[2]["MinValue"] + "</td><td>" + dtSlm.Rows[2]["Lof90"] + "</td><td>" + dtSlm.Rows[2]["Lof50"] + "</td><td>" + dtSlm.Rows[2]["Lof10"] + "</td></tr>"));
                            }
                            else
                            {
                                pnlPin.Controls.Add(new LiteralControl("<tr><td><a class='pins' id=p_" + dtPin.Rows[k]["pin_id"] + " href='#'>" + dtPin.Rows[k]["sira"] + "</a></td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>"));
                            }

                            #region html


                            /*
                             
          
                                  
                                  <tr>
                                              <td><a href="#">1</a></td><td>Lamax</td><td>Lamin</td><td>LA90</td><td>LA90</td><td>LA90</td><td>LCeq</td><td>Lcmax</td><td>Lcmin</td><td>LC90</td>
                      
                                              </tr>
                                  <tr>
                                              <td><a href="#">2</a></td><td>Lamax</td><td>Lamin</td><td>LA90</td><td>LA90</td><td>LA90</td><td>LCeq</td><td>Lcmax</td><td>Lcmin</td><td>LC90</td>
                      
                                              </tr>
                                  <tr>
                                              <td><a href="#">3</a></td><td>Lamax</td><td>Lamin</td><td>LA90</td><td>LA90</td><td>LA90</td><td>LCeq</td><td>Lcmax</td><td>Lcmin</td><td>LC90</td>
                      
                                              </tr>
                                  <tr>
                                              <td><a href="#">4</a></td><td>Lamax</td><td>Lamin</td><td>LA90</td><td>LA90</td><td>LA90</td><td>LCeq</td><td>Lcmax</td><td>Lcmin</td><td>LC90</td>
                      
                                              </tr>
                                  <tr>
                                              <td><a href="#">5</a></td><td>Lamax</td><td>Lamin</td><td>LA90</td><td>LA90</td><td>LA90</td><td>LCeq</td><td>Lcmax</td><td>Lcmin</td><td>LC90</td>
                      
                                              </tr>
          
                        
                             
                             */

                            //pnlPin.Controls.Add(new LiteralControl("<li><a class='pins' id=p_" + dtPin.Rows[k]["pin_id"] + " href='#'>" + dtPin.Rows[k]["sira"] + ".Nokta</a></li><table class='detail'><tr><td colspan='4' style='font-size:14px;font-weight:bolder'>Profil 1</td> <td colspan='4' style='font-size:14px;font-weight:bolder'>Profil 2</td></tr><th>LAeq</th><th>LAmax</th><th>LAmin</th><th>LA90</th><th>LCeq</th><th>LCmax</th><th>LCmin</th><th>LC90</th><tr><td>LAeq</td><td>Lamax</td><td>Lamin</td><td>LA90</td><td>LCeq</td><td>Lcmax</td><td>Lcmin</td><td>LC90</td></tr></table>"));
                            //pnlPin.Controls.Add(new LiteralControl("<li><a class='pins' id=p_" + dtPin.Rows[k]["pin_id"] + " href='#'>" + dtPin.Rows[k]["sira"] + ".Nokta</a></li>"));

                            #endregion html
                        }

                        pnlPin.Controls.Add(new LiteralControl("</table>"));
                    }
                    else
                    {
                        Response.Redirect("Default.aspx");
                    }
                }

            }
            
        }
        else
        {
            Response.Redirect("Kapi/Default.aspx");
        }
        
    }

    [WebMethod]
    public static string pinOlustur()
    {
        
        string pinler = "";
        string header = "";
        string sonuc = "";
        string Ln = "";
        string hava = "";
        string fotograf = "";
        string xls_dosyasi = "";
        string pin_id = "";
       try
        {
            #region kod
            if (HttpContext.Current.Session["KullaniciRefno"] != null)
            {

                if (HttpContext.Current.Session["prjrefno"] == null)
                {
                    string sqlGetPin;

                    string kulNo = HttpContext.Current.Session["KullaniciRefno"].ToString();

                    int projeID = Convert.ToInt32(DbClassMysql.ScalarQuery2("select id from tbl_proje where firma_id=" + kulNo + " and durum=1 order by tarih desc"));

                    if (projeID == 0)
                    {
                        projeID = Convert.ToInt32(DbClassMysql.ScalarQuery2("select id from tbl_proje where durum=1 order by tarih desc"));
                    }

                    sqlGetPin = "SELECT * FROM tbl_projedetay where proje_id=" + projeID + " order by tarih asc";

                    DataTable dtGetPin = DbClassMysql.Fill2(sqlGetPin);

                    for (int i = 0; i < dtGetPin.Rows.Count; i++)
                    {
                        string enlem = dtGetPin.Rows[i]["enlem"].ToString();

                        string boylam = dtGetPin.Rows[i]["boylam"].ToString();

                        string zoom = dtGetPin.Rows[i]["zoom"].ToString();

                        pin_id = dtGetPin.Rows[i]["pin_id"].ToString();

                        string aciklama = dtGetPin.Rows[i]["aciklama"].ToString();

                        string proje_detay_id = dtGetPin.Rows[i]["id"].ToString();

                        string sira = dtGetPin.Rows[i]["sira"].ToString();


                        if (dtGetPin.Rows.Count - 1 > i)
                        {
                            pinler += enlem + "*" + boylam + "*" + zoom + "*" + pin_id + "*" + aciklama + "*" + sira + "|";
                        }
                        else
                        {
                            pinler += enlem + "*" + boylam + "*" + zoom + "*" + pin_id + "*" + aciklama + "*" + sira;
                        }
                    }

                    string sqlGetHeader = "select * from tbl_header header right join tbl_projedetay detay on header.proje_detay_id=detay.id where detay.proje_id=" + projeID + " order by detay.id asc";

                    DataTable dtHeader = DbClassMysql.Fill2(sqlGetHeader);

                    for (int i = 0; i < dtHeader.Rows.Count; i++)
                    {

                        string calibration = dtHeader.Rows[i]["calibration_factor"].ToString();

                        string device = dtHeader.Rows[i]["device_type"].ToString();

                        string original_file_name = dtHeader.Rows[i]["original_file_name"].ToString();

                        string associated_file_name = dtHeader.Rows[i]["associated_file_name"].ToString();

                        string calibration_date = dtHeader.Rows[i]["calibration_date"].ToString();

                        string calibration_time = dtHeader.Rows[i]["calibration_time"].ToString();

                        string serial_no = dtHeader.Rows[i]["serial_no"].ToString();

                        if (dtHeader.Rows.Count - 1 > i)
                        {
                            header += calibration + "*" + device + "*" + original_file_name + "*" + associated_file_name + "*" + calibration_date + "*" + calibration_time + "*" + serial_no + "|";
                        }
                        else
                        {
                            header += calibration + "*" + device + "*" + original_file_name + "*" + associated_file_name + "*" + calibration_date + "*" + calibration_time + "*" + serial_no;
                        }
                    }

                    string sqlGetSonuc = "select sira,max(LEQ) as leq from tbl_slm sonuc right join tbl_projedetay detay on sonuc.ProjeDetayID=detay.id where detay.proje_id=" + HttpContext.Current.Session["prjrefno"] + " group by sira";

                    DataTable dtSonuc = DbClassMysql.Fill2(sqlGetSonuc);

                    for (int i = 0; i < dtSonuc.Rows.Count; i++)
                    {
                        string elapsed_time="";

                        string day = "";

                        string hour = "";

                        string filter = "";

                        string detector = "";

                        string peak = "";

                        string max = "";

                        string min = "";

                        string leq = dtSonuc.Rows[i]["leq"].ToString();

                        if (dtSonuc.Rows.Count - 1 > i)
                        {
                            if (day == "")
                            {
                                sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq + "*" + "|";
                            }
                        }
                        else
                        {
                            
                                sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq;
                            
                        }
                    }

                    string sqlGetLn = "select * from tbl_ln ln right join tbl_projedetay detay on ln.proje_detay_id=detay.id where detay.proje_id=" + projeID + " order by detay.id asc";

                    DataTable dtLn = DbClassMysql.Fill2(sqlGetLn);

                    for (int i = 0; i < dtLn.Rows.Count; i++)
                    {
                        pin_id = dtLn.Rows[i]["sira"].ToString();
                        xls_dosyasi = dtLn.Rows[i]["xls_dosya"].ToString();

                        string L10 = dtLn.Rows[i]["ln_10"].ToString();

                        string L90 = dtLn.Rows[i]["ln_90"].ToString();

                        if (dtLn.Rows.Count - 1 > i)
                        {
                            Ln += L10 + "*" + L90 + "|";
                        }
                        else
                        {
                            Ln += L10 + "*" + L90;
                        }


                    }

                    string sqlGetHava = "select * from tbl_hava hava right join tbl_projedetay detay on hava.proje_detay_id=detay.id where detay.proje_id=" + projeID + " order by detay.id asc";

                    DataTable dtHava = DbClassMysql.Fill2(sqlGetHava);

                    for (int i = 0; i < dtHava.Rows.Count; i++)
                    {
                        string temp = dtHava.Rows[i]["temp"].ToString();

                        string ruzgar = dtHava.Rows[i]["ruzgar"].ToString();

                        string nem = dtHava.Rows[i]["nem"].ToString();

                        if (dtHava.Rows.Count - 1 > i)
                        {
                            hava += temp + "*" + ruzgar + "*" + nem + "|";
                        }
                        else
                        {
                            hava += temp + "*" + ruzgar + "*" + nem;
                        }
                    }

                    string sqlGetFoto = "select * from tbl_foto foto right join tbl_projedetay detay on foto.proje_detay_id=detay.id where detay.proje_id=" + projeID + " order by detay.id asc";

                    DataTable dtFoto = DbClassMysql.Fill2(sqlGetFoto);

                    for (int i = 0; i < dtFoto.Rows.Count; i++)
                    {
                        string foto_adi = dtFoto.Rows[i]["fotograf"].ToString();
                        string foto_adi2 = "";
                        string ilk = "";
                        string ilk2 = "";
                        string[] bol = foto_adi.Split(']');

                        if (foto_adi == "")
                        {
                            fotograf += "" + "|";
                            continue;
                        }

                        if (!string.IsNullOrEmpty(bol[1]))
                        {
                            ilk = bol[1].Substring(0, (bol[1].Length - 4));
                        }
                        if (i != 0)
                        {
                            foto_adi2 = dtFoto.Rows[i - 1]["fotograf"].ToString();

                            string[] bol2 = foto_adi2.Split(']');

                            if (foto_adi2 != "")
                            {
                                if (!string.IsNullOrEmpty(bol2[1]))
                                {
                                    ilk2 = bol2[1].Substring(0, (bol2[1].Length - 4));
                                }
                            }

                            if (ilk2 == ilk)
                            {
                                if (fotograf.Substring(0, fotograf.Length - 1).IndexOf('|') != -1)
                                {
                                    //fotograf = fotograf.Substring(0, fotograf.Length - 1);    
                                }
                                fotograf += "*" + foto_adi;
                            }
                            else
                            {
                                fotograf += "|" + foto_adi;
                            }
                        }
                        else
                        {
                            fotograf += foto_adi;
                        }

                    }

                }
                else
                {
                    int prjRefno = Convert.ToInt32(HttpContext.Current.Session["prjrefno"]);
                    int firmaRefno = Convert.ToInt32(HttpContext.Current.Session["KullaniciRefno"]);
                    int control = Convert.ToInt32(DbClassMysql.ScalarQuery2("select * from tbl_proje where id=" + prjRefno + " and firma_id=" + firmaRefno + " and durum=1"));
                    switch (Convert.ToInt32(HttpContext.Current.Session["yetki"]))
                    {
                        case 1:
                            control = 10;
                            break;
                        case 2:

                            break;
                        case 3:
                            control = 10;
                            break;
                        default:
                            control = 10;
                            break;
                    }

                    if (control > 0)
                    {

                        string sqlGetPin = "SELECT * FROM tbl_projedetay where proje_id=" + HttpContext.Current.Session["prjrefno"] + " order by tarih asc";

                        DataTable dtGetPin = DbClassMysql.Fill2(sqlGetPin);

                        for (int i = 0; i < dtGetPin.Rows.Count; i++)
                        {
                            string enlem = dtGetPin.Rows[i]["enlem"].ToString();

                            string boylam = dtGetPin.Rows[i]["boylam"].ToString();

                            string zoom = dtGetPin.Rows[i]["zoom"].ToString();

                            pin_id = dtGetPin.Rows[i]["pin_id"].ToString();

                            string aciklama = dtGetPin.Rows[i]["aciklama"].ToString();

                            string proje_detay_id = dtGetPin.Rows[i]["id"].ToString();

                            string sira = dtGetPin.Rows[i]["sira"].ToString();

                            if (dtGetPin.Rows.Count - 1 > i)
                            {
                                pinler += enlem + "*" + boylam + "*" + zoom + "*" + pin_id + "*" + aciklama + "*" + sira + "|";
                            }
                            else
                            {
                                pinler += enlem + "*" + boylam + "*" + zoom + "*" + pin_id + "*" + aciklama + "*" + sira;
                            }

                        }

                        string sqlGetHeader = "select * from tbl_header header right join tbl_projedetay detay on header.proje_detay_id=detay.id where detay.proje_id=" + HttpContext.Current.Session["prjrefno"] + " order by detay.id asc";

                        DataTable dtHeader = DbClassMysql.Fill2(sqlGetHeader);

                        for (int i = 0; i < dtHeader.Rows.Count; i++)
                        {

                            string calibration = dtHeader.Rows[i]["calibration_factor"].ToString();

                            string device = dtHeader.Rows[i]["device_type"].ToString();

                            string original_file_name = dtHeader.Rows[i]["original_file_name"].ToString();

                            string associated_file_name = dtHeader.Rows[i]["associated_file_name"].ToString();

                            string calibration_date = dtHeader.Rows[i]["calibration_date"].ToString();

                            string calibration_time = dtHeader.Rows[i]["calibration_time"].ToString();

                            string serial_no = dtHeader.Rows[i]["serial_no"].ToString();

                            if (dtHeader.Rows.Count - 1 > i)
                            {
                                header += calibration + "*" + device + "*" + original_file_name + "*" + associated_file_name + "*" + calibration_date + "*" + calibration_time + "*" + serial_no + "|";
                            }
                            else
                            {
                                header += calibration + "*" + device + "*" + original_file_name + "*" + associated_file_name + "*" + calibration_date + "*" + calibration_time + "*" + serial_no;
                            }
                        }

                        string sqlGetSonuc = "select sira,max(LEQ) as leq from tbl_slm sonuc right join tbl_projedetay detay on sonuc.ProjeDetayID=detay.id where detay.proje_id=" + HttpContext.Current.Session["prjrefno"] + " group by sira";

                        DataTable dtSonuc = DbClassMysql.Fill2(sqlGetSonuc);

                        for (int i = 0; i < dtSonuc.Rows.Count; i++)
                        {

                            string elapsed_time = "";


                            string day = "";

                            string hour = "";

                            string filter = "";

                            string detector = "";



                            string peak = "";

                            string max = "";

                            string min = "";

                            string leq = dtSonuc.Rows[i]["leq"].ToString();


                            if (dtSonuc.Rows.Count - 1 > i)
                            {
                                if (day == "")
                                {
                                    sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq + "*" + "|";
                                }
                                
                            }
                            else
                            {
                                
                                    sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq;
                                
                            }
                        }

                        string sqlGetLn = "select * from tbl_ln ln right join tbl_projedetay detay on ln.proje_detay_id=detay.id where detay.proje_id=" + HttpContext.Current.Session["prjrefno"] + " order by detay.id asc";

                        DataTable dtLn = DbClassMysql.Fill2(sqlGetLn);

                        for (int i = 0; i < dtLn.Rows.Count; i++)
                        {
                            pin_id = dtLn.Rows[i]["sira"].ToString();
                            xls_dosyasi = dtLn.Rows[i]["xls_dosya"].ToString();

                            string L10 = dtLn.Rows[i]["ln_10"].ToString();

                            string L90 = dtLn.Rows[i]["ln_90"].ToString();
                            if (dtLn.Rows.Count - 1 > i)
                            {
                                Ln += L10 + "*" + L90 + "|";
                            }
                            else
                            {
                                Ln += L10 + "*" + L90;
                            }


                        }

                        string sqlGetHava = "select * from tbl_hava hava right join tbl_projedetay detay on hava.proje_detay_id=detay.id where detay.proje_id=" + HttpContext.Current.Session["prjrefno"] + " order by detay.id asc";

                        DataTable dtHava = DbClassMysql.Fill2(sqlGetHava);

                        for (int i = 0; i < dtHava.Rows.Count; i++)
                        {

                            string temp = dtHava.Rows[i]["temp"].ToString();

                            string ruzgar = dtHava.Rows[i]["ruzgar"].ToString();

                            string nem = dtHava.Rows[i]["nem"].ToString();

                            if (dtHava.Rows.Count - 1 > i)
                            {
                                hava += temp + "*" + ruzgar + "*" + nem + "|";
                            }
                            else
                            {
                                hava += temp + "*" + ruzgar + "*" + nem;
                            }
                        }

                        string sqlGetFoto = "select * from tbl_foto foto right join tbl_projedetay detay on foto.proje_detay_id=detay.id where detay.proje_id=" + HttpContext.Current.Session["prjrefno"] + " order by detay.id asc";

                        DataTable dtFoto = DbClassMysql.Fill2(sqlGetFoto);

                        for (int i = 0; i < dtFoto.Rows.Count; i++)
                        {


                            string foto_adi = dtFoto.Rows[i]["fotograf"].ToString();
                            string foto_adi2 = "";
                            string ilk = "";
                            string ilk2 = "";
                            string[] bol = foto_adi.Split(']');

                            if (foto_adi == "")
                            {
                                fotograf += "" + "|";
                                continue;
                            }

                            if (!string.IsNullOrEmpty(bol[1]))
                            {
                                ilk = bol[1].Substring(0, (bol[1].Length - 4));
                            }
                            if (i != 0)
                            {
                                foto_adi2 = dtFoto.Rows[i - 1]["fotograf"].ToString();

                                string[] bol2 = foto_adi2.Split(']');



                                if (foto_adi2 != "")
                                {
                                    if (!string.IsNullOrEmpty(bol2[1]))
                                    {
                                        ilk2 = bol2[1].Substring(0, (bol2[1].Length - 4));
                                    }
                                }

                                if (ilk2 == ilk)
                                {
                                    fotograf += "*" + foto_adi;
                                }
                                else
                                {
                                    fotograf += "|" + foto_adi;
                                }
                            }
                            else
                            {
                                fotograf += foto_adi;
                            }

                        }

                    }
                    else
                    {
                        HttpContext.Current.Response.Redirect("Default.aspx");
                    }
                }
            }
            #endregion kod

            fotograf = fotograf.TrimEnd('|');
            sonuc = sonuc.TrimEnd('|');
        }
        catch (Exception ex)
        {

            HttpContext.Current.Response.Write(" Hata Acıklaması :" + ex.Message + " \n\r Hatalı Pin:"+pin_id+" \n\r Hatalı Xls-Dosya:"+xls_dosyasi+"  ~");

        }
        

        return pinler + "~" + header + "~" + sonuc + "~" + Ln + "~" + hava + "~" + fotograf;
        
        
        //return fotograf;
    }

#region bos_seyler
    
    //public string pinOlustur2()
    //{

    //    string pinler = "";
    //    string header = "";
    //    string sonuc = "";
    //    string Ln = "";
    //    string hava = "";
    //    string fotograf = "";
    //    string pin_id = "";

    //    if (HttpContext.Current.Session["KullaniciRefno"] != null)
    //    {

    //        if (HttpContext.Current.Session["prjrefno"] == null)
    //        {
    //            string sqlGetPin;

    //            string kulNo = HttpContext.Current.Session["KullaniciRefno"].ToString();

    //            int projeID = Convert.ToInt32(DbClassMysql.ScalarQuery2("select id from tbl_proje where firma_id=" + kulNo + " order by tarih desc"));

    //            if (projeID == 0)
    //            {
    //                projeID = Convert.ToInt32(DbClassMysql.ScalarQuery2("select id from tbl_proje order by tarih desc"));
    //            }

    //            sqlGetPin = "SELECT * FROM tbl_projedetay where proje_id=" + projeID + " order by tarih asc";

    //            DataTable dtGetPin = DbClassMysql.Fill2(sqlGetPin);

    //            for (int i = 0; i < dtGetPin.Rows.Count; i++)
    //            {
    //                string enlem = dtGetPin.Rows[i]["enlem"].ToString();

    //                string boylam = dtGetPin.Rows[i]["boylam"].ToString();

    //                string zoom = dtGetPin.Rows[i]["zoom"].ToString();

    //                pin_id = dtGetPin.Rows[i]["pin_id"].ToString();

    //                string aciklama = dtGetPin.Rows[i]["aciklama"].ToString();

    //                string proje_detay_id = dtGetPin.Rows[i]["id"].ToString();

    //                string sira = dtGetPin.Rows[i]["sira"].ToString();

    //                if (dtGetPin.Rows.Count - 1 > i)
    //                {
    //                    pinler += enlem + "*" + boylam + "*" + zoom + "*" + pin_id + "*" + aciklama + "*" + sira + "|";
    //                }
    //                else
    //                {
    //                    pinler += enlem + "*" + boylam + "*" + zoom + "*" + pin_id + "*" + aciklama + "*" + sira;
    //                }
    //            }

    //            string sqlGetHeader = "select * from tbl_header header right join tbl_projedetay detay on header.proje_detay_id=detay.id where detay.proje_id=" + projeID + " order by detay.id asc";

    //            DataTable dtHeader = DbClassMysql.Fill2(sqlGetHeader);

    //            for (int i = 0; i < dtHeader.Rows.Count; i++)
    //            {

    //                string calibration = dtHeader.Rows[i]["calibration_factor"].ToString();

    //                string device = dtHeader.Rows[i]["device_type"].ToString();

    //                string original_file_name = dtHeader.Rows[i]["original_file_name"].ToString();

    //                string associated_file_name = dtHeader.Rows[i]["associated_file_name"].ToString();

    //                string calibration_date = dtHeader.Rows[i]["calibration_date"].ToString();

    //                string calibration_time = dtHeader.Rows[i]["calibration_time"].ToString();

    //                string serial_no = dtHeader.Rows[i]["serial_no"].ToString();

    //                if (dtHeader.Rows.Count - 1 > i)
    //                {
    //                    header += calibration + "*" + device + "*" + original_file_name + "*" + associated_file_name + "*" + calibration_date + "*" + calibration_time + "*" + serial_no + "|";
    //                }
    //                else
    //                {
    //                    header += calibration + "*" + device + "*" + original_file_name + "*" + associated_file_name + "*" + calibration_date + "*" + calibration_time + "*" + serial_no;
    //                }
    //            }

    //            string sqlGetSonuc = "select * from tbl_sonuc sonuc right join tbl_projedetay detay on sonuc.proje_detay_id=detay.id where detay.proje_id=" + projeID + " order by detay.id asc";

    //            DataTable dtSonuc = DbClassMysql.Fill2(sqlGetSonuc);

    //            for (int i = 0; i < dtSonuc.Rows.Count; i++)
    //            {
    //                DateTime test;
    //                string elapsed_time;

    //                if (DateTime.TryParse(Convert.ToString(dtSonuc.Rows[i]["elapsed_time"].ToString()), out test))
    //                {
    //                    elapsed_time = dtSonuc.Rows[i]["elapsed_time"].ToString();
    //                }
    //                else
    //                {
    //                    string[] olcum = Convert.ToString(dtSonuc.Rows[i]["elapsed_time"].ToString()).Split(':');

    //                    if (olcum[0].Trim() == "24")
    //                    {
    //                        elapsed_time = "01";
    //                    }
    //                    else
    //                    {
    //                        elapsed_time = olcum[0].ToString();
    //                    }

    //                    if (elapsed_time != "")
    //                    {
    //                        elapsed_time += ":" + olcum[1].ToString() + ":" + olcum[2].ToString();
    //                    }
    //                }

    //                string day = dtSonuc.Rows[i]["day"].ToString();

    //                string hour = dtSonuc.Rows[i]["hour"].ToString();

    //                string filter = dtSonuc.Rows[i]["filter"].ToString();

    //                string detector = dtSonuc.Rows[i]["detector"].ToString();

    //                string peak = dtSonuc.Rows[i]["peak"].ToString();

    //                string max = dtSonuc.Rows[i]["max"].ToString();

    //                string min = dtSonuc.Rows[i]["min"].ToString();

    //                string leq = dtSonuc.Rows[i]["leq"].ToString();

    //                if (dtSonuc.Rows.Count - 1 > i)
    //                {
    //                    if (day == "")
    //                    {
    //                        sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq + "*" + "|";
    //                    }
    //                    else
    //                    {
    //                        if (filter == "Lin")
    //                        {
    //                            sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq + "|";
    //                        }
    //                        else
    //                        {
    //                            sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq + "*";
    //                        }
    //                    }
    //                }
    //                else
    //                {
    //                    if (day == "")
    //                    {
    //                        sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq + "|";
    //                    }
    //                    else
    //                    {
    //                        sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq;
    //                    }
    //                }
    //            }

    //            string sqlGetLn = "select * from tbl_ln ln right join tbl_projedetay detay on ln.proje_detay_id=detay.id where detay.proje_id=" + projeID + " order by detay.id asc";

    //            DataTable dtLn = DbClassMysql.Fill2(sqlGetLn);

    //            for (int i = 0; i < dtLn.Rows.Count; i++)
    //            {

    //                string L10 = dtLn.Rows[i]["ln_10"].ToString();

    //                string L90 = dtLn.Rows[i]["ln_90"].ToString();

    //                if (dtLn.Rows.Count - 1 > i)
    //                {
    //                    Ln += L10 + "*" + L90 + "|";
    //                }
    //                else
    //                {
    //                    Ln += L10 + "*" + L90;
    //                }


    //            }

    //            string sqlGetHava = "select * from tbl_hava hava right join tbl_projedetay detay on hava.proje_detay_id=detay.id where detay.proje_id=" + projeID + " order by detay.id asc";

    //            DataTable dtHava = DbClassMysql.Fill2(sqlGetHava);

    //            for (int i = 0; i < dtHava.Rows.Count; i++)
    //            {
    //                string temp = dtHava.Rows[i]["temp"].ToString();

    //                string ruzgar = dtHava.Rows[i]["ruzgar"].ToString();

    //                string nem = dtHava.Rows[i]["nem"].ToString();

    //                if (dtHava.Rows.Count - 1 > i)
    //                {
    //                    hava += temp + "*" + ruzgar + "*" + nem + "|";
    //                }
    //                else
    //                {
    //                    hava += temp + "*" + ruzgar + "*" + nem;
    //                }
    //            }

    //            string sqlGetFoto = "select * from tbl_foto foto right join tbl_projedetay detay on foto.proje_detay_id=detay.id where detay.proje_id=" + projeID + " order by detay.id asc";

    //            DataTable dtFoto = DbClassMysql.Fill2(sqlGetFoto);

    //            for (int i = 0; i < dtFoto.Rows.Count; i++)
    //            {
    //                string foto_adi = dtFoto.Rows[i]["fotograf"].ToString();
    //                string foto_adi2 = "";
    //                string ilk = "";
    //                string ilk2 = "";
    //                string[] bol = foto_adi.Split(']');

    //                if (foto_adi == "")
    //                {
    //                    fotograf += "" + "|";
    //                    continue;
    //                }

    //                if (!string.IsNullOrEmpty(bol[1]))
    //                {
    //                    ilk = bol[1].Substring(0, (bol[1].Length - 4));
    //                }
    //                if (i != 0)
    //                {
    //                    foto_adi2 = dtFoto.Rows[i - 1]["fotograf"].ToString();

    //                    string[] bol2 = foto_adi2.Split(']');

    //                    if (foto_adi2 != "")
    //                    {
    //                        if (!string.IsNullOrEmpty(bol2[1]))
    //                        {
    //                            ilk2 = bol2[1].Substring(0, (bol2[1].Length - 4));
    //                        }
    //                    }

    //                    if (ilk2 == ilk)
    //                    {
    //                        if (fotograf.Substring(0, fotograf.Length - 1).IndexOf('|') != -1)
    //                        {
    //                            //fotograf = fotograf.Substring(0, fotograf.Length - 1);    
    //                        }
    //                        fotograf += "*" + foto_adi;
    //                    }
    //                    else
    //                    {
    //                        fotograf += "|" + foto_adi;
    //                    }
    //                }
    //                else
    //                {
    //                    fotograf += foto_adi;
    //                }

    //            }

    //        }
    //        else
    //        {
    //            int prjRefno = Convert.ToInt32(HttpContext.Current.Session["prjrefno"]);
    //            int firmaRefno = Convert.ToInt32(HttpContext.Current.Session["KullaniciRefno"]);
    //            int control = Convert.ToInt32(DbClassMysql.ScalarQuery2("select * from tbl_proje where id=" + prjRefno + " and firma_id=" + firmaRefno));
    //            switch (Convert.ToInt32(HttpContext.Current.Session["yetki"]))
    //            {
    //                case 1:
    //                    control = 10;
    //                    break;
    //                case 2:

    //                    break;
    //                case 3:
    //                    control = 10;
    //                    break;
    //                default:
    //                    control = 10;
    //                    break;
    //            }

    //            if (control > 0)
    //            {

    //                string sqlGetPin = "SELECT * FROM tbl_projedetay where proje_id=" + HttpContext.Current.Session["prjrefno"] + " order by tarih asc";

    //                DataTable dtGetPin = DbClassMysql.Fill2(sqlGetPin);

    //                for (int i = 0; i < dtGetPin.Rows.Count; i++)
    //                {
    //                    string enlem = dtGetPin.Rows[i]["enlem"].ToString();

    //                    string boylam = dtGetPin.Rows[i]["boylam"].ToString();

    //                    string zoom = dtGetPin.Rows[i]["zoom"].ToString();

    //                    pin_id = dtGetPin.Rows[i]["pin_id"].ToString();

    //                    string aciklama = dtGetPin.Rows[i]["aciklama"].ToString();

    //                    string proje_detay_id = dtGetPin.Rows[i]["id"].ToString();

    //                    string sira = dtGetPin.Rows[i]["sira"].ToString();

    //                    if (dtGetPin.Rows.Count - 1 > i)
    //                    {
    //                        pinler += enlem + "*" + boylam + "*" + zoom + "*" + pin_id + "*" + aciklama + "*" + sira + "|";
    //                    }
    //                    else
    //                    {
    //                        pinler += enlem + "*" + boylam + "*" + zoom + "*" + pin_id + "*" + aciklama + "*" + sira;
    //                    }

    //                }

    //                string sqlGetHeader = "select * from tbl_header header right join tbl_projedetay detay on header.proje_detay_id=detay.id where detay.proje_id=" + HttpContext.Current.Session["prjrefno"] + " order by detay.id asc";

    //                DataTable dtHeader = DbClassMysql.Fill2(sqlGetHeader);

    //                for (int i = 0; i < dtHeader.Rows.Count; i++)
    //                {

    //                    string calibration = dtHeader.Rows[i]["calibration_factor"].ToString();

    //                    string device = dtHeader.Rows[i]["device_type"].ToString();

    //                    string original_file_name = dtHeader.Rows[i]["original_file_name"].ToString();

    //                    string associated_file_name = dtHeader.Rows[i]["associated_file_name"].ToString();

    //                    string calibration_date = dtHeader.Rows[i]["calibration_date"].ToString();

    //                    string calibration_time = dtHeader.Rows[i]["calibration_time"].ToString();

    //                    string serial_no = dtHeader.Rows[i]["serial_no"].ToString();

    //                    if (dtHeader.Rows.Count - 1 > i)
    //                    {
    //                        header += calibration + "*" + device + "*" + original_file_name + "*" + associated_file_name + "*" + calibration_date + "*" + calibration_time + "*" + serial_no + "|";
    //                    }
    //                    else
    //                    {
    //                        header += calibration + "*" + device + "*" + original_file_name + "*" + associated_file_name + "*" + calibration_date + "*" + calibration_time + "*" + serial_no;
    //                    }
    //                }

    //                string sqlGetSonuc = "select * from tbl_sonuc sonuc right join tbl_projedetay detay on sonuc.proje_detay_id=detay.id where detay.proje_id=" + HttpContext.Current.Session["prjrefno"] + " order by detay.id asc";

    //                DataTable dtSonuc = DbClassMysql.Fill2(sqlGetSonuc);

    //                for (int i = 0; i < dtSonuc.Rows.Count; i++)
    //                {
    //                    DateTime test;
    //                    string elapsed_time;

    //                    if (DateTime.TryParse(Convert.ToString(dtSonuc.Rows[i]["elapsed_time"].ToString()), out test))
    //                    {
    //                        elapsed_time = dtSonuc.Rows[i]["elapsed_time"].ToString();
    //                    }
    //                    else
    //                    {
    //                        string[] olcum = Convert.ToString(dtSonuc.Rows[i]["elapsed_time"].ToString()).Split(':');

    //                        if (olcum[0].Trim() == "24")
    //                        {
    //                            elapsed_time = "01";
    //                        }
    //                        else
    //                        {
    //                            elapsed_time = olcum[0].ToString();
    //                        }

    //                        if (elapsed_time != "")
    //                        {
    //                            elapsed_time += ":" + olcum[1].ToString() + ":" + olcum[2].ToString();
    //                        }
    //                    }

    //                    string day = dtSonuc.Rows[i]["day"].ToString();

    //                    string hour = dtSonuc.Rows[i]["hour"].ToString();

    //                    string filter = dtSonuc.Rows[i]["filter"].ToString();

    //                    string detector = dtSonuc.Rows[i]["detector"].ToString();



    //                    string peak = dtSonuc.Rows[i]["peak"].ToString();

    //                    string max = dtSonuc.Rows[i]["max"].ToString();

    //                    string min = dtSonuc.Rows[i]["min"].ToString();

    //                    string leq = dtSonuc.Rows[i]["leq"].ToString();


    //                    if (dtSonuc.Rows.Count - 1 > i)
    //                    {
    //                        if (day == "")
    //                        {
    //                            sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq + "*" + "|";
    //                        }
    //                        else
    //                        {
    //                            if (filter == "Lin")
    //                            {
    //                                sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq + "|";
    //                            }
    //                            else
    //                            {
    //                                sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq + "*";
    //                            }
    //                        }
    //                    }
    //                    else
    //                    {
    //                        if (day == "")
    //                        {
    //                            sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq + "|";
    //                        }
    //                        else
    //                        {
    //                            sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq;
    //                        }
    //                    }
    //                }

    //                string sqlGetLn = "select * from tbl_ln ln right join tbl_projedetay detay on ln.proje_detay_id=detay.id where detay.proje_id=" + HttpContext.Current.Session["prjrefno"] + " order by detay.id asc";

    //                DataTable dtLn = DbClassMysql.Fill2(sqlGetLn);

    //                for (int i = 0; i < dtLn.Rows.Count; i++)
    //                {

    //                    string L10 = dtLn.Rows[i]["ln_10"].ToString();

    //                    string L90 = dtLn.Rows[i]["ln_90"].ToString();
    //                    if (dtLn.Rows.Count - 1 > i)
    //                    {
    //                        Ln += L10 + "*" + L90 + "|";
    //                    }
    //                    else
    //                    {
    //                        Ln += L10 + "*" + L90;
    //                    }


    //                }

    //                string sqlGetHava = "select * from tbl_hava hava right join tbl_projedetay detay on hava.proje_detay_id=detay.id where detay.proje_id=" + HttpContext.Current.Session["prjrefno"] + " order by detay.id asc";

    //                DataTable dtHava = DbClassMysql.Fill2(sqlGetHava);

    //                for (int i = 0; i < dtHava.Rows.Count; i++)
    //                {

    //                    string temp = dtHava.Rows[i]["temp"].ToString();

    //                    string ruzgar = dtHava.Rows[i]["ruzgar"].ToString();

    //                    string nem = dtHava.Rows[i]["nem"].ToString();

    //                    if (dtHava.Rows.Count - 1 > i)
    //                    {
    //                        hava += temp + "*" + ruzgar + "*" + nem + "|";
    //                    }
    //                    else
    //                    {
    //                        hava += temp + "*" + ruzgar + "*" + nem;
    //                    }
    //                }

    //                string sqlGetFoto = "select * from tbl_foto foto right join tbl_projedetay detay on foto.proje_detay_id=detay.id where detay.proje_id=" + HttpContext.Current.Session["prjrefno"] + " order by detay.id asc";

    //                DataTable dtFoto = DbClassMysql.Fill2(sqlGetFoto);

    //                for (int i = 0; i < dtFoto.Rows.Count; i++)
    //                {


    //                    string foto_adi = dtFoto.Rows[i]["fotograf"].ToString();
    //                    string foto_adi2 = "";
    //                    string ilk = "";
    //                    string ilk2 = "";
    //                    string[] bol = foto_adi.Split(']');

    //                    if (foto_adi == "")
    //                    {
    //                        fotograf += "" + "|";
    //                        continue;
    //                    }

    //                    if (!string.IsNullOrEmpty(bol[1]))
    //                    {
    //                        ilk = bol[1].Substring(0, (bol[1].Length - 4));
    //                    }
    //                    if (i != 0)
    //                    {
    //                        foto_adi2 = dtFoto.Rows[i - 1]["fotograf"].ToString();

    //                        string[] bol2 = foto_adi2.Split(']');



    //                        if (foto_adi2 != "")
    //                        {
    //                            if (!string.IsNullOrEmpty(bol2[1]))
    //                            {
    //                                ilk2 = bol2[1].Substring(0, (bol2[1].Length - 4));
    //                            }
    //                        }

    //                        if (ilk2 == ilk)
    //                        {
    //                            fotograf += "*" + foto_adi;
    //                        }
    //                        else
    //                        {
    //                            fotograf += "|" + foto_adi;
    //                        }
    //                    }
    //                    else
    //                    {
    //                        fotograf += foto_adi;
    //                    }

    //                }

    //            }
    //            else
    //            {
    //                HttpContext.Current.Response.Redirect("Default.aspx");
    //            }
    //        }
    //    }
    //    fotograf = fotograf.TrimEnd('|');
    //    sonuc = sonuc.TrimEnd('|');

    //    return pinler + "~" + header + "~" + sonuc + "~" + Ln + "~" + hava + "~" + fotograf;

    //    //return fotograf;
    //}

    #endregion bos_seyler

    public void xlsExport()
    {
        if (Session["activePrj"]==null)
        {
            Response.Redirect("Kapi/Default.aspx");
        }
        string sql = "select prj.proje_adi,p.pin_id,son.filter,son.day,h.device_type,h.original_file_name,son.elapsed_time,son.detector,son.leq,son.max,son.min,l.ln_10,l.ln_90,p.enlem,p.boylam,ha.temp,ha.ruzgar,ha.nem from tbl_sonuc son"; 
                sql +=" right join tbl_projedetay p on son.proje_detay_id=p.id ";
                sql +=" left join tbl_ln l on l.proje_detay_id=p.id ";
                sql +=" left join tbl_proje prj on prj.id=p.proje_id";
                sql +=" left join tbl_header h on h.proje_detay_id=p.id ";
                sql +=" left join tbl_hava ha on ha.proje_detay_id=p.id ";
                sql += " where p.proje_id=" + Session["activePrj"] + " order by p.tarih asc";

                DataTable dtRapor = db.Fill(sql);

        Random rnd = new Random();
        string random = "";
        for (int i = 0; i < 10; i++)
        {
            random += rnd.Next(0, 10);
        }

        string newFile = HttpContext.Current.Server.MapPath("~/TempFiles/") + @"rapor_"+random+".xlsx";

        //sablonu kopyala
        File.Copy(HttpContext.Current.Server.MapPath("~/sablon/") + @"rapor.xlsx", newFile);
        ExcelPackage pck;
        using (var fs = new FileStream(newFile,FileMode.Open))
        {
            pck = new ExcelPackage(fs);
            var ws = pck.Workbook.Worksheets[1];

        int sabit = 4;

        int no=0;

        int satir = 0;

        for (int i = 0; i < dtRapor.Rows.Count;)
        {
            ws.Cells[1, 2].Value = dtRapor.Rows[i]["proje_adi"].ToString(); // proje adı    

            if (string.IsNullOrEmpty(dtRapor.Rows[i]["filter"].ToString()))
            {
                no++;
                i++;
                continue;
            }
            else
            {
                no++;
                ws.Cells[sabit + satir, 1].Value = no;
                ws.Cells[sabit + satir, 2].Value = dtRapor.Rows[i]["day"].ToString(); // day
                ws.Cells[sabit + satir, 3].Value = dtRapor.Rows[i]["device_type"].ToString(); // cihaz_adi
                ws.Cells[sabit + satir, 4].Value = dtRapor.Rows[i]["original_file_name"].ToString(); // cihaz_adi
                ws.Cells[sabit + satir, 5].Value = dtRapor.Rows[i]["elapsed_time"].ToString(); // ölçüm süresi

                ws.Cells[sabit + satir, 6].Value = dtRapor.Rows[i]["enlem"].ToString(); // detector
                ws.Cells[sabit + satir, 7].Value = dtRapor.Rows[i]["boylam"].ToString(); // Aleq
                ws.Cells[sabit + satir, 8].Value = dtRapor.Rows[i]["temp"].ToString()+" °C"; // Aleq
                ws.Cells[sabit + satir, 9].Value = dtRapor.Rows[i]["nem"].ToString()+" mm/s"; // Aleq
                ws.Cells[sabit + satir, 10].Value = dtRapor.Rows[i]["ruzgar"].ToString()+" %"; // Aleq
                ws.Cells[sabit + satir, 11].Value = dtRapor.Rows[i]["detector"].ToString(); // Aleq
                                
                ws.Cells[sabit + satir, 12].Value = dtRapor.Rows[i]["leq"].ToString(); // Aleq
                ws.Cells[sabit + satir, 13].Value = dtRapor.Rows[i]["max"].ToString(); // Aleq
                ws.Cells[sabit + satir, 14].Value = dtRapor.Rows[i]["min"].ToString(); // Aleq
                ws.Cells[sabit + satir, 15].Value = dtRapor.Rows[i]["ln_10"].ToString(); // Aleq
                ws.Cells[sabit + satir, 16].Value = dtRapor.Rows[i]["ln_90"].ToString(); // Aleq

                ws.Cells[sabit + satir, 17].Value = dtRapor.Rows[i + 1]["detector"].ToString(); // detector
                ws.Cells[sabit + satir, 18].Value = dtRapor.Rows[i + 1]["leq"].ToString(); // Cleq
                ws.Cells[sabit + satir, 19].Value = dtRapor.Rows[i + 1]["max"].ToString(); // Aleq
                ws.Cells[sabit + satir, 20].Value = dtRapor.Rows[i + 1]["min"].ToString(); // Aleq
                ws.Cells[sabit + satir, 21].Value = dtRapor.Rows[i + 1]["ln_10"].ToString(); // Aleq
                ws.Cells[sabit + satir, 22].Value = dtRapor.Rows[i + 1]["ln_90"].ToString(); // Aleq
                i+=3;
                satir++;
            }

        }
        }   
                
        //***************************************************
        FileInfo file = new FileInfo(newFile);
        pck.SaveAs(Response.OutputStream);
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.AddHeader("content-disposition", "attachment;  filename=" + file.Name);
        Response.End();      
        
    }

    //protected void Button1_Click(object sender, EventArgs e)
    //{
    //    xlsExport();
    //}
    public class DataPoints{
    
        public DateTime x{get;set;}
        public float y{get;set;}
        

        public DataPoints(DateTime x,float y){
        
            this.x=x;
            this.y=y;
            
        }

        public DataPoints()
        {
        }

    }

    public class DataPointsColumn
    {

        public string label { get; set; }
        public double y { get; set; }


        public DataPointsColumn(string label, double y)
        {

            this.label = label;
            this.y = y;

        }

        public DataPointsColumn()
        {
        }

    }

    public class Logger {
        public string type { get; set; }
        public bool showInLegend { get; set; }
        public string legendText { get; set; }
        public string xValueType { get; set; }
        public string info { get; set; }
        public double Lday { get; set; }
        public double Leve { get; set; }
        public double Lnight { get; set; }
        public double Lden { get; set; }
        public List<DataPoints> dataPoints { get; set; }
    
    }

    public class _1_3_Octav
    {
        public string type { get; set; }
        public bool showInLegend { get; set; }
        public string indexLabel { get; set; }
        public string indexLabelOrientation { get; set; }
        public string indexLabelFontColor { get; set; }
        public List<DataPointsColumn> dataPoints { get; set; }

    }

    public class _1_1_Octav
    {
        public string type { get; set; }
        public bool showInLegend { get; set; }
        public string indexLabel { get; set; }
        public string xValueType { get; set; }
        public string indexLabelFontColor { get; set; }
        public List<DataPointsColumn> dataPoints { get; set; }

    }

    [WebMethod]
    public static object getLoggerChartData()
    {

        string sql = "select LEQ,Profil,SonucDate from tbl_slm_history where ProjeDetayID=" + HttpContext.Current.Session["maxDetayID"] + " order by SonucDate asc";

        DataTable dtPoints=DbClass.DbClassMysql.Fill2(sql);
        
        Logger lg = new Logger();
        
        ArrayList dataPoint = new ArrayList();
        //var dataSeries = { type: "line", showInLegend: true, legendText: "P1 LEQ" };

        for (int k = 0; k < 3; k++)
        {
           // dataPoint = new ArrayList();
            
            DataRow[] result={};
            lg = new Logger();
            switch (k)
            {
                case 0:
                    lg.type = "line";
                    lg.legendText = "P1 LEQ";
                    lg.showInLegend = true;
                    lg.xValueType = "dateTime";
                    
                    result = dtPoints.Select("Profil=1");            
                    break;
                case 1:
                    lg.type = "line";
                    lg.legendText = "P2 LEQ";
                    lg.showInLegend = true;
                    lg.xValueType = "dateTime";
                    result = dtPoints.Select("Profil=2");            
                    break;
                case 2:
                    lg.type = "line";
                    lg.legendText = "P3 LEQ";
                    lg.showInLegend = true;
                    lg.xValueType = "dateTime";
                    result = dtPoints.Select("Profil=3");            
                    break;
                default:
                    break;
            }
            
            List<DataPoints> pointDetail = new List<DataPoints>();
            int test = 0;
            foreach (DataRow row in result)
            {
                /*if (test<100)
                {
                    test++;*/

                    DataPoints p = new DataPoints();
                    p.x = Convert.ToDateTime(row["SonucDate"]);
                    p.y = Convert.ToSingle(row["LEQ"]);
                    


                    pointDetail.Add(p);



                    //dataPoint.Add(lg);

                    lg.dataPoints = pointDetail; 
               /* }
                else
                {
                    break;
                }*/

            }

            dataPoint.Add(lg);    
        }
 

        return new
        {
            dataPoints=dataPoint

        };
        //return fotograf;
    }

    [WebMethod]
    public static object get_1_3_Octav_Band(string pin_id)
    {

        string sql = "select *  from tbl_oktav_sonuc  octav inner join tbl_projedetay as detay on octav.ProjeDetayID=detay.id where detay.pin_id=" + pin_id + "  ";

        DataTable dtPoints = DbClass.DbClassMysql.Fill2(sql);

        _1_3_Octav lg = new _1_3_Octav();

        ArrayList dataPoint = new ArrayList();
        //var dataSeries = { type: "line", showInLegend: true, legendText: "P1 LEQ" };

        if (dtPoints.Rows.Count<1)
        {
            return null;
        }
            // dataPoint = new ArrayList();
        
            DataRow[] result = { };
            lg = new _1_3_Octav();

            lg.type = "column";
            lg.indexLabel = "{y}";
            lg.indexLabelOrientation = "horizontal";
            lg.indexLabelFontColor= "black";
            


                DataPointsColumn p = new DataPointsColumn();
                List<DataPointsColumn> pointDetail = new List<DataPointsColumn>();
                for (int i = 6; i < 37; i++)
                {


                    p = new DataPointsColumn();
                    p.y = Math.Round(Convert.ToSingle(dtPoints.Rows[0][i]),1);

                switch (i)
                {
                    case 6:
                        p.label = "20 Hz";
                        break;
                    case 7:
                        p.label = "25 Hz";
                        break;
                    case 8:
                        p.label = "31,5 Hz";
                        break;
                    case 9:
                        p.label = "40 Hz";
                        break;
                    case 10:
                        p.label = "50 Hz";
                        break;
                    case 11:
                        p.label = "63 Hz";
                        break;
                    case 12:
                        p.label = "80 Hz";
                        break;
                    case 13:
                        p.label = "100 Hz";
                        break;
                    case 14:
                        p.label = "125 Hz";
                        break;
                    case 15:
                        p.label = "160 Hz";
                        break;
                    case 16:
                        p.label = "200 Hz";
                        break;
                    case 17:
                        p.label = "250 Hz";
                        break;
                    case 18:
                        p.label = "315 Hz";
                        break;
                    case 19:
                        p.label = "400 Hz";
                        break;
                    case 20:
                        p.label = "500 Hz";
                        break;
                    case 21:
                        p.label = "630 Hz";
                        break;
                    case 22:
                        p.label = "800 Hz";
                        break;
                    case 23:
                        p.label = "1000 Hz";
                        break;
                    case 24:
                        p.label = "1250 Hz";
                        break;
                    case 25:
                        p.label = "1600 Hz";
                        break;
                    case 26:
                        p.label = "2000 Hz";
                        break;
                    case 27:
                        p.label = "2500 Hz";
                        break;
                    case 28:
                        p.label = "3150 Hz";
                        break;
                    case 29:
                        p.label = "4000 Hz";
                        break;
                    case 30:
                        p.label = "5000 Hz";
                        break;
                    case 31:
                        p.label = "6300 Hz";
                        break;
                    case 32:
                        p.label = "8000 Hz";
                        break;
                    case 33:
                        p.label = "10000 Hz";
                        break;
                    case 34:
                        p.label = "12500 Hz";
                        break;
                    case 35:
                        p.label = "16000 Hz";
                        break;
                    case 36:
                        p.label = "20000 Hz";
                        break;
                    default:
                        break;
                }
                //switch (switch_on)
                //{
                //    default:
                //}
                //p.y = Convert.ToSingle(row["LEQ"]);



                pointDetail.Add(p);



                //dataPoint.Add(lg);

                lg.dataPoints = pointDetail;
                }

            

            dataPoint.Add(lg);
        

        //string json = JsonConvert.SerializeObject(dataPoint);


        return new
        {
            dataPoints = dataPoint

        };
        //return fotograf;
    }

    [WebMethod]
    public static object get_1_1_Octav_Band(string pin_id)
    {
        /*
          log((1/n)x(10ussu(spl/10)+10ussu(spl2/10)+...+(10ussu(spln/10))
          n saatlik veri sayisi
         */

        string sql = "select ProjeDetayID,DbHz25,DbHz31Half,DbHz40,DbHz50,DbHz63,DbHz80,DbHz100,DbHz125,DbHz160,DbHz200,DbHz250,DbHz315,DbHz400,DbHz500,DbHz630,DbHz800,DbHz1000,DbHz1250,DbHz1600,DbHz2000,DbHz2500,DbHz3150,DbHz4000,DbHz5000,DbHz6300,DbHz8000,DbHz10000 from tbl_oktav_sonuc  octav inner join tbl_projedetay as detay on octav.ProjeDetayID=detay.id where detay.pin_id=" + pin_id + "  ";

        DataTable dtPoints = DbClass.DbClassMysql.Fill2(sql);

        _1_3_Octav lg = new _1_3_Octav();


        ArrayList dataPoint = new ArrayList();

        if (dtPoints.Rows.Count<1)
        {
            return null;
        }


        //var dataSeries = { type: "line", showInLegend: true, legendText: "P1 LEQ" };


        // dataPoint = new ArrayList();

        DataRow[] result = { };
        lg = new _1_3_Octav();

        lg.type = "column";
        lg.indexLabel = "{y}";
        lg.indexLabelOrientation = "horizontal";
        lg.indexLabelFontColor = "black";



        DataPointsColumn p = new DataPointsColumn();
        
        List<DataPointsColumn> pointDetail = new List<DataPointsColumn>();

        for (int i = 1; i < 10; i++)
        {
            //1/1 31,5 Hz = 10*log(10^(25hz/10)+10^(31,5hz/10)+10^(40hz/10))

            p = new DataPointsColumn();

            double logValue;

            switch (i)
            {
                case 1:
                    p.label = "31,5 Hz";

                    logValue = Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz25"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz31Half"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz40"]) / 10));

                    p.y = Math.Round((10 * (Math.Log(logValue, 10))),1);

                    break;
                case 2:
                    p.label = "63 Hz";

                    logValue = Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz50"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz63"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz80"]) / 10));

                    p.y = Math.Round((10 * (Math.Log(logValue, 10))),1);

                    break;
                case 3:
                    p.label = "125 Hz";
                    
                    logValue = Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz100"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz125"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz160"]) / 10));

                    p.y = Math.Round((10 * (Math.Log(logValue, 10))),1);

                    break;
                case 4:
                    p.label = "250 Hz";

                    logValue = Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz200"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz250"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz315"]) / 10));

                    p.y = Math.Round((10 * (Math.Log(logValue, 10))),1);

                    break;
                case 5:
                    p.label = "500 Hz";

                    logValue = Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz400"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz500"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz630"]) / 10));

                    p.y = Math.Round((10 * (Math.Log(logValue, 10))),1);

                    break;
                case 6:
                    p.label = "1000 Hz";

                    logValue = Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz800"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz1000"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz1250"]) / 10));

                    p.y = Math.Round((10 * (Math.Log(logValue, 10))),1);

                    break;
                case 7:
                    p.label = "2000 Hz";

                    logValue = Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz1600"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz2000"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz3150"]) / 10));

                    p.y = Math.Round((10 * (Math.Log(logValue, 10))),1);

                    break;
                case 8:
                    p.label = "4000 Hz";

                    logValue = Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz3150"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz4000"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz5000"]) / 10));

                    p.y = Math.Round((10 * (Math.Log(logValue, 10))),1);

                    break;
                case 9:
                    p.label = "8000 Hz";

                    logValue = Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz6300"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz8000"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz10000"]) / 10));

                    p.y = Math.Round((10 * (Math.Log(logValue, 10))),1);

                    break;
                default:
                    break;
            }
            //switch (switch_on)
            //{
            //    default:
            //}
            //p.y = Convert.ToSingle(row["LEQ"]);



            pointDetail.Add(p);



            //dataPoint.Add(lg);

            lg.dataPoints = pointDetail;
        }



        dataPoint.Add(lg);


        //string json = JsonConvert.SerializeObject(dataPoint);


        return new
        {
            dataPoints = dataPoint

        };
        //return fotograf;
    }

    private static double LCalculate(string session,DataTable dt){

        var query=dt.AsEnumerable();

        double avg=0, log=0;

        switch (session)
        {
            case "Lday":

                if (query.Where(x => Convert.ToDateTime(x["SonucDate"]).Hour > 7 && Convert.ToDateTime(x["SonucDate"]).Hour < 19).Count() > 0)
                {
                    avg = query.Where(x => Convert.ToDateTime(x["SonucDate"]).Hour > 7 && Convert.ToDateTime(x["SonucDate"]).Hour < 19).Average(x => Math.Pow(10, (x.Field<double>("LEQ")) / 10)); 

                    log = 10 * Math.Log(avg, 10);
                }

                return log;
                
            case "Leve":
                
                if (query.Where(x => Convert.ToDateTime(x["SonucDate"]).Hour > 19 && Convert.ToDateTime(x["SonucDate"]).Hour < 23).Count() > 0)
                {
                    avg = query.Where(x => Convert.ToDateTime(x["SonucDate"]).Hour > 19 && Convert.ToDateTime(x["SonucDate"]).Hour < 23).Average(x => Math.Pow(10, (x.Field<double>("LEQ")) / 10)); 

                    log = 10 * Math.Log(avg, 10);
                }

                return log;
                
            case "Lnight":
                if (query.Where(x => Convert.ToDateTime(x["SonucDate"]).Hour > 23 && Convert.ToDateTime(x["SonucDate"]).Hour < 7).Count() > 0)
                {
                    avg = query.Where(x => Convert.ToDateTime(x["SonucDate"]).Hour > 23 && Convert.ToDateTime(x["SonucDate"]).Hour < 7).Average(x => Math.Pow(10, (x.Field<double>("LEQ")) / 10)); 

                    log = 10 * Math.Log(avg, 10);
                }

                return log;
                
            case "Lden":
                return 0;
                
            default:
                return 0;
                
        }

    }

    private static double LDen(double lday,double leve,double lnight)
    {

        double avg = (0.5 * Math.Pow(10, (lday / 10))) + ((4/24) * Math.Pow(10, (leve / 10))) + ((8/24) * Math.Pow(10, (lnight / 10)));     

        double log = 10 * Math.Log(avg, 10);

        return log;

    }

    [WebMethod]
    public static object getLoggerChartDataID(string pin_id)
    {

        //string sql = "select SPL,SonucDate,LEQ,Profil from tbl_slm_history slm inner join tbl_projedetay as detay on slm.ProjeDetayID=detay.id where detay.pin_id=" + pin_id + " order by SonucDate asc";

        string sql = "SELECT slm.ProjeDetayID,slm.profil,detay.pin_id, " +
        "case when datepart(mi,SonucDate)<1  " +
        "then dateadd(mi, datediff(mi, 0, SonucDate)+0, 0) else dateadd(mi,1,dateadd(mi,datediff(mi,0,SonucDate)+0,0)) end as SonucDate," +
        "Round(AVG(slm.LEQ),2) AS LEQ,Round(AVG(slm.SPL),2) AS SPL from tbl_slm_history  " +
        "slm inner join tbl_projedetay as detay on slm.ProjeDetayID=detay.id " +
        "where detay.pin_id=" + pin_id + " and slm.Profil in(1,2,3)  " +
        "GROUP BY slm.Profil,slm.ProjeDetayID,detay.pin_id, " +
        "case when datepart(mi,SonucDate)<1  then dateadd(mi, datediff(mi, 0, SonucDate)+0, 0) else dateadd(mi,1,dateadd(mi,datediff(mi,0,SonucDate)+0,0))" +
        " end order by SonucDate,slm.profil";

        DataTable dtPoints = DbClass.DbClassMysql.Fill2(sql);

        Logger lg = new Logger();


        if (dtPoints.Rows.Count==0)
        {
            return 0;
        }

        
        double lday = Math.Round(LCalculate("Lday", dtPoints),2);

        double leve = Math.Round(LCalculate("Leve", dtPoints),2);

        double lnight = Math.Round(LCalculate("Lnight", dtPoints),2);

        double lden =Math.Round( LDen(lday, leve, lnight),2);
        


        ArrayList dataPoint = new ArrayList();
        //var dataSeries = { type: "line", showInLegend: true, legendText: "P1 LEQ" };

        
        for (int k = 0; k < 3; k++)
        {
            // dataPoint = new ArrayList();

            lg = new Logger();

            DataRow[] result = { };    
            

            lg.Lday = lday;
            lg.Leve = leve;
            lg.Lnight = lnight;
            lg.Lden = lden;
            bool durum = false;
            
            List<DataPoints> pointDetail = new List<DataPoints>();
            
            DataPoints p = new DataPoints();

            switch (k)
            {
                case 0:
                    lg.type = "line";
                    lg.legendText = "dBA";
                    lg.showInLegend = true;
                    lg.xValueType = "dateTime";
                    durum = true;
                    result = dtPoints.Select("Profil=1");
                    lg.info = result[0]["pin_id"].ToString();
                    break;
                case 1:
                    p = new DataPoints();

                    lg.type = "line";
                    lg.legendText = "dBC";
                    lg.showInLegend = true;
                    durum = true;
                    lg.xValueType = "dateTime";
                    result = dtPoints.Select("Profil=2");
                    lg.info = result[0]["pin_id"].ToString();
                    break;
                case 2:
                    p = new DataPoints();
                    lg.type = "line";
                    lg.legendText = "dBZ";
                    lg.showInLegend = true;
                    durum = true;
                    lg.xValueType = "dateTime";
                    result = dtPoints.Select("Profil=3");
                    lg.info = result[0]["pin_id"].ToString();
                    break;
                default:
                    break;
            }

            
            
            foreach (DataRow row in result)
            {
                /*if (test<100)
                {
                    test++;*/

                p = new DataPoints();

                if (durum)
                {
                    p.x = Convert.ToDateTime(row["SonucDate"]);
                    p.y = Convert.ToSingle(row["SPL"]); 
                }
                


                if (durum)
                {
                    pointDetail.Add(p);

                    //dataPoint.Add(lg);

                    lg.dataPoints = pointDetail;  
                }
                
                /* }
                 else
                 {
                     break;
                 }*/

            }

            dataPoint.Add(lg);
        }


        return new
        {
            dataPoints = dataPoint

        };
        //return fotograf;
    }

    [WebMethod]
    public static void UploadImage(string imageData)
    {

        string fileNameWitPath = HttpContext.Current.Server.MapPath(DateTime.Now.ToString().Replace("/", "-").Replace(" ", "- ").Replace(":", "") + ".png");

        using (FileStream fs = new FileStream(fileNameWitPath, FileMode.Create))
        {

            using (BinaryWriter bw = new BinaryWriter(fs))
            {

                byte[] data = Convert.FromBase64String(imageData);

                bw.Write(data);

                bw.Close();

            }

        }

    }

    [WebMethod]
    public static object getMeasureRange(string pin_id) {


        if (HttpContext.Current.Session["prjrefno"] == null)
        {
            return 0;
        }

        string sqlRange = "";

        string range = Convert.ToString(DbClass.DbClassMysql.ScalarQuery2("select measure_range from tbl_proje where id=" + HttpContext.Current.Session["prjrefno"]));

        switch (range)
        {
            case "h":
                sqlRange = "SELECT slm.profil,"+ 
                            " DATEADD(hh,DATEDIFF(hh,0,SonucDate),0) AS start_time,"+
                            " DATEADD(hh,DATEDIFF(hh,0,SonucDate),0) + '01:00:00' AS end_time,"+
                            " Round(AVG(slm.LEQ),2) AS average"+
                            " FROM tbl_slm_history  slm inner join tbl_projedetay as detay on slm.ProjeDetayID=detay.id where detay.pin_id="+pin_id+" "+
                            " and slm.Profil in(1,3) "+
                            " GROUP BY slm.Profil, DATEADD(hh,DATEDIFF(hh,0,SonucDate),0)"+
                            " ORDER BY start_time,slm.profil";
                break;
            case "hh":
                sqlRange = "SELECT "+
                              "slm.profil,"+
                              " case when datepart(mi,SonucDate)<30 "+
                              " then dateadd(hh, datediff(hh, 0, SonucDate)+0, 0)"+
                              " else"+
		                            " dateadd(mi,30,dateadd(hh,datediff(hh,0,SonucDate)+0,0)) end as start_time,"+
                              " case when datepart(mi,SonucDate)<30 then dateadd(mi,30,dateadd(hh,datediff(hh,0,SonucDate)+0,0))"+
                              " else"+
		                            " dateadd(hh,1,dateadd(hh,datediff(hh,0,SonucDate)+0,0)) end as end_time,Round(AVG(slm.LEQ),2) AS average"+
		                            " from tbl_slm_history  slm inner join tbl_projedetay as detay on slm.ProjeDetayID=detay.id where detay.pin_id="+pin_id+""+
		                            " and slm.Profil in(1,3) "+
                              " GROUP BY slm.Profil, case when datepart(mi,SonucDate)<30 "+
                              " then dateadd(hh, datediff(hh, 0, SonucDate)+0, 0)"+
                              " else"+
		                            " dateadd(mi,30,dateadd(hh,datediff(hh,0,SonucDate)+0,0)) end,"+
                              " case when datepart(mi,SonucDate)<30 then dateadd(mi,30,dateadd(hh,datediff(hh,0,SonucDate)+0,0))"+
                              " else"+
		                            " dateadd(hh,1,dateadd(hh,datediff(hh,0,SonucDate)+0,0)) end"+
	                            " order by start_time,slm.profil";
                break;
                default:
                    break;
        }


        DataTable dt = DbClass.DbClassMysql.Fill2(sqlRange);

        ArrayList getMeasure = new ArrayList();
        
        string date="", start_time="", end_time="", p1="", p2="";

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["profil"].ToString()=="1")
            {
                date = Convert.ToDateTime(dt.Rows[i]["start_time"]).ToString("dd/MM/yyyy");

                start_time = Convert.ToDateTime(dt.Rows[i]["start_time"]).ToString("HH:mm");

                end_time = Convert.ToDateTime(dt.Rows[i]["end_time"]).ToString("HH:mm");

                p1 = dt.Rows[i]["average"].ToString(); 
            }
            else if (dt.Rows[i]["profil"].ToString() == "3")
            {
                p2 = dt.Rows[i]["average"].ToString();

                getMeasure.Add(new string[] { date, start_time, end_time, p1, p2 });
            }
        }

        return new
        {
            getMeasureRange=getMeasure
        };

        
    }
}