﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DbClass;
using System.Web.Services;
using System.Data;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Data.OleDb;
using System.IO;
public partial class _Default : System.Web.UI.Page
{
    DbClassMysql db = new DbClassMysql();

    protected void Page_Load(object sender, EventArgs e)
    {
        //string test = pinOlustur2();

        if (Session["yetki"]!=null)
        {

            if (Session["KullaniciRefno"]!=null)
            {

                string sqlFirmaadi = Convert.ToString(db.ScalarQuery("select isnull(NULLIF(musteri_adi,''),kullanici_adi) as ad from tbl_kullanici where id=" + Session["KullaniciRefno"].ToString()));

                hybMer.Text = "Hoşgeldiniz <b>" + sqlFirmaadi + "</b>";
                string sqlPrjler;

                switch (Convert.ToInt32(Session["yetki"]))
                {
                    case 1:
                        sqlPrjler = "select * from tbl_proje where durum=1 order by tarih desc";
                        break;
                    case 2:
                        sqlPrjler = "select * from tbl_proje where durum=1 and firma_id=" + Session["KullaniciRefno"].ToString() + "order by tarih desc";
                        break;
                    case 3:
                        sqlPrjler = "select * from tbl_proje where durum=1 order by tarih desc";
                        break;
                    default:
                        sqlPrjler = "select * from tbl_proje where durum=1 order by tarih desc";
                        break;
                }
                
                

                DataTable dtPrj = db.Fill(sqlPrjler);
                                
                for (int i = 0; i < dtPrj.Rows.Count; i++)
                {
                    pnlprj.Controls.Add(new LiteralControl("<li class='has-sub'><a id=" + dtPrj.Rows[i]["id"].ToString() + "_" + i + " href='Default.aspx?prjrefno=" + dtPrj.Rows[i]["id"].ToString() + "'><span>" + dtPrj.Rows[i]["proje_adi"].ToString() + "</span></a></li>"));

                }
                
                if (Request["prjrefno"] == null)
                {
                    Session["prjrefno"] = null;
                    string sqlPrjAdi = "";
                    DataTable dtPin=new DataTable();
                    if (dtPrj.Rows.Count != 0)
                    {
                        Session["activePrj"] = dtPrj.Rows[0]["id"].ToString();
                    
                    string sqlNoktalar = "select * from tbl_projedetay where proje_id=" + dtPrj.Rows[0]["id"].ToString() + " order by sira asc";

                    dtPin = db.Fill(sqlNoktalar);
                    
                    sqlPrjAdi = Convert.ToString(db.ScalarQuery("select proje_adi from tbl_proje where id=" + dtPrj.Rows[0]["id"].ToString()));
                    }


                    if (sqlPrjAdi!="")
                    {
                        hypPro.Text = "Seçilen proje | <b>" + sqlPrjAdi + "</b> |"; 
                    }
                    else
                    {
                        hypPro.Text = "Herhangi bir proje bulunamadı"; 
                    }

                    for (int k = 0; k < dtPin.Rows.Count; k++)
                    {
                        pnlPin.Controls.Add(new LiteralControl("<li><a class='pins' id=p_" + dtPin.Rows[k]["pin_id"] + " href='#'>" + dtPin.Rows[k]["sira"] + ".Nokta</a></li>"));
                    }
                }
                else
                {
                    Session["prjrefno"] = Request["prjrefno"];
                    Session["activePrj"] = Request["prjrefno"];
                    int control = Convert.ToInt32(db.ScalarQuery("select * from tbl_proje where id=" + Convert.ToInt32(Request["prjrefno"]) + " and firma_id=" + Convert.ToInt32(Session["KullaniciRefno"])));
                    switch (Convert.ToInt32(HttpContext.Current.Session["yetki"]))
                    {
                        case 1:
                            control = 10;
                            break;
                        case 2:

                            break;
                        case 3:
                            control = 10;
                            break;
                        default:
                            control = 10;
                            break;
                    }
                    if (control>0)
                    {
                        string sqlNoktalar = "select * from tbl_projedetay where proje_id=" + Request["prjrefno"] + " order by sira asc";

                        string sqlPrjAdi = Convert.ToString(db.ScalarQuery("select proje_adi from tbl_proje where id=" + Request["prjrefno"].ToString()));

                        hypPro.Text = "Seçilen proje | <b>" + sqlPrjAdi + "</b> |";

                        DataTable dtPin = db.Fill(sqlNoktalar);

                        for (int k = 0; k < dtPin.Rows.Count; k++)
                        {
                            

                            pnlPin.Controls.Add(new LiteralControl("<li><a class='pins' id=p_" + dtPin.Rows[k]["pin_id"] + " href='#'>" + dtPin.Rows[k]["sira"] + ".Nokta</a></li>"));
                        } 
                    }
                    else
                    {
                        Response.Redirect("Default.aspx");
                    }
                }

            }
            
        }
        else
        {
            Response.Redirect("Kapi/Default.aspx");
        }
        
    }

    [WebMethod]
    public static string pinOlustur()
    {
        
        string pinler = "";
        string header = "";
        string sonuc = "";
        string Ln = "";
        string hava = "";
        string fotograf = "";
        string xls_dosyasi = "";
        string pin_id = "";
       try
        {
            #region kod
            if (HttpContext.Current.Session["KullaniciRefno"] != null)
            {

                if (HttpContext.Current.Session["prjrefno"] == null)
                {
                    string sqlGetPin;

                    string kulNo = HttpContext.Current.Session["KullaniciRefno"].ToString();

                    int projeID = Convert.ToInt32(DbClassMysql.ScalarQuery2("select id from tbl_proje where firma_id=" + kulNo + " and durum=1 order by tarih desc"));

                    if (projeID == 0)
                    {
                        projeID = Convert.ToInt32(DbClassMysql.ScalarQuery2("select id from tbl_proje order by tarih desc"));
                    }

                    sqlGetPin = "SELECT * FROM tbl_projedetay where proje_id=" + projeID + " order by tarih asc";

                    DataTable dtGetPin = DbClassMysql.Fill2(sqlGetPin);

                    for (int i = 0; i < dtGetPin.Rows.Count; i++)
                    {
                        string enlem = dtGetPin.Rows[i]["enlem"].ToString();

                        string boylam = dtGetPin.Rows[i]["boylam"].ToString();

                        string zoom = dtGetPin.Rows[i]["zoom"].ToString();

                        pin_id = dtGetPin.Rows[i]["pin_id"].ToString();

                        string aciklama = dtGetPin.Rows[i]["aciklama"].ToString();

                        string proje_detay_id = dtGetPin.Rows[i]["id"].ToString();

                        string sira = dtGetPin.Rows[i]["sira"].ToString();


                        if (dtGetPin.Rows.Count - 1 > i)
                        {
                            pinler += enlem + "*" + boylam + "*" + zoom + "*" + pin_id + "*" + aciklama + "*" + sira + "|";
                        }
                        else
                        {
                            pinler += enlem + "*" + boylam + "*" + zoom + "*" + pin_id + "*" + aciklama + "*" + sira;
                        }
                    }

                    string sqlGetHeader = "select * from tbl_header header right join tbl_projedetay detay on header.proje_detay_id=detay.id where detay.proje_id=" + projeID + " order by detay.id asc";

                    DataTable dtHeader = DbClassMysql.Fill2(sqlGetHeader);

                    for (int i = 0; i < dtHeader.Rows.Count; i++)
                    {

                        string calibration = dtHeader.Rows[i]["calibration_factor"].ToString();

                        string device = dtHeader.Rows[i]["device_type"].ToString();

                        string original_file_name = dtHeader.Rows[i]["original_file_name"].ToString();

                        string associated_file_name = dtHeader.Rows[i]["associated_file_name"].ToString();

                        string calibration_date = dtHeader.Rows[i]["calibration_date"].ToString();

                        string calibration_time = dtHeader.Rows[i]["calibration_time"].ToString();

                        string serial_no = dtHeader.Rows[i]["serial_no"].ToString();

                        if (dtHeader.Rows.Count - 1 > i)
                        {
                            header += calibration + "*" + device + "*" + original_file_name + "*" + associated_file_name + "*" + calibration_date + "*" + calibration_time + "*" + serial_no + "|";
                        }
                        else
                        {
                            header += calibration + "*" + device + "*" + original_file_name + "*" + associated_file_name + "*" + calibration_date + "*" + calibration_time + "*" + serial_no;
                        }
                    }

                    string sqlGetSonuc = "select * from tbl_sonuc sonuc right join tbl_projedetay detay on sonuc.proje_detay_id=detay.id where detay.proje_id=" + projeID + " order by detay.id asc";

                    DataTable dtSonuc = DbClassMysql.Fill2(sqlGetSonuc);

                    for (int i = 0; i < dtSonuc.Rows.Count; i++)
                    {
                        DateTime test;
                        string elapsed_time;
                        pin_id = dtSonuc.Rows[i]["sira"].ToString();
                        xls_dosyasi = dtSonuc.Rows[i]["xls_dosya"].ToString();

                        if (DateTime.TryParse(Convert.ToString(dtSonuc.Rows[i]["elapsed_time"].ToString()), out test))
                        {
                            elapsed_time = dtSonuc.Rows[i]["elapsed_time"].ToString();
                        }
                        else
                        {
                            string[] olcum = Convert.ToString(dtSonuc.Rows[i]["elapsed_time"].ToString()).Split(':');

                            if (olcum[0].Trim() == "24")
                            {
                                //elapsed_time = "01";
                                elapsed_time = "24";
                            }
                            else
                            {
                                elapsed_time = olcum[0].ToString();
                            }

                            if (elapsed_time != "")
                            {
                                elapsed_time += ":" + olcum[1].ToString() + ":" + olcum[2].ToString();
                               // throw new SystemException(""+i+"");
                            }
                        }

                        string day = dtSonuc.Rows[i]["day"].ToString();

                        string hour = dtSonuc.Rows[i]["hour"].ToString();

                        string filter = dtSonuc.Rows[i]["filter"].ToString();

                        string detector = dtSonuc.Rows[i]["detector"].ToString();

                        string peak = dtSonuc.Rows[i]["peak"].ToString();

                        string max = dtSonuc.Rows[i]["max"].ToString();

                        string min = dtSonuc.Rows[i]["min"].ToString();

                        string leq = dtSonuc.Rows[i]["leq"].ToString();

                        if (dtSonuc.Rows.Count - 1 > i)
                        {
                            if (day == "")
                            {
                                sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq + "*" + "|";
                            }
                            else
                            {
                                if (filter == "Lin")
                                {
                                    sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq + "|";
                                }
                                else
                                {
                                    sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq + "*";
                                }
                            }
                        }
                        else
                        {
                            if (day == "")
                            {
                                sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq + "|";
                            }
                            else
                            {
                                sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq;
                            }
                        }
                    }

                    string sqlGetLn = "select * from tbl_ln ln right join tbl_projedetay detay on ln.proje_detay_id=detay.id where detay.proje_id=" + projeID + " order by detay.id asc";

                    DataTable dtLn = DbClassMysql.Fill2(sqlGetLn);

                    for (int i = 0; i < dtLn.Rows.Count; i++)
                    {
                        pin_id = dtLn.Rows[i]["sira"].ToString();
                        xls_dosyasi = dtLn.Rows[i]["xls_dosya"].ToString();

                        string L10 = dtLn.Rows[i]["ln_10"].ToString();

                        string L90 = dtLn.Rows[i]["ln_90"].ToString();

                        if (dtLn.Rows.Count - 1 > i)
                        {
                            Ln += L10 + "*" + L90 + "|";
                        }
                        else
                        {
                            Ln += L10 + "*" + L90;
                        }


                    }

                    string sqlGetHava = "select * from tbl_hava hava right join tbl_projedetay detay on hava.proje_detay_id=detay.id where detay.proje_id=" + projeID + " order by detay.id asc";

                    DataTable dtHava = DbClassMysql.Fill2(sqlGetHava);

                    for (int i = 0; i < dtHava.Rows.Count; i++)
                    {
                        string temp = dtHava.Rows[i]["temp"].ToString();

                        string ruzgar = dtHava.Rows[i]["ruzgar"].ToString();

                        string nem = dtHava.Rows[i]["nem"].ToString();

                        if (dtHava.Rows.Count - 1 > i)
                        {
                            hava += temp + "*" + ruzgar + "*" + nem + "|";
                        }
                        else
                        {
                            hava += temp + "*" + ruzgar + "*" + nem;
                        }
                    }

                    string sqlGetFoto = "select * from tbl_foto foto right join tbl_projedetay detay on foto.proje_detay_id=detay.id where detay.proje_id=" + projeID + " order by detay.id asc";

                    DataTable dtFoto = DbClassMysql.Fill2(sqlGetFoto);

                    for (int i = 0; i < dtFoto.Rows.Count; i++)
                    {
                        string foto_adi = dtFoto.Rows[i]["fotograf"].ToString();
                        string foto_adi2 = "";
                        string ilk = "";
                        string ilk2 = "";
                        string[] bol = foto_adi.Split(']');

                        if (foto_adi == "")
                        {
                            fotograf += "" + "|";
                            continue;
                        }

                        if (!string.IsNullOrEmpty(bol[1]))
                        {
                            ilk = bol[1].Substring(0, (bol[1].Length - 4));
                        }
                        if (i != 0)
                        {
                            foto_adi2 = dtFoto.Rows[i - 1]["fotograf"].ToString();

                            string[] bol2 = foto_adi2.Split(']');

                            if (foto_adi2 != "")
                            {
                                if (!string.IsNullOrEmpty(bol2[1]))
                                {
                                    ilk2 = bol2[1].Substring(0, (bol2[1].Length - 4));
                                }
                            }

                            if (ilk2 == ilk)
                            {
                                if (fotograf.Substring(0, fotograf.Length - 1).IndexOf('|') != -1)
                                {
                                    //fotograf = fotograf.Substring(0, fotograf.Length - 1);    
                                }
                                fotograf += "*" + foto_adi;
                            }
                            else
                            {
                                fotograf += "|" + foto_adi;
                            }
                        }
                        else
                        {
                            fotograf += foto_adi;
                        }

                    }

                }
                else
                {
                    int prjRefno = Convert.ToInt32(HttpContext.Current.Session["prjrefno"]);
                    int firmaRefno = Convert.ToInt32(HttpContext.Current.Session["KullaniciRefno"]);
                    int control = Convert.ToInt32(DbClassMysql.ScalarQuery2("select * from tbl_proje where id=" + prjRefno + " and firma_id=" + firmaRefno + " and durum=1"));
                    switch (Convert.ToInt32(HttpContext.Current.Session["yetki"]))
                    {
                        case 1:
                            control = 10;
                            break;
                        case 2:

                            break;
                        case 3:
                            control = 10;
                            break;
                        default:
                            control = 10;
                            break;
                    }

                    if (control > 0)
                    {

                        string sqlGetPin = "SELECT * FROM tbl_projedetay where proje_id=" + HttpContext.Current.Session["prjrefno"] + " order by tarih asc";

                        DataTable dtGetPin = DbClassMysql.Fill2(sqlGetPin);

                        for (int i = 0; i < dtGetPin.Rows.Count; i++)
                        {
                            string enlem = dtGetPin.Rows[i]["enlem"].ToString();

                            string boylam = dtGetPin.Rows[i]["boylam"].ToString();

                            string zoom = dtGetPin.Rows[i]["zoom"].ToString();

                            pin_id = dtGetPin.Rows[i]["pin_id"].ToString();

                            string aciklama = dtGetPin.Rows[i]["aciklama"].ToString();

                            string proje_detay_id = dtGetPin.Rows[i]["id"].ToString();

                            string sira = dtGetPin.Rows[i]["sira"].ToString();

                            if (dtGetPin.Rows.Count - 1 > i)
                            {
                                pinler += enlem + "*" + boylam + "*" + zoom + "*" + pin_id + "*" + aciklama + "*" + sira + "|";
                            }
                            else
                            {
                                pinler += enlem + "*" + boylam + "*" + zoom + "*" + pin_id + "*" + aciklama + "*" + sira;
                            }

                        }

                        string sqlGetHeader = "select * from tbl_header header right join tbl_projedetay detay on header.proje_detay_id=detay.id where detay.proje_id=" + HttpContext.Current.Session["prjrefno"] + " order by detay.id asc";

                        DataTable dtHeader = DbClassMysql.Fill2(sqlGetHeader);

                        for (int i = 0; i < dtHeader.Rows.Count; i++)
                        {

                            string calibration = dtHeader.Rows[i]["calibration_factor"].ToString();

                            string device = dtHeader.Rows[i]["device_type"].ToString();

                            string original_file_name = dtHeader.Rows[i]["original_file_name"].ToString();

                            string associated_file_name = dtHeader.Rows[i]["associated_file_name"].ToString();

                            string calibration_date = dtHeader.Rows[i]["calibration_date"].ToString();

                            string calibration_time = dtHeader.Rows[i]["calibration_time"].ToString();

                            string serial_no = dtHeader.Rows[i]["serial_no"].ToString();

                            if (dtHeader.Rows.Count - 1 > i)
                            {
                                header += calibration + "*" + device + "*" + original_file_name + "*" + associated_file_name + "*" + calibration_date + "*" + calibration_time + "*" + serial_no + "|";
                            }
                            else
                            {
                                header += calibration + "*" + device + "*" + original_file_name + "*" + associated_file_name + "*" + calibration_date + "*" + calibration_time + "*" + serial_no;
                            }
                        }

                        string sqlGetSonuc = "select * from tbl_sonuc sonuc right join tbl_projedetay detay on sonuc.proje_detay_id=detay.id where detay.proje_id=" + HttpContext.Current.Session["prjrefno"] + " order by detay.id asc";

                        DataTable dtSonuc = DbClassMysql.Fill2(sqlGetSonuc);

                        for (int i = 0; i < dtSonuc.Rows.Count; i++)
                        {
                            DateTime test;
                            string elapsed_time;
                            pin_id = dtSonuc.Rows[i]["sira"].ToString();
                            xls_dosyasi = dtSonuc.Rows[i]["xls_dosya"].ToString();
                            if (DateTime.TryParse(Convert.ToString(dtSonuc.Rows[i]["elapsed_time"].ToString()), out test))
                            {
                                elapsed_time = dtSonuc.Rows[i]["elapsed_time"].ToString();
                            }
                            else
                            {
                                string[] olcum = Convert.ToString(dtSonuc.Rows[i]["elapsed_time"].ToString()).Split(':');

                                if (olcum[0].Trim() == "24")
                                {
                                    elapsed_time = "24";
                                }
                                else
                                {
                                    elapsed_time = olcum[0].ToString();
                                }

                                if (elapsed_time != "")
                                {
                                
                                        elapsed_time += ":" + olcum[1].ToString() + ":" + olcum[2].ToString(); 
                                    

                                    //throw new SystemException("" + i + "");
                                }
                                
                            }

                            string day = dtSonuc.Rows[i]["day"].ToString();

                            string hour = dtSonuc.Rows[i]["hour"].ToString();

                            string filter = dtSonuc.Rows[i]["filter"].ToString();

                            string detector = dtSonuc.Rows[i]["detector"].ToString();



                            string peak = dtSonuc.Rows[i]["peak"].ToString();

                            string max = dtSonuc.Rows[i]["max"].ToString();

                            string min = dtSonuc.Rows[i]["min"].ToString();

                            string leq = dtSonuc.Rows[i]["leq"].ToString();


                            if (dtSonuc.Rows.Count - 1 > i)
                            {
                                if (day == "")
                                {
                                    sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq + "*" + "|";
                                }
                                else
                                {
                                    if (filter == "Lin")
                                    {
                                        sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq + "|";
                                    }
                                    else
                                    {
                                        sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq + "*";
                                    }
                                }
                            }
                            else
                            {
                                if (day == "")
                                {
                                    sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq + "|";
                                }
                                else
                                {
                                    sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq;
                                }
                            }
                        }

                        string sqlGetLn = "select * from tbl_ln ln right join tbl_projedetay detay on ln.proje_detay_id=detay.id where detay.proje_id=" + HttpContext.Current.Session["prjrefno"] + " order by detay.id asc";

                        DataTable dtLn = DbClassMysql.Fill2(sqlGetLn);

                        for (int i = 0; i < dtLn.Rows.Count; i++)
                        {
                            pin_id = dtLn.Rows[i]["sira"].ToString();
                            xls_dosyasi = dtLn.Rows[i]["xls_dosya"].ToString();

                            string L10 = dtLn.Rows[i]["ln_10"].ToString();

                            string L90 = dtLn.Rows[i]["ln_90"].ToString();
                            if (dtLn.Rows.Count - 1 > i)
                            {
                                Ln += L10 + "*" + L90 + "|";
                            }
                            else
                            {
                                Ln += L10 + "*" + L90;
                            }


                        }

                        string sqlGetHava = "select * from tbl_hava hava right join tbl_projedetay detay on hava.proje_detay_id=detay.id where detay.proje_id=" + HttpContext.Current.Session["prjrefno"] + " order by detay.id asc";

                        DataTable dtHava = DbClassMysql.Fill2(sqlGetHava);

                        for (int i = 0; i < dtHava.Rows.Count; i++)
                        {

                            string temp = dtHava.Rows[i]["temp"].ToString();

                            string ruzgar = dtHava.Rows[i]["ruzgar"].ToString();

                            string nem = dtHava.Rows[i]["nem"].ToString();

                            if (dtHava.Rows.Count - 1 > i)
                            {
                                hava += temp + "*" + ruzgar + "*" + nem + "|";
                            }
                            else
                            {
                                hava += temp + "*" + ruzgar + "*" + nem;
                            }
                        }

                        string sqlGetFoto = "select * from tbl_foto foto right join tbl_projedetay detay on foto.proje_detay_id=detay.id where detay.proje_id=" + HttpContext.Current.Session["prjrefno"] + " order by detay.id asc";

                        DataTable dtFoto = DbClassMysql.Fill2(sqlGetFoto);

                        for (int i = 0; i < dtFoto.Rows.Count; i++)
                        {


                            string foto_adi = dtFoto.Rows[i]["fotograf"].ToString();
                            string foto_adi2 = "";
                            string ilk = "";
                            string ilk2 = "";
                            string[] bol = foto_adi.Split(']');

                            if (foto_adi == "")
                            {
                                fotograf += "" + "|";
                                continue;
                            }

                            if (!string.IsNullOrEmpty(bol[1]))
                            {
                                ilk = bol[1].Substring(0, (bol[1].Length - 4));
                            }
                            if (i != 0)
                            {
                                foto_adi2 = dtFoto.Rows[i - 1]["fotograf"].ToString();

                                string[] bol2 = foto_adi2.Split(']');



                                if (foto_adi2 != "")
                                {
                                    if (!string.IsNullOrEmpty(bol2[1]))
                                    {
                                        ilk2 = bol2[1].Substring(0, (bol2[1].Length - 4));
                                    }
                                }

                                if (ilk2 == ilk)
                                {
                                    fotograf += "*" + foto_adi;
                                }
                                else
                                {
                                    fotograf += "|" + foto_adi;
                                }
                            }
                            else
                            {
                                fotograf += foto_adi;
                            }

                        }

                    }
                    else
                    {
                        HttpContext.Current.Response.Redirect("Default.aspx");
                    }
                }
            }
            #endregion kod

            fotograf = fotograf.TrimEnd('|');
            sonuc = sonuc.TrimEnd('|');
        }
        catch (Exception ex)
        {

            HttpContext.Current.Response.Write(" Hata Acıklaması :" + ex.Message + " \n\r Hatalı Pin:"+pin_id+" \n\r Hatalı Xls-Dosya:"+xls_dosyasi+"  ~");

        }
        

        return pinler + "~" + header + "~" + sonuc + "~" + Ln + "~" + hava + "~" + fotograf;
        
        
        //return fotograf;
    }

    
    //public string pinOlustur2()
    //{

    //    string pinler = "";
    //    string header = "";
    //    string sonuc = "";
    //    string Ln = "";
    //    string hava = "";
    //    string fotograf = "";
    //    string pin_id = "";

    //    if (HttpContext.Current.Session["KullaniciRefno"] != null)
    //    {

    //        if (HttpContext.Current.Session["prjrefno"] == null)
    //        {
    //            string sqlGetPin;

    //            string kulNo = HttpContext.Current.Session["KullaniciRefno"].ToString();

    //            int projeID = Convert.ToInt32(DbClassMysql.ScalarQuery2("select id from tbl_proje where firma_id=" + kulNo + " order by tarih desc"));

    //            if (projeID == 0)
    //            {
    //                projeID = Convert.ToInt32(DbClassMysql.ScalarQuery2("select id from tbl_proje order by tarih desc"));
    //            }

    //            sqlGetPin = "SELECT * FROM tbl_projedetay where proje_id=" + projeID + " order by tarih asc";

    //            DataTable dtGetPin = DbClassMysql.Fill2(sqlGetPin);

    //            for (int i = 0; i < dtGetPin.Rows.Count; i++)
    //            {
    //                string enlem = dtGetPin.Rows[i]["enlem"].ToString();

    //                string boylam = dtGetPin.Rows[i]["boylam"].ToString();

    //                string zoom = dtGetPin.Rows[i]["zoom"].ToString();

    //                pin_id = dtGetPin.Rows[i]["pin_id"].ToString();

    //                string aciklama = dtGetPin.Rows[i]["aciklama"].ToString();

    //                string proje_detay_id = dtGetPin.Rows[i]["id"].ToString();

    //                string sira = dtGetPin.Rows[i]["sira"].ToString();

    //                if (dtGetPin.Rows.Count - 1 > i)
    //                {
    //                    pinler += enlem + "*" + boylam + "*" + zoom + "*" + pin_id + "*" + aciklama + "*" + sira + "|";
    //                }
    //                else
    //                {
    //                    pinler += enlem + "*" + boylam + "*" + zoom + "*" + pin_id + "*" + aciklama + "*" + sira;
    //                }
    //            }

    //            string sqlGetHeader = "select * from tbl_header header right join tbl_projedetay detay on header.proje_detay_id=detay.id where detay.proje_id=" + projeID + " order by detay.id asc";

    //            DataTable dtHeader = DbClassMysql.Fill2(sqlGetHeader);

    //            for (int i = 0; i < dtHeader.Rows.Count; i++)
    //            {

    //                string calibration = dtHeader.Rows[i]["calibration_factor"].ToString();

    //                string device = dtHeader.Rows[i]["device_type"].ToString();

    //                string original_file_name = dtHeader.Rows[i]["original_file_name"].ToString();

    //                string associated_file_name = dtHeader.Rows[i]["associated_file_name"].ToString();

    //                string calibration_date = dtHeader.Rows[i]["calibration_date"].ToString();

    //                string calibration_time = dtHeader.Rows[i]["calibration_time"].ToString();

    //                string serial_no = dtHeader.Rows[i]["serial_no"].ToString();

    //                if (dtHeader.Rows.Count - 1 > i)
    //                {
    //                    header += calibration + "*" + device + "*" + original_file_name + "*" + associated_file_name + "*" + calibration_date + "*" + calibration_time + "*" + serial_no + "|";
    //                }
    //                else
    //                {
    //                    header += calibration + "*" + device + "*" + original_file_name + "*" + associated_file_name + "*" + calibration_date + "*" + calibration_time + "*" + serial_no;
    //                }
    //            }

    //            string sqlGetSonuc = "select * from tbl_sonuc sonuc right join tbl_projedetay detay on sonuc.proje_detay_id=detay.id where detay.proje_id=" + projeID + " order by detay.id asc";

    //            DataTable dtSonuc = DbClassMysql.Fill2(sqlGetSonuc);

    //            for (int i = 0; i < dtSonuc.Rows.Count; i++)
    //            {
    //                DateTime test;
    //                string elapsed_time;

    //                if (DateTime.TryParse(Convert.ToString(dtSonuc.Rows[i]["elapsed_time"].ToString()), out test))
    //                {
    //                    elapsed_time = dtSonuc.Rows[i]["elapsed_time"].ToString();
    //                }
    //                else
    //                {
    //                    string[] olcum = Convert.ToString(dtSonuc.Rows[i]["elapsed_time"].ToString()).Split(':');

    //                    if (olcum[0].Trim() == "24")
    //                    {
    //                        elapsed_time = "01";
    //                    }
    //                    else
    //                    {
    //                        elapsed_time = olcum[0].ToString();
    //                    }

    //                    if (elapsed_time != "")
    //                    {
    //                        elapsed_time += ":" + olcum[1].ToString() + ":" + olcum[2].ToString();
    //                    }
    //                }

    //                string day = dtSonuc.Rows[i]["day"].ToString();

    //                string hour = dtSonuc.Rows[i]["hour"].ToString();

    //                string filter = dtSonuc.Rows[i]["filter"].ToString();

    //                string detector = dtSonuc.Rows[i]["detector"].ToString();

    //                string peak = dtSonuc.Rows[i]["peak"].ToString();

    //                string max = dtSonuc.Rows[i]["max"].ToString();

    //                string min = dtSonuc.Rows[i]["min"].ToString();

    //                string leq = dtSonuc.Rows[i]["leq"].ToString();

    //                if (dtSonuc.Rows.Count - 1 > i)
    //                {
    //                    if (day == "")
    //                    {
    //                        sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq + "*" + "|";
    //                    }
    //                    else
    //                    {
    //                        if (filter == "Lin")
    //                        {
    //                            sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq + "|";
    //                        }
    //                        else
    //                        {
    //                            sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq + "*";
    //                        }
    //                    }
    //                }
    //                else
    //                {
    //                    if (day == "")
    //                    {
    //                        sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq + "|";
    //                    }
    //                    else
    //                    {
    //                        sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq;
    //                    }
    //                }
    //            }

    //            string sqlGetLn = "select * from tbl_ln ln right join tbl_projedetay detay on ln.proje_detay_id=detay.id where detay.proje_id=" + projeID + " order by detay.id asc";

    //            DataTable dtLn = DbClassMysql.Fill2(sqlGetLn);

    //            for (int i = 0; i < dtLn.Rows.Count; i++)
    //            {

    //                string L10 = dtLn.Rows[i]["ln_10"].ToString();

    //                string L90 = dtLn.Rows[i]["ln_90"].ToString();

    //                if (dtLn.Rows.Count - 1 > i)
    //                {
    //                    Ln += L10 + "*" + L90 + "|";
    //                }
    //                else
    //                {
    //                    Ln += L10 + "*" + L90;
    //                }


    //            }

    //            string sqlGetHava = "select * from tbl_hava hava right join tbl_projedetay detay on hava.proje_detay_id=detay.id where detay.proje_id=" + projeID + " order by detay.id asc";

    //            DataTable dtHava = DbClassMysql.Fill2(sqlGetHava);

    //            for (int i = 0; i < dtHava.Rows.Count; i++)
    //            {
    //                string temp = dtHava.Rows[i]["temp"].ToString();

    //                string ruzgar = dtHava.Rows[i]["ruzgar"].ToString();

    //                string nem = dtHava.Rows[i]["nem"].ToString();

    //                if (dtHava.Rows.Count - 1 > i)
    //                {
    //                    hava += temp + "*" + ruzgar + "*" + nem + "|";
    //                }
    //                else
    //                {
    //                    hava += temp + "*" + ruzgar + "*" + nem;
    //                }
    //            }

    //            string sqlGetFoto = "select * from tbl_foto foto right join tbl_projedetay detay on foto.proje_detay_id=detay.id where detay.proje_id=" + projeID + " order by detay.id asc";

    //            DataTable dtFoto = DbClassMysql.Fill2(sqlGetFoto);

    //            for (int i = 0; i < dtFoto.Rows.Count; i++)
    //            {
    //                string foto_adi = dtFoto.Rows[i]["fotograf"].ToString();
    //                string foto_adi2 = "";
    //                string ilk = "";
    //                string ilk2 = "";
    //                string[] bol = foto_adi.Split(']');

    //                if (foto_adi == "")
    //                {
    //                    fotograf += "" + "|";
    //                    continue;
    //                }

    //                if (!string.IsNullOrEmpty(bol[1]))
    //                {
    //                    ilk = bol[1].Substring(0, (bol[1].Length - 4));
    //                }
    //                if (i != 0)
    //                {
    //                    foto_adi2 = dtFoto.Rows[i - 1]["fotograf"].ToString();

    //                    string[] bol2 = foto_adi2.Split(']');

    //                    if (foto_adi2 != "")
    //                    {
    //                        if (!string.IsNullOrEmpty(bol2[1]))
    //                        {
    //                            ilk2 = bol2[1].Substring(0, (bol2[1].Length - 4));
    //                        }
    //                    }

    //                    if (ilk2 == ilk)
    //                    {
    //                        if (fotograf.Substring(0, fotograf.Length - 1).IndexOf('|') != -1)
    //                        {
    //                            //fotograf = fotograf.Substring(0, fotograf.Length - 1);    
    //                        }
    //                        fotograf += "*" + foto_adi;
    //                    }
    //                    else
    //                    {
    //                        fotograf += "|" + foto_adi;
    //                    }
    //                }
    //                else
    //                {
    //                    fotograf += foto_adi;
    //                }

    //            }

    //        }
    //        else
    //        {
    //            int prjRefno = Convert.ToInt32(HttpContext.Current.Session["prjrefno"]);
    //            int firmaRefno = Convert.ToInt32(HttpContext.Current.Session["KullaniciRefno"]);
    //            int control = Convert.ToInt32(DbClassMysql.ScalarQuery2("select * from tbl_proje where id=" + prjRefno + " and firma_id=" + firmaRefno));
    //            switch (Convert.ToInt32(HttpContext.Current.Session["yetki"]))
    //            {
    //                case 1:
    //                    control = 10;
    //                    break;
    //                case 2:

    //                    break;
    //                case 3:
    //                    control = 10;
    //                    break;
    //                default:
    //                    control = 10;
    //                    break;
    //            }

    //            if (control > 0)
    //            {

    //                string sqlGetPin = "SELECT * FROM tbl_projedetay where proje_id=" + HttpContext.Current.Session["prjrefno"] + " order by tarih asc";

    //                DataTable dtGetPin = DbClassMysql.Fill2(sqlGetPin);

    //                for (int i = 0; i < dtGetPin.Rows.Count; i++)
    //                {
    //                    string enlem = dtGetPin.Rows[i]["enlem"].ToString();

    //                    string boylam = dtGetPin.Rows[i]["boylam"].ToString();

    //                    string zoom = dtGetPin.Rows[i]["zoom"].ToString();

    //                    pin_id = dtGetPin.Rows[i]["pin_id"].ToString();

    //                    string aciklama = dtGetPin.Rows[i]["aciklama"].ToString();

    //                    string proje_detay_id = dtGetPin.Rows[i]["id"].ToString();

    //                    string sira = dtGetPin.Rows[i]["sira"].ToString();

    //                    if (dtGetPin.Rows.Count - 1 > i)
    //                    {
    //                        pinler += enlem + "*" + boylam + "*" + zoom + "*" + pin_id + "*" + aciklama + "*" + sira + "|";
    //                    }
    //                    else
    //                    {
    //                        pinler += enlem + "*" + boylam + "*" + zoom + "*" + pin_id + "*" + aciklama + "*" + sira;
    //                    }

    //                }

    //                string sqlGetHeader = "select * from tbl_header header right join tbl_projedetay detay on header.proje_detay_id=detay.id where detay.proje_id=" + HttpContext.Current.Session["prjrefno"] + " order by detay.id asc";

    //                DataTable dtHeader = DbClassMysql.Fill2(sqlGetHeader);

    //                for (int i = 0; i < dtHeader.Rows.Count; i++)
    //                {

    //                    string calibration = dtHeader.Rows[i]["calibration_factor"].ToString();

    //                    string device = dtHeader.Rows[i]["device_type"].ToString();

    //                    string original_file_name = dtHeader.Rows[i]["original_file_name"].ToString();

    //                    string associated_file_name = dtHeader.Rows[i]["associated_file_name"].ToString();

    //                    string calibration_date = dtHeader.Rows[i]["calibration_date"].ToString();

    //                    string calibration_time = dtHeader.Rows[i]["calibration_time"].ToString();

    //                    string serial_no = dtHeader.Rows[i]["serial_no"].ToString();

    //                    if (dtHeader.Rows.Count - 1 > i)
    //                    {
    //                        header += calibration + "*" + device + "*" + original_file_name + "*" + associated_file_name + "*" + calibration_date + "*" + calibration_time + "*" + serial_no + "|";
    //                    }
    //                    else
    //                    {
    //                        header += calibration + "*" + device + "*" + original_file_name + "*" + associated_file_name + "*" + calibration_date + "*" + calibration_time + "*" + serial_no;
    //                    }
    //                }

    //                string sqlGetSonuc = "select * from tbl_sonuc sonuc right join tbl_projedetay detay on sonuc.proje_detay_id=detay.id where detay.proje_id=" + HttpContext.Current.Session["prjrefno"] + " order by detay.id asc";

    //                DataTable dtSonuc = DbClassMysql.Fill2(sqlGetSonuc);

    //                for (int i = 0; i < dtSonuc.Rows.Count; i++)
    //                {
    //                    DateTime test;
    //                    string elapsed_time;

    //                    if (DateTime.TryParse(Convert.ToString(dtSonuc.Rows[i]["elapsed_time"].ToString()), out test))
    //                    {
    //                        elapsed_time = dtSonuc.Rows[i]["elapsed_time"].ToString();
    //                    }
    //                    else
    //                    {
    //                        string[] olcum = Convert.ToString(dtSonuc.Rows[i]["elapsed_time"].ToString()).Split(':');

    //                        if (olcum[0].Trim() == "24")
    //                        {
    //                            elapsed_time = "01";
    //                        }
    //                        else
    //                        {
    //                            elapsed_time = olcum[0].ToString();
    //                        }

    //                        if (elapsed_time != "")
    //                        {
    //                            elapsed_time += ":" + olcum[1].ToString() + ":" + olcum[2].ToString();
    //                        }
    //                    }

    //                    string day = dtSonuc.Rows[i]["day"].ToString();

    //                    string hour = dtSonuc.Rows[i]["hour"].ToString();

    //                    string filter = dtSonuc.Rows[i]["filter"].ToString();

    //                    string detector = dtSonuc.Rows[i]["detector"].ToString();



    //                    string peak = dtSonuc.Rows[i]["peak"].ToString();

    //                    string max = dtSonuc.Rows[i]["max"].ToString();

    //                    string min = dtSonuc.Rows[i]["min"].ToString();

    //                    string leq = dtSonuc.Rows[i]["leq"].ToString();


    //                    if (dtSonuc.Rows.Count - 1 > i)
    //                    {
    //                        if (day == "")
    //                        {
    //                            sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq + "*" + "|";
    //                        }
    //                        else
    //                        {
    //                            if (filter == "Lin")
    //                            {
    //                                sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq + "|";
    //                            }
    //                            else
    //                            {
    //                                sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq + "*";
    //                            }
    //                        }
    //                    }
    //                    else
    //                    {
    //                        if (day == "")
    //                        {
    //                            sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq + "|";
    //                        }
    //                        else
    //                        {
    //                            sonuc += day + "*" + hour + "*" + filter + "*" + detector + "*" + elapsed_time + "*" + peak + "*" + max + "*" + min + "*" + leq;
    //                        }
    //                    }
    //                }

    //                string sqlGetLn = "select * from tbl_ln ln right join tbl_projedetay detay on ln.proje_detay_id=detay.id where detay.proje_id=" + HttpContext.Current.Session["prjrefno"] + " order by detay.id asc";

    //                DataTable dtLn = DbClassMysql.Fill2(sqlGetLn);

    //                for (int i = 0; i < dtLn.Rows.Count; i++)
    //                {

    //                    string L10 = dtLn.Rows[i]["ln_10"].ToString();

    //                    string L90 = dtLn.Rows[i]["ln_90"].ToString();
    //                    if (dtLn.Rows.Count - 1 > i)
    //                    {
    //                        Ln += L10 + "*" + L90 + "|";
    //                    }
    //                    else
    //                    {
    //                        Ln += L10 + "*" + L90;
    //                    }


    //                }

    //                string sqlGetHava = "select * from tbl_hava hava right join tbl_projedetay detay on hava.proje_detay_id=detay.id where detay.proje_id=" + HttpContext.Current.Session["prjrefno"] + " order by detay.id asc";

    //                DataTable dtHava = DbClassMysql.Fill2(sqlGetHava);

    //                for (int i = 0; i < dtHava.Rows.Count; i++)
    //                {

    //                    string temp = dtHava.Rows[i]["temp"].ToString();

    //                    string ruzgar = dtHava.Rows[i]["ruzgar"].ToString();

    //                    string nem = dtHava.Rows[i]["nem"].ToString();

    //                    if (dtHava.Rows.Count - 1 > i)
    //                    {
    //                        hava += temp + "*" + ruzgar + "*" + nem + "|";
    //                    }
    //                    else
    //                    {
    //                        hava += temp + "*" + ruzgar + "*" + nem;
    //                    }
    //                }

    //                string sqlGetFoto = "select * from tbl_foto foto right join tbl_projedetay detay on foto.proje_detay_id=detay.id where detay.proje_id=" + HttpContext.Current.Session["prjrefno"] + " order by detay.id asc";

    //                DataTable dtFoto = DbClassMysql.Fill2(sqlGetFoto);

    //                for (int i = 0; i < dtFoto.Rows.Count; i++)
    //                {


    //                    string foto_adi = dtFoto.Rows[i]["fotograf"].ToString();
    //                    string foto_adi2 = "";
    //                    string ilk = "";
    //                    string ilk2 = "";
    //                    string[] bol = foto_adi.Split(']');

    //                    if (foto_adi == "")
    //                    {
    //                        fotograf += "" + "|";
    //                        continue;
    //                    }

    //                    if (!string.IsNullOrEmpty(bol[1]))
    //                    {
    //                        ilk = bol[1].Substring(0, (bol[1].Length - 4));
    //                    }
    //                    if (i != 0)
    //                    {
    //                        foto_adi2 = dtFoto.Rows[i - 1]["fotograf"].ToString();

    //                        string[] bol2 = foto_adi2.Split(']');



    //                        if (foto_adi2 != "")
    //                        {
    //                            if (!string.IsNullOrEmpty(bol2[1]))
    //                            {
    //                                ilk2 = bol2[1].Substring(0, (bol2[1].Length - 4));
    //                            }
    //                        }

    //                        if (ilk2 == ilk)
    //                        {
    //                            fotograf += "*" + foto_adi;
    //                        }
    //                        else
    //                        {
    //                            fotograf += "|" + foto_adi;
    //                        }
    //                    }
    //                    else
    //                    {
    //                        fotograf += foto_adi;
    //                    }

    //                }

    //            }
    //            else
    //            {
    //                HttpContext.Current.Response.Redirect("Default.aspx");
    //            }
    //        }
    //    }
    //    fotograf = fotograf.TrimEnd('|');
    //    sonuc = sonuc.TrimEnd('|');

    //    return pinler + "~" + header + "~" + sonuc + "~" + Ln + "~" + hava + "~" + fotograf;

    //    //return fotograf;
    //}

    
    public void xlsExport()
    {
        if (Session["activePrj"]==null)
        {
            Response.Redirect("Kapi/Default.aspx");
        }
        string sql = "select prj.proje_adi,p.pin_id,son.filter,son.day,h.device_type,h.original_file_name,son.elapsed_time,son.detector,son.leq,son.max,son.min,l.ln_10,l.ln_90,p.enlem,p.boylam,ha.temp,ha.ruzgar,ha.nem from tbl_sonuc son"; 
                sql +=" right join tbl_projedetay p on son.proje_detay_id=p.id ";
                sql +=" left join tbl_ln l on l.proje_detay_id=p.id ";
                sql +=" left join tbl_proje prj on prj.id=p.proje_id";
                sql +=" left join tbl_header h on h.proje_detay_id=p.id ";
                sql +=" left join tbl_hava ha on ha.proje_detay_id=p.id ";
                sql += " where p.proje_id=" + Session["activePrj"] + " order by p.tarih asc";

                DataTable dtRapor = db.Fill(sql);

        Random rnd = new Random();
        string random = "";
        for (int i = 0; i < 10; i++)
        {
            random += rnd.Next(0, 10);
        }

        string newFile = HttpContext.Current.Server.MapPath("~/TempFiles/") + @"rapor_"+random+".xlsx";

        //sablonu kopyala
        File.Copy(HttpContext.Current.Server.MapPath("~/sablon/") + @"rapor.xlsx", newFile);
        ExcelPackage pck;
        using (var fs = new FileStream(newFile,FileMode.Open))
        {
            pck = new ExcelPackage(fs);
            var ws = pck.Workbook.Worksheets[1];

        int sabit = 4;

        int no=0;

        int satir = 0;

        for (int i = 0; i < dtRapor.Rows.Count;)
        {
            ws.Cells[1, 2].Value = dtRapor.Rows[i]["proje_adi"].ToString(); // proje adı    

            if (string.IsNullOrEmpty(dtRapor.Rows[i]["filter"].ToString()))
            {
                no++;
                i++;
                continue;
            }
            else
            {
                no++;
                ws.Cells[sabit + satir, 1].Value = no;
                ws.Cells[sabit + satir, 2].Value = dtRapor.Rows[i]["day"].ToString(); // day
                ws.Cells[sabit + satir, 3].Value = dtRapor.Rows[i]["device_type"].ToString(); // cihaz_adi
                ws.Cells[sabit + satir, 4].Value = dtRapor.Rows[i]["original_file_name"].ToString(); // cihaz_adi
                ws.Cells[sabit + satir, 5].Value = dtRapor.Rows[i]["elapsed_time"].ToString(); // ölçüm süresi

                ws.Cells[sabit + satir, 6].Value = dtRapor.Rows[i]["enlem"].ToString(); // detector
                ws.Cells[sabit + satir, 7].Value = dtRapor.Rows[i]["boylam"].ToString(); // Aleq
                ws.Cells[sabit + satir, 8].Value = dtRapor.Rows[i]["temp"].ToString()+" °C"; // Aleq
                ws.Cells[sabit + satir, 9].Value = dtRapor.Rows[i]["nem"].ToString()+" mm/s"; // Aleq
                ws.Cells[sabit + satir, 10].Value = dtRapor.Rows[i]["ruzgar"].ToString()+" %"; // Aleq
                ws.Cells[sabit + satir, 11].Value = dtRapor.Rows[i]["detector"].ToString(); // Aleq
                                
                ws.Cells[sabit + satir, 12].Value = dtRapor.Rows[i]["leq"].ToString(); // Aleq
                ws.Cells[sabit + satir, 13].Value = dtRapor.Rows[i]["max"].ToString(); // Aleq
                ws.Cells[sabit + satir, 14].Value = dtRapor.Rows[i]["min"].ToString(); // Aleq
                ws.Cells[sabit + satir, 15].Value = dtRapor.Rows[i]["ln_10"].ToString(); // Aleq
                ws.Cells[sabit + satir, 16].Value = dtRapor.Rows[i]["ln_90"].ToString(); // Aleq

                ws.Cells[sabit + satir, 17].Value = dtRapor.Rows[i + 1]["detector"].ToString(); // detector
                ws.Cells[sabit + satir, 18].Value = dtRapor.Rows[i + 1]["leq"].ToString(); // Cleq
                ws.Cells[sabit + satir, 19].Value = dtRapor.Rows[i + 1]["max"].ToString(); // Aleq
                ws.Cells[sabit + satir, 20].Value = dtRapor.Rows[i + 1]["min"].ToString(); // Aleq
                ws.Cells[sabit + satir, 21].Value = dtRapor.Rows[i + 1]["ln_10"].ToString(); // Aleq
                ws.Cells[sabit + satir, 22].Value = dtRapor.Rows[i + 1]["ln_90"].ToString(); // Aleq
                i+=3;
                satir++;
            }

        }
        }   
                
        //***************************************************
        FileInfo file = new FileInfo(newFile);
        pck.SaveAs(Response.OutputStream);
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.AddHeader("content-disposition", "attachment;  filename=" + file.Name);
        Response.End();      
        
    }

    //protected void Button1_Click(object sender, EventArgs e)
    //{
    //    xlsExport();
    //}
}