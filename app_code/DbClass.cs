﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;

namespace DbClass
{
    public class DbClassMysql
    {
        /// <summary>
        /// Mysql örnek bağlantı cümlesi "Server=xxx.xxx.xxx.xxx;Database=xxx;Uid=xxx;Pwd=xxxx;charset=latin5";
        /// </summary>

        /// <summary>
        /// Bu metot bölme işlemi yapar
        /// </summary>
        /// <param name="s1">Double türünde bölünen sayı</param>
        /// <param name="s2">Double türünde bölünecek sayı</param>
        /// <returns>Double türünde sonuç döner</returns>

        // Summary:
        //     Determines whether the specified System.Object is equal to the current System.Object.
        //
        // Parameters:
        //   obj:
        //     The System.Object to compare with the current System.Object.
        //
        // Returns:
        //     true if the specified System.Object is equal to the current System.Object;
        //     otherwise, false.
        public string cnnstr = "Data Source=94.73.148.9;Initial Catalog=DB140727191208;User Id=USR140727191208;Persist Security Info=True;Password=PSS!P9H4M8%";

       // public string cnnstr = "Data Source=localhost;Initial Catalog=DB140727191208;Integrated Security=True";



        public static string cnnstr2 = "Data Source=94.73.148.9;Initial Catalog=DB140727191208;User Id=USR140727191208;Persist Security Info=True;Password=PSS!P9H4M8%";

        // public static string cnnstr2 = "Data Source=localhost;Initial Catalog=DB140727191208;Integrated Security=True";
        
        SqlConnection cnn = new SqlConnection();
        


        /// <summary>
        /// Bu metot bölme işlemi yapar
        /// </summary>
        /// <param name="s1">Double türünde bölünen sayı</param>
        /// <param name="s2">Double türünde bölünecek sayı</param>
        /// <returns>Double türünde sonuç döner</returns>
        /// 

        // Summary:
        //     Determines whether the specified System.Object is equal to the current System.Object.
        //
        // Parameters:
        //   obj:
        //     The System.Object to compare with the current System.Object.
        //
        // Returns:
        //     true if the specified System.Object is equal to the current System.Object;
        //     otherwise, false.
        public DbClassMysql()
        {
            cnn.ConnectionString = cnnstr;
        }


        public DataSet DataSetGetir(string sql, string tablo)
        {//aynı datasette birden çok tablo saklanabilir.
            SqlCommand cmd = new SqlCommand(sql, cnn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds, tablo);//sql ile belirtilen bilgi ds'ye tablo olarak aktarılıyor
            return ds;
        }

        public DataTable sp_Fill(string usp, params SqlParameter[] prms)
        {
            SqlCommand cmd = new SqlCommand(usp, cnn);

            cmd.CommandType = CommandType.StoredProcedure;

            if (prms != null)
            {
                cmd.Parameters.AddRange(prms);
            }

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            DataTable dt = new DataTable();

            da.Fill(dt);

            return dt;
        }

        public DataTable Fill(string sql, params SqlParameter[] prms)
        {
            SqlCommand cmd = new SqlCommand(sql, cnn);
            if (prms != null)
            {
                cmd.Parameters.AddRange(prms);
            }
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);//sql ile belirtilen bilgi dt tablosuna aktarılıyor
            return dt;
        }

        public static DataTable Fill2(string sql, params SqlParameter[] prms)
        {
            SqlConnection cnn2 = new SqlConnection();

            cnn2.ConnectionString = cnnstr2;

            SqlCommand cmd = new SqlCommand(sql, cnn2);
            if (prms != null)
            {
                cmd.Parameters.AddRange(prms);
            }
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);//sql ile belirtilen bilgi dt tablosuna aktarılıyor
            return dt;
        }

        public int ExecuteNonQuery(string sql, params SqlParameter[] prms)
        {
            int s = 0;

            SqlCommand cmd = new SqlCommand(sql, cnn);

            if (prms != null)
            {
                cmd.Parameters.AddRange(prms);
            }

            if (cnn.State == ConnectionState.Closed)
            {
                cnn.Open();
            }

            try
            {
                cmd.ExecuteNonQuery();

                SqlCommand cmd1 = new SqlCommand("SELECT @@IDENTITY AS SAYI", cnn);

                object o = cmd1.ExecuteScalar();
                if (o != DBNull.Value)
                {
                    s = Convert.ToInt32(o);
                }
            }
            finally
            {
                cnn.Close();
            }

            return s;
        }

        public static int ExecuteNonQuery2(string sql, params SqlParameter[] prms)
        {
            int s = 0;
            SqlConnection cnn2 = new SqlConnection();

            cnn2.ConnectionString = cnnstr2;

            SqlCommand cmd = new SqlCommand(sql, cnn2);
            if (prms != null)
            {
                cmd.Parameters.AddRange(prms);
            }

            if (cnn2.State == ConnectionState.Closed)
            {
                cnn2.Open();
            }

            try
            {

                cmd.ExecuteNonQuery();

                SqlCommand cmd1 = new SqlCommand("SELECT @@IDENTITY AS SAYI", cnn2);
                object o = cmd1.ExecuteScalar();
                if (o != DBNull.Value)
                {
                    s = Convert.ToInt32(o);
                }
            }
            finally
            {
                cnn2.Close();
            }

            return s;
        }

        public static object ScalarQuery2(string sql, params SqlParameter[] prms)
        {
            object s = null;

            SqlConnection cnn2 = new SqlConnection();

            cnn2.ConnectionString = cnnstr2;

            SqlCommand cmd = new SqlCommand(sql, cnn2);

            if (prms != null)
            {
                cmd.Parameters.AddRange(prms);
            }

            if (cnn2.State == ConnectionState.Closed)
            {
                cnn2.Open();
            }

            try
            {
                s = cmd.ExecuteScalar();
            }
            finally
            {
                cnn2.Close();
            }
            return s;
        }

        public object ScalarQuery(string sql, params SqlParameter[] prms)
        {
            object s = null;

            SqlCommand cmd = new SqlCommand(sql, cnn);
            if (prms != null)
            {
                cmd.Parameters.AddRange(prms);
            }

            if (cnn.State == ConnectionState.Closed)
            {
                cnn.Open();
            }

            try
            {
                s = cmd.ExecuteScalar();
            }
            finally
            {
                cnn.Close();
            }
            return s;
        }

    }

}
