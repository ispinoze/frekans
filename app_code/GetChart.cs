﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI.DataVisualization.Charting;
/// <summary>
/// Summary description for GetChart
/// </summary>
public class GetChart
{
    DbClass.DbClassMysql db = new DbClass.DbClassMysql();

	public GetChart()
	{
        
	}

    #region generalMethods

    public class DataPointsColumn
    {

        public string label { get; set; }
        public double y { get; set; }


        public DataPointsColumn(string label, double y)
        {

            this.label = label;
            this.y = y;

        }

        public DataPointsColumn()
        {
        }

    }

    public ArrayList get_1_1_Octav_Band(int pin_id)
    {
        /*
          log((1/n)x(10ussu(spl/10)+10ussu(spl2/10)+...+(10ussu(spln/10))
          n saatlik veri sayisi
         */

        string sql = "select ProjeDetayID,DbHz25,DbHz31Half,DbHz40,DbHz50,DbHz63,DbHz80,DbHz100,DbHz125,DbHz160,DbHz200,DbHz250,DbHz315,DbHz400,DbHz500,DbHz630,DbHz800,DbHz1000,DbHz1250,DbHz1600,DbHz2000,DbHz2500,DbHz3150,DbHz4000,DbHz5000,DbHz6300,DbHz8000,DbHz10000 from tbl_oktav_sonuc  octav inner join tbl_projedetay as detay on octav.ProjeDetayID=detay.id where octav.ProjeDetayID=" + pin_id + "  ";

        DataTable dtPoints = DbClass.DbClassMysql.Fill2(sql);

        
        ArrayList dataPoint = new ArrayList();

        if (dtPoints.Rows.Count < 1)
        {
            return null;
        }


        //var dataSeries = { type: "line", showInLegend: true, legendText: "P1 LEQ" };


        // dataPoint = new ArrayList();

        DataRow[] result = { };




        DataPointsColumn p = new DataPointsColumn();



        for (int i = 1; i < 10; i++)
        {
            //1/1 31,5 Hz = 10*log(10^(25hz/10)+10^(31,5hz/10)+10^(40hz/10))

            p = new DataPointsColumn();

            double logValue;

            switch (i)
            {
                case 1:
                    p.label = "31,5 Hz";

                    logValue = Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz25"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz31Half"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz40"]) / 10));

                    p.y = Math.Round((10 * (Math.Log(logValue, 10))), 1);

                    break;
                case 2:
                    p.label = "63 Hz";

                    logValue = Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz50"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz63"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz80"]) / 10));

                    p.y = Math.Round((10 * (Math.Log(logValue, 10))), 1);

                    break;
                case 3:
                    p.label = "125 Hz";

                    logValue = Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz100"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz125"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz160"]) / 10));

                    p.y = Math.Round((10 * (Math.Log(logValue, 10))), 1);

                    break;
                case 4:
                    p.label = "250 Hz";

                    logValue = Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz200"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz250"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz315"]) / 10));

                    p.y = Math.Round((10 * (Math.Log(logValue, 10))), 1);

                    break;
                case 5:
                    p.label = "500 Hz";

                    logValue = Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz400"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz500"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz630"]) / 10));

                    p.y = Math.Round((10 * (Math.Log(logValue, 10))), 1);

                    break;
                case 6:
                    p.label = "1000 Hz";

                    logValue = Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz800"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz1000"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz1250"]) / 10));

                    p.y = Math.Round((10 * (Math.Log(logValue, 10))), 1);

                    break;
                case 7:
                    p.label = "2000 Hz";

                    logValue = Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz1600"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz2000"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz3150"]) / 10));

                    p.y = Math.Round((10 * (Math.Log(logValue, 10))), 1);

                    break;
                case 8:
                    p.label = "4000 Hz";

                    logValue = Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz3150"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz4000"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz5000"]) / 10));

                    p.y = Math.Round((10 * (Math.Log(logValue, 10))), 1);

                    break;
                case 9:
                    p.label = "8000 Hz";

                    logValue = Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz6300"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz8000"]) / 10)) + Math.Pow(10, (Convert.ToDouble(dtPoints.Rows[0]["DbHz10000"]) / 10));

                    p.y = Math.Round((10 * (Math.Log(logValue, 10))), 1);

                    break;
                default:
                    break;
            }

            dataPoint.Add(p);


        }






        //string json = JsonConvert.SerializeObject(dataPoint);


        return dataPoint;
        //return fotograf;
    }

    public ArrayList get_1_3_Octav_Band(int pin_id)
    {
        string sql = "select *  from tbl_oktav_sonuc  octav inner join tbl_projedetay as detay on octav.ProjeDetayID=detay.id where octav.ProjeDetayID=" + pin_id + "  ";

        DataTable dtPoints = DbClass.DbClassMysql.Fill2(sql);

        ArrayList dataPoint = new ArrayList();
        //var dataSeries = { type: "line", showInLegend: true, legendText: "P1 LEQ" };

        if (dtPoints.Rows.Count < 1)
        {
            return null;
        }

        DataPointsColumn p = new DataPointsColumn();
        
        for (int i = 6; i < 37; i++)
        {


            p = new DataPointsColumn();

            p.y = Math.Round(Convert.ToSingle(dtPoints.Rows[0][i]), 1);

            switch (i)
            {
                case 6:
                    p.label = "20 Hz";
                    break;
                case 7:
                    p.label = "25 Hz";
                    break;
                case 8:
                    p.label = "31,5 Hz";
                    break;
                case 9:
                    p.label = "40 Hz";
                    break;
                case 10:
                    p.label = "50 Hz";
                    break;
                case 11:
                    p.label = "63 Hz";
                    break;
                case 12:
                    p.label = "80 Hz";
                    break;
                case 13:
                    p.label = "100 Hz";
                    break;
                case 14:
                    p.label = "125 Hz";
                    break;
                case 15:
                    p.label = "160 Hz";
                    break;
                case 16:
                    p.label = "200 Hz";
                    break;
                case 17:
                    p.label = "250 Hz";
                    break;
                case 18:
                    p.label = "315 Hz";
                    break;
                case 19:
                    p.label = "400 Hz";
                    break;
                case 20:
                    p.label = "500 Hz";
                    break;
                case 21:
                    p.label = "630 Hz";
                    break;
                case 22:
                    p.label = "800 Hz";
                    break;
                case 23:
                    p.label = "1000 Hz";
                    break;
                case 24:
                    p.label = "1250 Hz";
                    break;
                case 25:
                    p.label = "1600 Hz";
                    break;
                case 26:
                    p.label = "2000 Hz";
                    break;
                case 27:
                    p.label = "2500 Hz";
                    break;
                case 28:
                    p.label = "3150 Hz";
                    break;
                case 29:
                    p.label = "4000 Hz";
                    break;
                case 30:
                    p.label = "5000 Hz";
                    break;
                case 31:
                    p.label = "6300 Hz";
                    break;
                case 32:
                    p.label = "8000 Hz";
                    break;
                case 33:
                    p.label = "10000 Hz";
                    break;
                case 34:
                    p.label = "12500 Hz";
                    break;
                case 35:
                    p.label = "16000 Hz";
                    break;
                case 36:
                    p.label = "20000 Hz";
                    break;
                default:
                    break;
            }
            
            dataPoint.Add(p);

                        
        }



        return dataPoint;
        
    }

    #endregion generalMethods

    #region charts

    public Image GetChartLogger(int projeDetayID)
    {

        string sql = "SELECT slm.ProjeDetayID,slm.profil,detay.pin_id, case when datepart(mi,SonucDate)<1  then dateadd(mi, datediff(mi, 0, SonucDate)+0, 0) else dateadd(mi,1,dateadd(mi,datediff(mi,0,SonucDate)+0,0)) end as SonucDate,Round(AVG(slm.LEQ),2) AS LEQ,Round(AVG(slm.SPL),2) AS SPL from tbl_slm_history  slm inner join tbl_projedetay as detay on slm.ProjeDetayID=detay.id where slm.ProjeDetayID=" + projeDetayID + " and slm.Profil in(1,2,3)  GROUP BY slm.Profil,slm.ProjeDetayID,detay.pin_id, case when datepart(mi,SonucDate)<1  then dateadd(mi, datediff(mi, 0, SonucDate)+0, 0) else dateadd(mi,1,dateadd(mi,datediff(mi,0,SonucDate)+0,0)) end order by SonucDate,slm.profil";

        DataTable dt = db.Fill(sql);

        ArrayList y = new ArrayList();
        ArrayList x = new ArrayList();
        ArrayList y1 = new ArrayList();
        ArrayList x1 = new ArrayList();
        ArrayList y2 = new ArrayList();
        ArrayList x2 = new ArrayList();

        string max = dt.AsEnumerable()
        .Where(row => row["SPL"].ToString() != "jack")
        .Max(row => row["SPL"])
        .ToString();

        string min = dt.AsEnumerable()
        .Where(row => row["SPL"].ToString() != "jack")
        .Min(row => row["SPL"])
        .ToString();

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (Convert.ToInt32(dt.Rows[i]["Profil"]) == 1)
            {
                y.Add(Convert.ToSingle(dt.Rows[i]["SPL"]));
                x.Add(Convert.ToDateTime(dt.Rows[i]["SonucDate"]));
            }
            if (Convert.ToInt32(dt.Rows[i]["Profil"]) == 2)
            {

                y1.Add(Convert.ToSingle(dt.Rows[i]["SPL"]));
                x1.Add(Convert.ToDateTime(dt.Rows[i]["SonucDate"]));
            }

            if (Convert.ToInt32(dt.Rows[i]["Profil"]) == 3)
            {

                y2.Add(Convert.ToSingle(dt.Rows[i]["SPL"]));
                x2.Add(Convert.ToDateTime(dt.Rows[i]["SonucDate"]));
            }
        }


        Chart chart = new Chart();

        // Build a pie series
        Series seriesA = new Series("dBA");
        Series seriesC = new Series("dBC");
        Series seriesZ = new Series("dB");

        seriesA.ChartType = SeriesChartType.Line;
        seriesC.ChartType = SeriesChartType.Line;
        seriesZ.ChartType = SeriesChartType.Line;

        //series["PieLabelStyle"] = "Disabled";
        chart.Series.Add(seriesA);
        chart.Series.Add(seriesC);
        chart.Series.Add(seriesZ);

        // Define the chart area
        ChartArea chartArea = new ChartArea();
        ChartArea3DStyle areaStyle = new ChartArea3DStyle(chartArea);

        //chart.Titles.Add("LOGGER GRAPH (dBA,dBC,dBZ)");

        Axis yAxis = new Axis(chartArea, AxisName.Y);
        Axis xAxis = new Axis(chartArea, AxisName.X);
        chartArea.AxisX.LabelStyle.Angle = -90;
        // Bind the data to the chart
        chart.Series["dBA"].Points.DataBindXY(x, y);
        chart.Series["dBC"].Points.DataBindXY(x1, y1);
        chart.Series["dB"].Points.DataBindXY(x2, y2);

        // Save the chart as a 300 x 200 image
        chart.Width = new System.Web.UI.WebControls.Unit(546, System.Web.UI.WebControls.UnitType.Pixel);
        chart.Height = new System.Web.UI.WebControls.Unit(308, System.Web.UI.WebControls.UnitType.Pixel);
        chart.ChartAreas.Add(chartArea);
        Legend legend = new Legend();
        legend.Docking = Docking.Bottom;
        legend.IsDockedInsideChartArea = false;
        chart.ChartAreas[0].AxisX.LabelStyle.Angle = 90;

        chart.ChartAreas[0].AxisX.LabelStyle.Format = "dd/MM/yy hh:mm";

        chart.ChartAreas[0].AxisX.LabelStyle.Font = new System.Drawing.Font("Arial", 7.5F, System.Drawing.FontStyle.Regular);
        chart.ChartAreas[0].AxisY.LabelStyle.Font = new System.Drawing.Font("Arial", 7.5F, System.Drawing.FontStyle.Regular);

        chart.ChartAreas[0].AxisX.IntervalType = DateTimeIntervalType.Hours;

        chart.ChartAreas[0].AxisX.Interval = 1;

        chart.ChartAreas[0].AxisY.LabelStyle.Format = "{0:0}";

        //chart.ChartAreas[0].AxisY.Minimum =Convert.ToDouble(dt.Rows[0]["minvalue"])-20;
        //chart.ChartAreas[0].AxisY.Maximum = Convert.ToDouble(dt.Rows[0]["maxvalue"]);

        chart.ChartAreas[0].AxisY.Minimum = Convert.ToSingle(min) - 1;
        chart.ChartAreas[0].AxisY.Maximum = Convert.ToSingle(max) + 1;

        chart.ChartAreas[0].AxisX.MajorTickMark.Enabled = true;
        chart.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
        chart.ChartAreas[0].AxisX.MinorGrid.Enabled = false;
        chart.ChartAreas[0].AxisX.MinorTickMark.Enabled = false;
        chart.ChartAreas[0].AxisY.MajorTickMark.Enabled = true;
        chart.ChartAreas[0].AxisY.MajorGrid.Enabled = true;
        chart.ChartAreas[0].AxisY.MinorGrid.Enabled = false;
        chart.ChartAreas[0].AxisY.MinorTickMark.Enabled = false;
        chart.ChartAreas[0].AxisY.MajorGrid.LineDashStyle = ChartDashStyle.Dash;
        chart.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.LightGray;

        chart.Legends.Add(legend);

        Stream sr = new MemoryStream();

        chart.SaveImage(sr);

        System.Drawing.Image img = System.Drawing.Image.FromStream(sr);

        return img;

    }

    public Image GetChart1_1Octav(int projeDetayID) {

        ArrayList data = get_1_1_Octav_Band(projeDetayID);

        ArrayList y = new ArrayList();
        ArrayList x = new ArrayList();

        for (int i = 0; i < data.Count; i++)
        {
            DataPointsColumn dtDetail = (DataPointsColumn)data[i];

            x.Add(dtDetail.label);
            y.Add(dtDetail.y);
        }

        
        Chart chart = new Chart();

        // Build a pie series
        Series series = new Series("1_1Octav");


        series.ChartType = SeriesChartType.Column;

        chart.Series.Add(series);


        // Define the chart area
        ChartArea chartArea = new ChartArea();
        ChartArea3DStyle areaStyle = new ChartArea3DStyle(chartArea);


        Axis yAxis = new Axis(chartArea, AxisName.Y);
        Axis xAxis = new Axis(chartArea, AxisName.X);

        chart.Series["1_1Octav"].Points.DataBindXY(x, y);


        // Save the chart as a 300 x 200 image
        chart.Width = new System.Web.UI.WebControls.Unit(551, System.Web.UI.WebControls.UnitType.Pixel);
        chart.Height = new System.Web.UI.WebControls.Unit(237, System.Web.UI.WebControls.UnitType.Pixel);
        chart.ChartAreas.Add(chartArea);
        Legend legend = new Legend();
        legend.Docking = Docking.Bottom;
        legend.IsDockedInsideChartArea = false;
        //chart.ChartAreas[0].AxisX.LabelStyle.Angle = 90;

        chart.ChartAreas[0].AxisX.LabelStyle.Format = "{0:0} Hz";

        chart.ChartAreas[0].AxisY.Interval = 10;

        chart.ChartAreas[0].AxisX.LabelStyle.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
        chart.ChartAreas[0].AxisY.LabelStyle.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);

        chart.ChartAreas[0].AxisX.Interval = 1;

        chart.ChartAreas[0].AxisX.MinorGrid.Enabled = false;

        chart.ChartAreas[0].AxisY.LabelStyle.Format = "{0:0}";

        //chart.ChartAreas[0].AxisY.Minimum =Convert.ToDouble(dt.Rows[0]["minvalue"])-20;
        //chart.ChartAreas[0].AxisY.Maximum = Convert.ToDouble(dt.Rows[0]["maxvalue"]);

        //chart.ChartAreas[0].AxisY.Minimum =40.82F-1 ;
        //chart.ChartAreas[0].AxisY.Maximum =80.32F+1;
        //chart.Series[0].IsValueShownAsLabel = true;

        chart.ChartAreas[0].AxisX.MajorTickMark.Enabled = true;
        chart.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
        chart.ChartAreas[0].AxisX.MinorGrid.Enabled = false;
        chart.ChartAreas[0].AxisX.MinorTickMark.Enabled = false;
        chart.ChartAreas[0].AxisY.MajorTickMark.Enabled = true;
        chart.ChartAreas[0].AxisY.MajorGrid.Enabled = true;
        chart.ChartAreas[0].AxisY.MinorGrid.Enabled = false;
        chart.ChartAreas[0].AxisY.MinorTickMark.Enabled = false;


        chart.Series[0].CustomProperties = "PointWidth=1";

        chart.ChartAreas[0].AxisX.LineWidth = 0;
        chart.ChartAreas[0].AxisY.LineWidth = 0;

        StripLine stripLine1 = new StripLine();
        stripLine1.StripWidth = 0;
        stripLine1.BorderColor = Color.LightGray;
        stripLine1.BorderWidth = 1;

        stripLine1.Interval = 1;



        //stripLine1.BorderDashStyle = ChartDashStyle.Dash;

        StripLine stripLine2 = new StripLine();
        stripLine2.StripWidth = 0;
        stripLine2.BorderColor = Color.LightGray;
        stripLine2.BorderWidth = 1;
        stripLine2.Interval = 10;
        stripLine2.BorderDashStyle = ChartDashStyle.Dash;
        /*chart.ChartAreas[0].AxisX.StripLines.Add(stripLine1);*/
        chart.ChartAreas[0].AxisY.StripLines.Add(stripLine2);


        System.Drawing.Color col = System.Drawing.ColorTranslator.FromHtml("#C6D9F1");

        Color newColor = Color.FromArgb(150, col);

        System.Drawing.Color colBor = System.Drawing.ColorTranslator.FromHtml("#B0AFDD");

        chart.Series[0].BorderColor = colBor;

        chart.Series[0].BorderWidth = 1;

        chart.Series[0].Color = newColor;

        Stream sr = new MemoryStream();

        chart.SaveImage(sr);

        System.Drawing.Image img = System.Drawing.Image.FromStream(sr);

        return img;
    
    }

    public Image GetChart1_3Octav(int projeDetayID)
    {

        ArrayList data = get_1_3_Octav_Band(projeDetayID);

        ArrayList y = new ArrayList();
        ArrayList x = new ArrayList();

        for (int i = 0; i < data.Count; i++)
        {
            DataPointsColumn dtDetail = (DataPointsColumn)data[i];

            x.Add(dtDetail.label);
            y.Add(dtDetail.y);
        }


        Chart chart = new Chart();

        // Build a pie series
        Series series = new Series("1_1Octav");


        series.ChartType = SeriesChartType.Column;

        chart.Series.Add(series);


        // Define the chart area
        ChartArea chartArea = new ChartArea();
        ChartArea3DStyle areaStyle = new ChartArea3DStyle(chartArea);


        Axis yAxis = new Axis(chartArea, AxisName.Y);
        Axis xAxis = new Axis(chartArea, AxisName.X);

        chart.Series["1_1Octav"].Points.DataBindXY(x, y);


        // Save the chart as a 300 x 200 image
        chart.Width = new System.Web.UI.WebControls.Unit(551, System.Web.UI.WebControls.UnitType.Pixel);
        chart.Height = new System.Web.UI.WebControls.Unit(237, System.Web.UI.WebControls.UnitType.Pixel);
        chart.ChartAreas.Add(chartArea);
        
        

        chart.ChartAreas[0].AxisX.LabelStyle.Format = "{0:0} Hz";

        chart.ChartAreas[0].AxisY.Interval = 10;

        chart.ChartAreas[0].AxisX.LabelStyle.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
        chart.ChartAreas[0].AxisY.LabelStyle.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);

        chart.ChartAreas[0].AxisX.Interval = 1;

        chart.ChartAreas[0].AxisX.MinorGrid.Enabled = false;

        chart.ChartAreas[0].AxisY.LabelStyle.Format = "{0:0}";

        //chart.ChartAreas[0].AxisY.Minimum =Convert.ToDouble(dt.Rows[0]["minvalue"])-20;
        //chart.ChartAreas[0].AxisY.Maximum = Convert.ToDouble(dt.Rows[0]["maxvalue"]);

        //chart.ChartAreas[0].AxisY.Minimum =40.82F-1 ;
        //chart.ChartAreas[0].AxisY.Maximum =80.32F+1;
        chart.Series[0].SmartLabelStyle.Enabled = true;
        chart.Series[0].SmartLabelStyle.IsMarkerOverlappingAllowed = false;
        chart.Series[0].SmartLabelStyle.IsOverlappedHidden = true;
        //chart.Series[0].IsValueShownAsLabel = true;
        
                
        /*chart.Series[0].Label = "#VALY";
        chart.Series[0].LabelAngle = 90;*/
        

        //
        chart.ChartAreas[0].AxisX.MajorTickMark.Enabled = true;
        chart.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
        chart.ChartAreas[0].AxisX.MinorGrid.Enabled = false;
        chart.ChartAreas[0].AxisX.MinorTickMark.Enabled = false;
        chart.ChartAreas[0].AxisY.MajorTickMark.Enabled = true;
        chart.ChartAreas[0].AxisY.MajorGrid.Enabled = true;
        chart.ChartAreas[0].AxisY.MinorGrid.Enabled = false;
        chart.ChartAreas[0].AxisY.MinorTickMark.Enabled = false;

        chart.ChartAreas[0].AxisX.LabelStyle.Angle = -90;

        
        chart.Series[0].CustomProperties = "PointWidth=1";

        chart.ChartAreas[0].AxisX.LineWidth = 0;
        chart.ChartAreas[0].AxisY.LineWidth = 0;

        StripLine stripLine1 = new StripLine();
        stripLine1.StripWidth = 0;
        stripLine1.BorderColor = Color.LightGray;
        stripLine1.BorderWidth = 1;

        stripLine1.Interval = 1;



        //stripLine1.BorderDashStyle = ChartDashStyle.Dash;

        StripLine stripLine2 = new StripLine();
        stripLine2.StripWidth = 0;
        stripLine2.BorderColor = Color.LightGray;
        stripLine2.BorderWidth = 1;
        stripLine2.Interval = 10;
        stripLine2.BorderDashStyle = ChartDashStyle.Dash;
        /*chart.ChartAreas[0].AxisX.StripLines.Add(stripLine1);*/
        chart.ChartAreas[0].AxisY.StripLines.Add(stripLine2);


        System.Drawing.Color col = System.Drawing.ColorTranslator.FromHtml("#C6D9F1");

        Color newColor = Color.FromArgb(150, col);

        System.Drawing.Color colBor = System.Drawing.ColorTranslator.FromHtml("#B0AFDD");

        chart.Series[0].BorderColor = colBor;

        chart.Series[0].BorderWidth = 1;

        chart.Series[0].Color = newColor;

        Stream sr = new MemoryStream();

        chart.SaveImage(sr);

        System.Drawing.Image img = System.Drawing.Image.FromStream(sr);

        return img;

    }

    #endregion charts
}