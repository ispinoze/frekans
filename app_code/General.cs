﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Web.Services;
using DbClass;
using System.Drawing;
using System.Collections;

/// <summary>
/// Summary description for General
/// </summary>
public class General
{

    DbClassMysql db = new DbClassMysql();

	public General()
	{

	}
    

    public void MetaYaz(Page page)
    {

        string siteBaslik,siteAciklama,anahtarKelimeler,favicon;
        siteBaslik="";siteAciklama="";anahtarKelimeler="";favicon="";
        string sqlMeta = "SELECT * FROM tbl_genel_ayar";

            DataTable dtMeta = db.Fill(sqlMeta);

        if (dtMeta.Rows.Count!=0)
	    {
		siteBaslik=dtMeta.Rows[0]["SITE_BASLIK"].ToString();
        siteAciklama=dtMeta.Rows[0]["SITE_ACIKLAMA"].ToString();
        anahtarKelimeler=dtMeta.Rows[0]["ANAHTAR_KELIMELER"].ToString();
        favicon=dtMeta.Rows[0]["FAVICON"].ToString(); 
	    }

        page.Header.Title = siteBaslik;
        
        HtmlMeta hMetaDes = new HtmlMeta();

        hMetaDes.Name = "description";
        hMetaDes.Content = siteAciklama;

        page.Header.Controls.Add(hMetaDes);

        HtmlMeta hMetaKey = new HtmlMeta();

        hMetaKey.Name = "keywords";
        hMetaKey.Content = anahtarKelimeler;

        page.Header.Controls.Add(hMetaKey);


        
        #region defaultMeta

        HtmlMeta hMetaAut = new HtmlMeta();
        hMetaAut.Name = "author";
        hMetaAut.Content = "Murat KIRMIZIGÜL";

        page.Header.Controls.Add(hMetaAut);

        HtmlMeta hMetaDis = new HtmlMeta();

        hMetaDis.Name = "distribution";
        hMetaDis.Content = "Global";

        page.Header.Controls.Add(hMetaDis);

        HtmlMeta hMetaCon = new HtmlMeta();

        hMetaCon.Name = "content-language";
        hMetaCon.Content = "tr";

        page.Header.Controls.Add(hMetaCon);

        HtmlMeta hMetaRob1 = new HtmlMeta();

        hMetaRob1.Name = "robots";
        hMetaRob1.Content = "index, follow";

        page.Header.Controls.Add(hMetaRob1);


        HtmlLink faviconLink = new HtmlLink();

        faviconLink.Href = favicon;
        faviconLink.Attributes.Add("rel", "icon");
        faviconLink.Attributes.Add("type", "image/x-icon");
        page.Header.Controls.Add(faviconLink);

        #endregion defaultMeta

    }



    public void cssYukle(Page page)
    {

        HtmlLink stylesheetLink = new HtmlLink();
        HtmlLink stylesheetLink2 = new HtmlLink();
        // Adding attributes based on std html link tag for stylesheet
        // example output: <link type="text/css" rel="stylesheet" href="" />
        stylesheetLink.Attributes.Add("type", "text/css");
        stylesheetLink.Attributes.Add("rel", "stylesheet");
        stylesheetLink2.Attributes.Add("type", "text/css");
        stylesheetLink2.Attributes.Add("rel", "stylesheet");
        
        // Browser Detection for IE or Chrome
        if (HttpContext.Current.Request.Browser.Browser.Contains("IE"))
        {
            // Attaching stylesheet url to href attribute of link
            stylesheetLink.Attributes.Add("href", "~/css/MainExp.css");
            stylesheetLink2.Attributes.Add("href", "~/css/sbimenu.css");
            
        }
        else if (HttpContext.Current.Request.Browser.Browser.Contains("Chrome"))
        {
            stylesheetLink.Attributes.Add("href", "~/css/Main.css");
            stylesheetLink2.Attributes.Add("href", "~/css/sbimenu.css");
        }
        else if (HttpContext.Current.Request.Browser.Browser.Contains("Firefox"))
        {
            stylesheetLink.Attributes.Add("href", "~/css/MainFirefox.css");
            stylesheetLink2.Attributes.Add("href", "~/css/sbimenu_ff.css");
        }
        else
        {
            stylesheetLink.Attributes.Add("href", "~/css/Main.css");
            stylesheetLink2.Attributes.Add("href", "~/css/sbimenu.css");
        }
        // Adding control to Page's header section
        page.Header.Controls.Add(stylesheetLink);
        page.Header.Controls.Add(stylesheetLink2);
        
    
    }

    public class Article
    {
        private string title;
        public string Title
        {
            get { return title; }
            set { title = value; }

        }
        private string site_name;
        public string Site_Name
        {
            get { return site_name; }
            set { site_name = value; }

        }

        private string id;
        public string ID
        {

            get { return id; }
            set { id = value; }

        }

        private long app_id;
        public long APP_ID
        {

            get { return app_id; }
            set { app_id = value; }

        }

        private string desc;
        public string Desc
        {

            get { return desc; }
            set { desc = value; }

        }

        private string image_path;
        public string Image_Path
        {
            get { return image_path; }
            set { image_path = value; }

        }

        private string content_type;
        public string Content_Type
        {
            get { return content_type; }
            set { content_type = value; }

        }


    }

    public void MetaGenerator(Article makale, Page page)
    {
        //<meta property="fb:admins" content="700518184" />
        HtmlMeta meta = new HtmlMeta();
        meta.Attributes.Add("property", "og:site_name");
        meta.Attributes.Add("content", makale.Site_Name);
        page.Header.Controls.Add(meta);

        meta = new HtmlMeta();
        meta.Attributes.Add("property", "og:title");
        meta.Attributes.Add("content", makale.Title);
        page.Header.Controls.Add(meta);

        meta = new HtmlMeta();
        meta.Attributes.Add("property", "og:type");
        meta.Attributes.Add("content", makale.Content_Type);
        page.Header.Controls.Add(meta);

        meta = new HtmlMeta();
        meta.Attributes.Add("property", "og:url");

        meta.Attributes.Add("content", "http://www.beforeafter.com.tr/items.aspx?"+makale.ID);
        page.Header.Controls.Add(meta);

        meta = new HtmlMeta();
        meta.Attributes.Add("property", "og:description");
        meta.Attributes.Add("content", makale.Desc);
        page.Header.Controls.Add(meta);


        //meta = new HtmlMeta();
        //meta.Attributes.Add("property", "fb:admins");
        //meta.Attributes.Add("content", Convert.ToString(makale.APP_ID));
        //page.Header.Controls.Add(meta);

        meta = new HtmlMeta();
        meta.Attributes.Add("property", "og:image");
        meta.Attributes.Add("content", makale.Image_Path);
        page.Header.Controls.Add(meta);
    }

    public void GecerMi(string refno, Page pg, string tabloAdi,string kolonAdi)
    {
        int max = 0;

        if (tabloAdi != "yazi")
        {
            max = Convert.ToInt32(db.ScalarQuery("SELECT max("+kolonAdi+") from " + tabloAdi + ""));
        }

        int sayfa_id;

        if (int.TryParse(refno, out sayfa_id))
        {
            if (sayfa_id > max & sayfa_id != max)
            {
                pg.Response.Redirect("Default.aspx");
            }

        }
        else
        {
            pg.Response.Redirect("Default.aspx");
        }

    }
    public bool GecerMi2(string kullaniciAdi, string sifre)
    {
        bool doner = true;

        string[] yasakliKelimeler = { "--", "SELECT", "UNION", "OR", "AND", "SP_", "<>", "%", "EXECUTE", "EXEC", "select", "union", "or", "and", "sp_", "execute", "exec" };

        for (int i = 0; i < yasakliKelimeler.Count(); i++)
        {
            if (kullaniciAdi.IndexOf(yasakliKelimeler[i].ToString()) != -1)
            {
                doner = false;
                break;
            }
            else if (sifre.IndexOf(yasakliKelimeler[i].ToString()) != -1)
            {
                doner = false;
                break;
            }
        }

        return doner;
    }


}