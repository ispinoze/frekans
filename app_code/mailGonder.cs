﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Net;
using DbClass;
using System.Data;

/// <summary>
/// Summary description for mailGonder
/// </summary>
public class mailGonder
{
	public mailGonder()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    DbClassMysql db = new DbClassMysql();

    public void MailSent(string mail,string konu,string subject,string bcc)
    {

        string sqlAyar = "SELECT * FROM tbl_eposta_ayar";

        

        DataTable dt = db.Fill(sqlAyar);

        MailMessage mailSent = new MailMessage();

        mailSent.From = new MailAddress(dt.Rows[0]["EPOSTA_ADRES"].ToString(), konu);

        //mailSent.Bcc.Add(new MailAddress("muratkirmizigul@gmail.com"));

        mailSent.IsBodyHtml = true;

        mailSent.Subject = subject;

        SmtpClient smtp = new SmtpClient(dt.Rows[0]["SMTP_HOST"].ToString(), Convert.ToInt32(dt.Rows[0]["SMTP_PORT"]));

        mailSent.Body = mail;

        smtp.Credentials = new NetworkCredential(dt.Rows[0]["EPOSTA_ADRES"].ToString(), dt.Rows[0]["SIFRE"].ToString());

        try
        {
            smtp.Send(mailSent);
        }
        catch (Exception)
        {

            throw;
        }

    }

    public static void MailSentStatic(string mail,string konu, string subject, string bcc)
    {
        string sqlAyar = "SELECT * FROM tbl_eposta_ayar";

        DataTable dt = DbClassMysql.Fill2(sqlAyar);

        MailMessage mailSent = new MailMessage();

        mailSent.From = new MailAddress(dt.Rows[0]["EPOSTA_ADRES"].ToString(), konu);

        if (bcc!="")
        {
            mailSent.Bcc.Add(new MailAddress(bcc)); 
        }

        mailSent.IsBodyHtml = true;
        
        mailSent.Subject = subject;

        SmtpClient smtp = new SmtpClient(dt.Rows[0]["SMTP_HOST"].ToString(), Convert.ToInt32(dt.Rows[0]["SMTP_PORT"]));

        mailSent.To.Add(new MailAddress(dt.Rows[0]["EPOSTA_ADRES"].ToString(), konu));
       
        mailSent.Body = mail;

        smtp.Credentials = new NetworkCredential(dt.Rows[0]["EPOSTA_ADRES"].ToString(), dt.Rows[0]["SIFRE"].ToString());

        try
        {
            smtp.Send(mailSent);
        }
        catch (Exception)
        {

            throw;
        }

    }
}