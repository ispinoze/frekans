﻿var map;

function goto_map_position() {

    var lat = document.forms[0].txtLatitude.value;
    var lng = document.forms[0].txtLongitude.value;
    map.panTo(new GLatLng(lat, lng));
    map.setCenter(new GLatLng(lat, lng),19);
    var marker = new GMarker(map.getCenter());
    map.addOverlay(marker);
    var infoTabs = [
          new GInfoWindowTab("Yer", document.forms[0].txtName.value),
          new GInfoWindowTab("Açıklama", document.forms[0].txtDescription.value)
       ];
    marker.openInfoWindowTabsHtml(infoTabs);

}

function load() {
    if (GBrowserIsCompatible()) {
        //Harita nesnesi
        map = new GMap2(document.getElementById("map_canvas"));

        map.addControl(new GSmallMapControl());

        map.addControl(new google.maps.LocalSearch(), new GControlPosition(G_ANCHOR_BOTTOM_RIGHT, new GSize(10, 20)));

        GSearch.setOnLoadCallback(load);
        map.addControl(new GMenuMapTypeControl());

        //Harita Tipi
        map.setMapType(G_SATELLITE_MAP);

        //Baslangic koordinat ve zoom değerleri
        map.setCenter(new GLatLng(38.75408327579141, 36.38671875), 5);

        //Belirtilen koordinatlara koyulan işağretçi
        var Koordinat = new GLatLng(38.75408327579141, 36.38671875);
        Isagretci = new GMarker(Koordinat);
        map.addOverlay(Isagretci);

        //Açıklama baloncuğu eklemek
        map.openInfoWindow(map.getCenter(),
                    document.createTextNode("Fotoğraf nerede çekildi ?"));

        //Harita üzerinde çift tıklanınca çalışan olaylar
        GEvent.addListener(map, "dblclick", function (overlay, point) {


            //Eklenen isagretcinin tıklanan yere kaydırılması
            Isagretci.setPoint(point);

            var center = map.getCenter();
            //  isteğe göre aktif / pasif edilebilir
            //  Eğer sayfa yüklenirken datadan koordinatları çekecekseniz ilgili txt lere bu koordinatları yazdırmanız lazım ki o koordinatlara gitsin,

            document.forms[0].txtLatitude.value = center.lat();
            document.forms[0].txtLongitude.value = center.lng();
            document.forms[0].txtLongitude0.value = map.getZoom();
  
        });



        //Haritaya çift tıklayınca zoom yapması önleniyor.
        map.disableDoubleClickZoom();

    }
}