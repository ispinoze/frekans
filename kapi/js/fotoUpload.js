﻿// check extension of file to be upload
function checkFileExtension(file) {
    var flag = true;
    var extension = file.substr((file.lastIndexOf('.') + 1));

    switch (extension) {
        case 'jpg':
           case 'JPG':
            flag = true;
            break;
        default:
            flag = false;
    }

    return flag;
}

function checkFileExtension2(file) {
    var flag = true;
    var extension = file.substr((file.lastIndexOf('.') + 1));

    switch (extension) {
        case 'XLS':
        case 'xls':
        case 'xlsx':

        case 'XLSX':


            flag = true;
            break;
        default:
            flag = false;
    }

    return flag;
}

//get file path from client system
function getNameFromPath(strFilepath) {

    var objRE = new RegExp(/([^\/\\]+)$/);
    var strName = objRE.exec(strFilepath);

    if (strName == null) {
        return null;
    }
    else {
        return strName[0];
    }

}


var refno;

$(document).ready(function () {
    $.ajax({
        type: "POST",
        url: "projeDetay.aspx/GetSession",
        data: "{session:'refno'}",
        dataType: "json",
        cache: "false",
        contentType: "application/json; charset=utf-8",
        success: function (msg) {

            

            refno = msg.d;
        }
    });  //ajax  

});


var pinText = "";

function ajaxFileUpload2() {// ajaxfileXls

    var pin = 0;

    if (xlsFilesize>0) {
        
        UyariX("Sadece 1 tane ölçüm dosyası yükleyebilirsiniz.");

        return false;
    }

    if ($("#pin").text() == 0) {

        UyariX("Excel dosyası yüklemeden önce ekrana pin atmalısınız !");
        
        return false;
    }

    else {
        // alert($("#pin").text());

        pin = $("#pin").text();

        if (pinText === pin) {
            pinText = "";
            return;
        }

        pinText = pin;
    }

        var fi = document.getElementById('fileToXls');

        var mb = 1024 * 1024;

        var size = fi.files[0].size / mb;

        //alert(size);

        var FileFolder = $('#hdnFileFolder').val();
        var fileToUpload = getNameFromPath($('#fileToXls').val());
        var filename = fileToUpload.substr(0, (fileToUpload.lastIndexOf('.')));

        if (checkFileExtension2(fileToUpload)) {

        if (filename != "" && filename != null && FileFolder != "0") {
            //Check duplicate file entry

                $("#loading").ajaxStart(function () {
                    $(this).show();
                }).ajaxComplete(function () {
                    $(this).hide();
                    return false;
                });

                $.ajaxFileUpload({
                    url: 'FileUpload2.ashx?id=' + FileFolder + "'&pin='" + $("#pin").text() + "'",
                    secureuri: false,
                    fileElementId: 'fileToXls',
                    dataType: 'json',
                    success: function (data, status) {

                        if (typeof (data.error) != 'undefined') {
                            if (data.error != '') {
                                alert(data.error);
                            } else {

                                
                                
                                ShowUploadedFiles2(data.upfile);
                                
                                //$("#ContentPlaceHolder1_lblDosya").text(""+fileToUpload+"");
                                
                            }
                        }
                    },
                    error: function (data, status, e) {
                        Uyari(e);
                    }
                });
                    }

           
        }
        else {

            UyariX("Ölçüm dosyaları bölümüne sadece xls veya xlsx uzantılı dosyalar yükleyebilirsiniz.");

        }
        return false;
        
}// ajaxfileXls

var pinText = "";
// Asynchronous file upload process
function ajaxFileUpload() {

    var pin = 0;
    if ($("#pin").text() == 0) {

        UyariX("Fotoğraf dosyası yüklemeden önce ekrana pin atmalısınız !");
        $("#ContentPlaceHolder1_lblDosya").text("Dosyası yüklenmeyecek ! ");
        return false;
    }
    else {
        // alert($("#pin").text());

        pin = $("#pin").text();

        if (pinText === pin) {
            pinText = "";
            return;
        }

        pinText = pin;
    }

    var fi = document.getElementById('fileToUpload');

    var mb = 1024 * 1024;

    var size = fi.files[0].size / mb;

    //alert(size);

    var FileFolder = $('#hdnFileFolder').val();
    var fileToUpload = getNameFromPath($('#fileToUpload').val());
    var fileGercek = fileToUpload;
    var filename = fileToUpload.substr(0, (fileToUpload.lastIndexOf('.')));

    fileToUpload = filename + "[" + refno + ".jpg";
    
    filename = filename + "[" + refno;

    if (checkFileExtension(fileGercek)) {

        var flag = true;
        var counter = $('#hdnCountFiles').val();

        if (filename != "" && filename != null && FileFolder != "0") {
            //Check duplicate file entry
            for (var i = 1; i <= counter; i++) {
                var hdnDocId = "#hdnDocId_" + i;

                if ($(hdnDocId).length > 0) {
                    var mFileName = "#lblfilename_" + i;
                    if ($(mFileName).html() == filename) {
                        flag = false;
                        break;
                    }
                }
            }
            if (flag == true) {
                $("#loading").ajaxStart(function () {
                    $(this).show();
                }).ajaxComplete(function () {
                    $(this).hide();
                    return false;
                });


                if (size > 50) {

                    UyariX("Yükleyeceğiniz fotoğraf dosyasının boyutu maksimum 5 MB olmalıdır.");
                    

                    return;

                }

                $.ajaxFileUpload({
                    url: 'FileUpload.ashx?id=' + FileFolder+"'&pin='"+pin+"'",
                    secureuri: false,
                    fileElementId: 'fileToUpload',
                    dataType: 'json',
                    success: function (data, status) {

                        if (typeof (data.error) != 'undefined') {
                            if (data.error != '') {
                                alert(data.error);
                            } else {

                                ShowUploadedFiles(data.upfile);
                            }
                        }
                    },
                    error: function (data, status, e) {
                        Uyari(e);
                    }
                });
            }
            else {
                UyariX('' + filename + ' dosyası daha önce yüklendi. ')
                return false;
            }
        }
    }
    else {

        UyariX("Fotoğraflar bölümüne sadece jpg uzantılı dosyalar yükleyebilirsiniz.");
        
    }
    return false;

}


//show uploaded file 
function ShowUploadedFiles(file) {

    count = parseInt($("#hdnCountFiles").val()) + 1;
    var hdnid = 'hdnPhotoId_' + count;
    var hdnidT = 'hdnPhotoIdT_' + 1;
    var txtDocDescId = 'txtPhotoDesc_' + count;
    var lblfilename = 'lblfilePhoto_' + count;
    var path = 'UploadFiles';
    var bol = file.split('[');
    $("#uploadedDiv table>tbody").append(""
        + "<tr id=" + hdnidT + "><td style='width:80%'><div id='" + hdnid + "' class='fotoYuklenen'><span title=" + file + " class='xlsDosyaAdi' id='" + lblfilename + "' style='width:250px;float:left;margin-left:40px;'>" + bol[0] + "</span>"
        +"</td><td style='text-align:center;'><a href='javascript:' title='" + file + "' name='' class='dellink' onclick='deleterow(\"#" + hdnidT + "," + file + "\")'><img src='images/remove.png'/></a>&nbsp;&nbsp;<a href='down.ashx?type=img&fileName="+file+"' title='" + file + "' name='' class='dellink')'><img src='images/download.png'/></a> </span>" // for deleting file
        + "</td></tr>");

    //update file counter
    $("#hdnCountFiles").val(count);
    return false;

}
var xlsFilesize;
function ShowUploadedFiles2(file) {

    count = parseInt($("#hdnCountFilesXls").val()) + 1;
    var hdnid = 'hdnDocId_' + 1;
    var hdnidT = 'hdnDocIdT_' + 1;
    var txtDocDescId = 'txtDocDesc_' + 1;
    var lblfilename = 'lblfileXls_' + 1;
    var path = 'xlsFiles';

    var bol = file.split(']');
    $("#uploadedDivXls table>tbody").append("<tr id=" + hdnidT + "><td style='width:80%'><div id='" + hdnid + "' class='fotoYuklenen'><span title='" + file + "' class='xlsDosyaAdi' id='" + lblfilename + "' style='width:250px;float:left;margin-left:40px;'>" + bol[0] + "</span>"
        + "</td><td  style='text-align:center;'><a href='javascript:' title='" + file + "' name='' class='dellink' onclick='deleterow2(\"#" + hdnidT + "," + file + "\")'><img src='images/remove.png'/></a>&nbsp;&nbsp;<a href='down.ashx?type=xls&fileName=" + file + "' title='" + file + "' name='' class='dellink')'><img src='images/download.png'/></a> </span>" // for deleting file
        + "</td></tr>");

    //update file counter
    $("#hdnCountFilesXls").val(count);
    xlsFilesize = count;
    return false;

}

// delete existing file
function deleterow(divrow) {
    var str = divrow.split(",");
    var row = str[0];
    var file = str[1];
    var path = 'UploadFiles';
    if (confirm('Dosyayı silmek istediğinize emin misiniz?')) {
        $.ajax({
            url: "FileUpload.ashx?path=" + path + "&file=" + file,
            type: "GET",
            cache: false,
            async: true,
            success: function (html) {

                $.ajax({
                    type: "POST",
                    traditional: true,
                    url: "projeDetay.aspx/pinSilPhoto",
                    data: "{photo_file:'" + file + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {

                        var sonuc = msg.d;

                    }
                }); //ajax  

            }
        });

        $(row).remove();

        

    }
    return false;
}


function deleterow2(divrow) {
    var str = divrow.split(",");
    var row = str[0];
    var file = str[1];
    var path = 'xlsFiles';
    if (confirm('Dosyayı silmek istediğinize emin misiniz?')) {
        $.ajax({
            url: "FileUpload2.ashx?path=" + path + "&file=" + file,
            type: "GET",
            cache: false,
            async: true,
            success: function (html) {
                $.ajax({
                    type: "POST",
                    traditional: true,
                    url: "projeDetay.aspx/pinSilXls",
                    data: "{xls_file:'" + file + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {

                    }
                }); //ajax  
            }
        });
        $(row).remove();
    }
    xlsFilesize = 0;
    return false;
}


function saveRow2(divrow) {
    var str = divrow.split(",");
    var row = str[0];
    var file = str[1];
    var path = 'xlsFiles';
    if (confirm('Dosyayı indirmek istediğinize emin misiniz?')) {
        
        $.ajax({
            type: "POST",
            traditional: true,
            url: "projeDetay.aspx/pinSaveXls",
            data: "{xls_file:'" + file + "'}",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (msg) {

            }
        }); //ajax  
        
    }
    xlsFilesize = 0;
    return false;
}