﻿var h = 50;
var w = 50;

function Load() {


    $(function () {

        $("#h").blur(function () {
            h = 50;
            w = 50;

            h = $("#h").val();
            w = $("#w").val();
            
            var jcrop_api= $('#originalImage').Jcrop({
                onChange: showCoords,
                onSelect: showCoords,
                setSelect: [0, 0, w, h]
                

            });

        });
    });
    // Patch for IE to force "overflow: auto;"
    document.getElementById("imgContainer").style["position"] = "relative";


}





function showCoords(c) {
    $('#x1').val(c.x);
    $('#y1').val(c.y);
    $('#x2').val(c.x2);
    $('#y2').val(c.y2);
    $('#w').val(c.w);
    $('#h').val(c.h);

};



function ValidateSelected() {
    if (document.getElementById("w").value == "" || document.getElementById("w").value == "0"
        || document.getElementById("h").value == "" || document.getElementById("h").value == "0") {
        alert("Kırpma yapabilmek için yükselik ve genişlik değeri giriniz.");
        return false;
    }
}

function validateBoyut() {
    if (document.getElementById("TextBox1").value>2000) {
        alert("Hatalı genişlik miktarı");
        return false;
    }
}


