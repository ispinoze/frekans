﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OleDb;
using System.Web.UI.HtmlControls;

using System.Web.Services;
using DbClass;
using System.Data.SqlClient;


public partial class Yonetim_Yazi : System.Web.UI.Page
{

    DbClassMysql db = new DbClassMysql();

    protected void Page_Load(object sender, EventArgs e)
    {


        if (!IsPostBack)
        {
            

            if (Convert.ToBoolean(Session["ADMIN_GIRIS"]))
            {

                string sql = "SELECT * FROM tbl_iletisim ORDER BY ID DESC";

                DataTable dt = db.Fill(sql);

                GridView1.DataSource = dt;
                GridView1.DataBind();

                for (int i = 0; i < GridView1.Rows.Count; i++)
                {
                    Image img = (Image)GridView1.Rows[i].FindControl("Image1");

                    string sqlDurum = "SELECT DURUM FROM tbl_iletisim WHERE ID="+dt.Rows[i]["ID"].ToString();

                    bool durum = Convert.ToBoolean(db.ScalarQuery(sqlDurum));

                    if (durum)
                    {
                        img.ImageUrl = "~/Kapi/images/icons/ok.png";
                    }
                    else
                    {
                        img.ImageUrl = "~/Kapi/images/icons/ok_not.png";
                    }
                }

                Panel1.Visible = true;
                Panel2.Visible = false;
                
            }
            else
            {
                Response.Redirect("Default.aspx");
            } 
        }
        string sqlBilgi = "SELECT * FROM tbl_bilgi where SAYFA_REFNO=" + Request["refno"].ToString() + "";

        DataTable dtBilgi = db.Fill(sqlBilgi);

        string bilgi = "";

        if (dtBilgi.Rows.Count != 0)
        {
            bilgi = dtBilgi.Rows[0]["ACIKLAMA"].ToString();

            bilgi = bilgi.Replace('\'', ' ');
        }
        else
        {
            bilgi = "Yardım içeriği bulunmamaktadır.";
        }

        pnlBilgi.Controls.Add(new LiteralControl("<a class='cep' title='" + bilgi + "' href='#'><img src='images/image/soru_32x.png' style='border:0; vertical-align:middle' alt='Soru' /></a>")); 
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        Label14.Text = "İletişim Bilgisi Güncelle";
        string haberRefNo = GridView1.SelectedDataKey.Value.ToString();

        string sql = "SELECT * FROM tbl_iletisim WHERE ID="+haberRefNo;

        DataTable dt = db.Fill(sql);

        txtAdres.Text = dt.Rows[0]["ILETISIM_ADI"].ToString();
        txtYAZI_KONUSU.Text = dt.Rows[0]["ADRES"].ToString();
        CheckBox2.Checked=Convert.ToBoolean(dt.Rows[0]["DURUM"]);
        lblRef.Text = dt.Rows[0]["ID"].ToString();
        txtEmail.Text = dt.Rows[0]["EMAIL"].ToString();
        txtTlf.Text = dt.Rows[0]["TEL_1"].ToString();
        txtFax.Text = dt.Rows[0]["FAX"].ToString();
        txtTlf0.Text = dt.Rows[0]["TEL_2"].ToString();
        txtAciklama.Text = dt.Rows[0]["ACIKLAMA"].ToString();
        string[] harita = dt.Rows[0]["HARITA_BILGISI"].ToString().Split('|');

        txtLatitude.Text = harita[0].ToString();
        txtLongitude.Text = harita[1].ToString();
        txtLongitude0.Text = harita[2].ToString();

        txtDescription.Text = dt.Rows[0]["HARITA_ACIKLAMA"].ToString();

        

        Session["0"]=txtLatitude.Text;
        Session["1"] = txtLongitude.Text;
        Session["2"]=txtLongitude0.Text;
        Session["3"]=txtDescription.Text;

        Panel1.Visible = false;
        Panel2.Visible = true;

    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        string sql = "";

            SqlParameter prm1 = new SqlParameter("@P1", txtAdres.Text);
            SqlParameter prm2 = new SqlParameter("@P2",  txtYAZI_KONUSU.Text);
            SqlParameter prm3 = new SqlParameter("@P3", txtTlf.Text);
            SqlParameter prm4 = new SqlParameter("@P4", txtTlf0.Text);
            SqlParameter prm5 = new SqlParameter("@P5", txtAciklama.Text);
            SqlParameter prm6 = new SqlParameter("@P6", txtFax.Text);
            SqlParameter prm7 = new SqlParameter("@P7", txtEmail.Text);
  
            SqlParameter prm8 = new SqlParameter("@P8", CheckBox2.Checked);

            string harita = txtLatitude.Text + "|" + txtLongitude.Text + "|" + txtLongitude0.Text;

            if (FileUpload1.HasFile)
            {
                txtDescription.Text = "Admin/images/icons/" + FileUpload1.FileName;

                FileUpload1.SaveAs(Request.PhysicalApplicationPath + "Admin/images/icons/" + FileUpload1.FileName);
                
            }

            SqlParameter prm9 = new SqlParameter("@P9", harita);

            SqlParameter prm10 = new SqlParameter("@P10",txtDescription.Text );


            SqlParameter prm11 = new SqlParameter("@P11", lblRef.Text);
            

            if (lblRef.Text != "")
        {


            sql = "UPDATE tbl_iletisim SET ILETISIM_ADI=@P1,ADRES=@P2,TEL_1=@P3,TEL_2=@P4,ACIKLAMA=@P5,FAX=@P6,EMAIL=@P7,DURUM=@P8,HARITA_BILGISI=@P9,HARITA_ACIKLAMA=@P10 WHERE ID=@P11";

            db.ExecuteNonQuery(sql, prm1, prm2, prm3, prm4, prm5, prm6, prm7, prm8, prm9, prm10, prm11);
                
        }

        else
        {
            sql = "INSERT INTO tbl_iletisim(ILETISIM_ADI,ADRES,TEL_1,TEL_2,ACIKLAMA,FAX,EMAIL,DURUM,HARITA_BILGISI,HARITA_ACIKLAMA) VALUES(@P1,@P2,@P3,@P4,@P5,@P6,@P7,@P8,@P9,@P10)";

            db.ExecuteNonQuery(sql, prm1, prm2, prm3, prm4, prm5, prm6, prm7, prm8, prm9, prm10);

        }

        string sql1 = "SELECT * FROM tbl_iletisim ORDER BY ID DESC";
        DataTable dt1 = db.Fill(sql1);

        GridView1.DataSource = dt1;
        GridView1.DataBind();

        int page = 0;

        for (int i = 0; i < GridView1.Rows.Count; i++)
        {
            Image img = (Image)GridView1.Rows[i].FindControl("Image1");

            string sqlDurum = "SELECT DURUM FROM tbl_iletisim WHERE ID=" + dt1.Rows[i]["ID"].ToString();

            bool durum = Convert.ToBoolean(db.ScalarQuery(sqlDurum));

            if (durum)
            {
                img.ImageUrl = "~/Kapi/images/icons/ok.png";
            }
            else
            {
                img.ImageUrl = "~/Kapi/images/icons/ok_not.png";
            }
        }

        Panel1.Visible = true;
        Panel2.Visible = false;

    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        Panel1.Visible = true;
        Panel2.Visible = false;
    }

    protected void Button5_Click(object sender, EventArgs e)
    {
        string yaziRefNo = GridView1.SelectedDataKey.Value.ToString();

        string sql = "DELETE FROM tbl_iletisim WHERE ID="+yaziRefNo;

        db.ExecuteNonQuery(sql);

        string sql1 = "SELECT * FROM tbl_iletisim ORDER BY ID DESC";
        DataTable dt1 = db.Fill(sql1);

        GridView1.DataSource = dt1;
        GridView1.DataBind();


        for (int i = 0; i < GridView1.Rows.Count; i++)
        {
            Image img = (Image)GridView1.Rows[i].FindControl("Image1");

            string sqlDurum = "SELECT DURUM FROM tbl_iletisim WHERE ID=" + dt1.Rows[i]["ID"].ToString();

            bool durum = Convert.ToBoolean(db.ScalarQuery(sqlDurum));

            if (durum)
            {
                img.ImageUrl = "~/Kapi/images/icons/ok.png";
            }
            else
            {
                img.ImageUrl = "~/Kapi/images/icons/ok_not.png";
            }
        }

        Panel1.Visible = true;
        Panel2.Visible = false;

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        Label14.Text = "Yeni İletişim Bilgisi Ekle";
        txtFax.Text = "";
        txtLatitude.Text = "";
        txtLongitude.Text = "";
        txtAdres.Text = "";
        txtLongitude0.Text = "";
        txtTlf.Text = "";
        txtEmail.Text = "";
        txtDescription.Text = "";
        txtYAZI_KONUSU.Text = "";
        CheckBox2.Checked = false;
        lblRef.Text = "";
        Panel2.Visible = true;
        Panel1.Visible = false;
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string yaziRefNo = GridView1.DataKeys[e.RowIndex].Values[0].ToString();


        string sql = "DELETE FROM tbl_iletisim WHERE ID=" + yaziRefNo;

        db.ExecuteNonQuery(sql);

        string sql1 = "SELECT * FROM tbl_iletisim ORDER BY ID DESC";
        DataTable dt1 = db.Fill(sql1);

        GridView1.DataSource = dt1;
        GridView1.DataBind();

        int page = 0;

        if (dt1.Rows.Count < 10)
        {
            page = dt1.Rows.Count;
        }
        else
        {
            page = 10;
        }

        for (int i = 0; i < dt1.Rows.Count; i++)
        {
            Image img = (Image)GridView1.Rows[i].FindControl("Image1");

            string sqlDurum = "SELECT DURUM FROM tbl_iletisim WHERE ID=" + dt1.Rows[i]["ID"].ToString();

            bool durum = Convert.ToBoolean(db.ScalarQuery(sqlDurum));

            if (durum)
            {
                img.ImageUrl = "~/Kapi/images/icons/ok.png";
            }
            else
            {
                img.ImageUrl = "~/Kapi/images/icons/ok_not.png";
            }
        }

        Panel1.Visible = true;
        Panel2.Visible = false;
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;

        string sql1 = "SELECT * FROM tbl_iletisim ORDER BY ID DESC";
        DataTable dt1 = db.Fill(sql1);

        GridView1.DataSource = dt1;
        GridView1.DataBind();



        int i = 0;

        int sayfa = 0;

        if (GridView1.PageIndex == 0)
        {


            i = sayfa;
        }
        else if (GridView1.PageIndex == 1)
        {
            sayfa = 10;

        }
        else
        {
            sayfa = 10;

            sayfa *= GridView1.PageIndex;


        }
     
        for (; i < GridView1.Rows.Count; i++)
        {
            Image img = (Image)GridView1.Rows[i].FindControl("Image1");

            bool durum = Convert.ToBoolean(dt1.Rows[sayfa]["DURUM"]);

            if (durum)
            {
                img.ImageUrl = "~/Kapi/images/icons/ok.png";
            }
            else
            {
                img.ImageUrl = "~/Kapi/images/icons/ok_not.png";
            }
            sayfa++;
        }

        Panel1.Visible = true;
        Panel2.Visible = false;
    }

    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        for (int j = 0; j < GridView1.Rows.Count; j++)
        {
            CheckBox chk = GridView1.Rows[j].FindControl("CheckBox1") as CheckBox;

            if (chk.Checked == true)
            {
                string sql = "DELETE  FROM tbl_iletisim WHERE ID=" + chk.ToolTip + "";
                db.ExecuteNonQuery(sql);
            }
        }

        string sql1 = "SELECT * FROM tbl_iletisim ORDER BY ID DESC";
        DataTable dt1 = db.Fill(sql1);

        GridView1.DataSource = dt1;
        GridView1.DataBind();

        for (int i = 0; i < GridView1.Rows.Count; i++)
        {
            Image img = (Image)GridView1.Rows[i].FindControl("Image1");

            string sqlDurum = "SELECT DURUM FROM tbl_iletisim WHERE ID=" + dt1.Rows[i]["ID"].ToString();

            bool durum = Convert.ToBoolean(db.ScalarQuery(sqlDurum));

            if (durum)
            {
                img.ImageUrl = "~/Kapi/images/icons/ok.png";
            }
            else
            {
                img.ImageUrl = "~/Kapi/images/icons/ok_not.png";
            }
        }

        Panel1.Visible = true;
        Panel2.Visible = false;
    }

    

    
}