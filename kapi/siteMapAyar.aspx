﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Kapi/AdminMasterPage.master" ValidateRequest="false"  AutoEventWireup="true" CodeFile="siteMapAyar.aspx.cs" Inherits="Yonetim_Yazi" %>





<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">





</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <div id="dialog"></div>
           <div class="icerikKapsar">
    
    <asp:Panel ID="Panel1" runat="server">
        <table>
                            <tr>
                <td style="padding-top: 20px;">
                    &nbsp;<asp:Button ID="Button1" runat="server" Text="Yeni" onclick="Button1_Click" 
                        CssClass="logButton2" /><span style="position:relative;right:0;left:580px"><a href="javascript:history.go(-1)" class="geriDon" title="Bir Önceki Sayfaya Geri Dön" style="font-size: small">« Geri Dön</a></span>
                        
                </td>
            </tr>
                    
 
            <tr>
                <td>
                    <div class="grid">
                        <div class="rounded">
                            <div class="top-outer">
                                <div class="top-inner">
                                    <div class="top">
                                        <h2 align="center">
                                            <asp:Label ID="lblBaslik" runat="server" Font-Names="Arial" Font-Size="11pt" 
                                                Text="Sitemap"></asp:Label>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                            <div class="mid-outer">
                                <div class="mid-inner">
                                    <div class="mid">
                                        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                                            AllowSorting="True" AutoGenerateColumns="False" CssClass="datatable" 
                                            DataKeyNames="ID" EnableModelValidation="True" GridLines="None" 
                                            onpageindexchanging="GridView1_PageIndexChanging" 
                                            onrowdeleting="GridView1_RowDeleting" 
                                            onselectedindexchanged="GridView1_SelectedIndexChanged" Width="840px" 
                                            Height="100%">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Seç">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="CheckBox1" runat="server" 
                                                            ToolTip='<%# Eval("ID") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Sil" ShowHeader="False">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" 
                                                            CommandName="Delete" ImageUrl="~/Kapi/images/icons/Eraser.png" onclick="ImageButton1_Click" 
                                                            onclientclick="return confirm(&quot;Seçilen Satır Silinsin mi?&quot;)" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField ButtonType="Image" HeaderText="Düzenle" 
                                                    SelectImageUrl="~/Kapi/images/icons//Edit.png" SelectText="Düzenle" 
                                                    ShowSelectButton="True">
                                                <ControlStyle CssClass="ortala" />
                                                <ControlStyle CssClass="ortala" />
                                                <FooterStyle HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                </asp:CommandField>
                                                <asp:BoundField DataField="SAYFA_ADI" HeaderText="Sayfa Adı" />
                                                <asp:BoundField DataField="YENILEME_PERIYOD" 
                                                    HeaderText="Güncelleştirme Periyodu" >
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ONCELIK" HeaderText="Öncelik" />
                                                <asp:TemplateField HeaderText="Onay">
                                                    <ItemTemplate>
                                                        <asp:Image ID="Image1" runat="server" />
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("KURUMSAL_DURUM") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings FirstPageText="«" LastPageText="»" />
                                            <PagerStyle CssClass="pager-row" />
                                            <RowStyle CssClass="row" HorizontalAlign="Center" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                            <div class="bottom-outer">
                                <div class="bottom-inner">
                                    <div class="bottom">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel2" runat="server">
        <table class="icerikTablo">

            <tr>
                    <td>
        <asp:Panel ID="pnlBilgi" runat="server"></asp:Panel>
        </td>
                <td align="center">
                    <asp:Label ID="Label14" runat="server" ></asp:Label><span style="position:relative;right:0;float:right"><a href="javascript:history.go(-1)" class="geriDon" title="Bir Önceki Sayfaya Geri Dön" style="font-size: small">« Geri Dön</a></span>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Label ID="lblRef" runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Sayfa Adı"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList Width="230px" ID="ddlSayfa" runat="server">
                    </asp:DropDownList>
                    <br />
                    <br />
                    
                </td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="Label3" runat="server" Text="Güncelleştirme Periyodu"></asp:Label>
                </td>
                <td >
                    <asp:DropDownList ID="DropDownList1" runat="server" Width="230px">
                        <asp:ListItem>Güncelleştirme Periyodu</asp:ListItem>
                        <asp:ListItem Value="always">Daima</asp:ListItem>
                        <asp:ListItem Value="hourly">Saatlik</asp:ListItem>
                        <asp:ListItem Value="daily">Günlük</asp:ListItem>
                        <asp:ListItem Value="weekly">Haftalık</asp:ListItem>
                        <asp:ListItem Value="monthly">Aylık</asp:ListItem>
                        <asp:ListItem Value="yearly">Yıllık</asp:ListItem>
                        <asp:ListItem Value="never">Hiçbir Zaman</asp:ListItem>
                    </asp:DropDownList>
                    
                </td>
            </tr>

            <tr>
                <td>
                    <asp:Label ID="Label15" runat="server" Text="Öncelik"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList2" runat="server" Width="230px">
                        <asp:ListItem>Öncelik Belirleyiniz.</asp:ListItem>
                        <asp:ListItem Value="0.1">0.1</asp:ListItem>
                        <asp:ListItem Value="0.2">0.2</asp:ListItem>
                        <asp:ListItem Value="0.3">0.3</asp:ListItem>
                        <asp:ListItem Value="0.4">0.4</asp:ListItem>
                        <asp:ListItem Value="0.5">0.5</asp:ListItem>
                        <asp:ListItem Value="0.6">0.6</asp:ListItem>
                        <asp:ListItem Value="0.7">0.7</asp:ListItem>
                        <asp:ListItem>0.8</asp:ListItem>
                        <asp:ListItem>0.9</asp:ListItem>
                        <asp:ListItem>1</asp:ListItem>
                    </asp:DropDownList>
                    <br />
                </td>
            </tr>

            <tr>
                <td >
                    <asp:Label ID="Label13" runat="server" Text="Onay"></asp:Label>
                </td>
                <td>
                    <asp:CheckBox ID="CheckBox2" runat="server" />
                </td>
            </tr>

            <tr>
                
                <td colspan="2" align="left">
                    <asp:Button ID="Button3" runat="server" CssClass="logButton" 
                        onclick="Button3_Click" Text="Kaydet" ValidationGroup="kaydet" />
                    <asp:Button ID="Button4" runat="server" CssClass="logButton" 
                        onclick="Button4_Click" Text="Vazgeç" />
                    <asp:Button ID="Button5" runat="server" CssClass="logButton" 
                        onclick="Button5_Click" Text="Sil"/>
                </td>
            </tr>

        </table>
             
    </asp:Panel>
    </div>
</asp:Content>

