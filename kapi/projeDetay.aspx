﻿<%@ Page Title="" Language="C#" MasterPageFile="~/kapi/AdminMasterPage.master" AutoEventWireup="true" CodeFile="projeDetay.aspx.cs" Inherits="kapi_projeGiris" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    

    <script type="text/javascript">


        $(document).ready(function () {
            var map;
            var index2 = 0;
            var marker;
            var pinSecilen = 0;
            var pinIndex = 0;
            var pinler = new Array();
            var sil = new Array();
            var markers = {};
            var infowindow;
            var pin = 0;
            var pinSonuc = 0;
            var sonPinID = 0;
            //var newIcon = MapIconMaker.createMarkerIcon({ width: 32, height: 32, primaryColor: "#00ff00" });
            function rtrim(str) {
                for (var j = str.length - 1; j >= 0 && isWhitespace(str.charAt(j)); j--);
                return str.substring(0, j + 1);
            }

            $("#gotoPin").click(function () {

                var secilen = $("#pinNo").val();
                               
                
                for (var i = 0; i < pinler.length; i++) {

                    if (typeof pinler[i] != 'undefined') {
                        if (pinler[i].get('title').toString()==secilen) {
                            
                            map.setZoom(17);
                            map.panTo(pinler[i].position);
                        }
                    }

                }

            });

            function getUrlVars() {
                var vars = [], hash;
                var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                for (var i = 0; i < hashes.length; i++) {
                    hash = hashes[i].split('=');
                    vars.push(hash[0]);
                    vars[hash[0]] = hash[1];
                }
                return vars;
            }

            var refnoK = getUrlVars()["refno"];

            if (refnoK.indexOf('#') != -1) {

                refnoK = refnoK.substring(0, refnoK.indexOf('#'));

            }

            document.getElementById("<%=fileToXls.ClientID %>").disabled = true;

            document.getElementById("<%=fileToUpload.ClientID %>").disabled = true;

            function initialize() {
                pinSecilen = pin;
                var mapOptions = {
                    center: new google.maps.LatLng(38.75408327579141, 36.38671875),
                    zoom: 6,
                    zoomControl: true,
                    zoomControlOptions: {
                        style: google.maps.ZoomControlStyle.SMALL
                    },
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    streetViewControl: false,
                    disableDoubleClickZoom: true
                }; // map create

                map = new google.maps.Map(document.getElementById("map-canvas"),
            mapOptions);

                var getMarkerUniqueId = function () {

                    var sayi = "";

                    for (var i = 0; i < 15; i++) {
                        sayi += Math.floor(Math.random() * 10);
                    }

                    return sayi;
                }

                var getLatLng = function (lat, lng) {

                    return new google.maps.LatLng(lat, lng);
                }

                Array.prototype.remove = function () {
                    var what, a = arguments, L = a.length, ax;
                    while (L && this.length) {
                        what = a[--L];
                        while ((ax = this.indexOf(what)) !== -1) {
                            this.splice(ax, 1);
                        }
                    }
                    return this;
                };



                var removeMarker = function (marker, markerid) {
                    var marker = markers[markerid];
                    marker.setMap(null);
                    var i = pinler.indexOf(marker);
                    if (i != -1) {
                        pinler.splice(i, 1);
                    }
                }

                function placeMarker(latitude, longtitude, map) {
                    pinIndex = index2;
                    if (!KontrolEt(pinler)) {
                        return;
                    }
                    var lat = latitude;
                    var lng = longtitude;

                    var markedID = getMarkerUniqueId();
                    pin = markedID;
                    sonPinID = markedID;


                    var marker = new google.maps.Marker({
                        position: getLatLng(lat, lng),
                        map: map,
                        title: "" + (parseInt(pinIndex) + 1) + "",
                        zoom: map.getZoom(),
                        id: "marker_" + markedID,
                        rec: false,
                        draggable: true,
                        animation: google.maps.Animation.DROP
                    });


                    markers[markedID] = marker;

                    pinIndex++;
                    pinler[index2] = marker;

                    index2 = pinIndex;
                    google.maps.event.addListener(marker, "rightclick", function () {

                        var marker = markers[markedID];

                        removeMarker(marker, markedID);

                        bosalt();

                        $.ajax({
                            type: "POST",
                            traditional: true,
                            url: "projeDetay.aspx/pinSil",
                            data: "{pin_id:'" + markedID + "'}",
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            beforeSend: function () {
                                UyariX("Lütfen Bekleyiniz...");
                            },
                            complete: function () {
                                UyariX("İşlem Tamam");
                            },
                            success: function (msg) {

                            }
                        }); //ajax  
                    });
                    pin = markedID;
                    google.maps.event.addListener(marker, 'dragend', function (event) {

                        $("#<%=txtEnlem.ClientID%>").val(event.latLng.lat());

                        $("#<%=txtBoylam.ClientID%>").val(event.latLng.lng());

                        $("#<%=txtZoom.ClientID%>").val(map.getZoom());
                        pinSecilen = marker.get('id');
                    }); // sürükleyip bıraktığın yerin koordinatını alır.

                    google.maps.event.addListener(marker, 'click', function (event) {

                        $("#<%=txtEnlem.ClientID%>").val(event.latLng.lat());

                        $("#<%=txtBoylam.ClientID%>").val(event.latLng.lng());

                        $("#<%=txtZoom.ClientID%>").val(map.getZoom());
                        pinSec();
                        pinSecilen = pin;
                        $("#pin").text(pin);
                        $("#pinNo").val(marker.get("title"))
                        document.getElementById("<%=fileToUpload.ClientID %>").disabled = false;
                        document.getElementById("<%=fileToXls.ClientID %>").disabled = false;

                    }); // click

                    $("#<%=txtEnlem.ClientID%>").val(lat);

                    $("#<%=txtBoylam.ClientID%>").val(lng);

                    $("#<%=txtZoom.ClientID%>").val(map.getZoom());

                    $("#pin").text(markedID);

                    document.getElementById("<%=fileToXls.ClientID %>").disabled = false;
                    document.getElementById("<%=fileToUpload.ClientID %>").disabled = false;
                    var latLng = new google.maps.LatLng(lat, lng);
                    map.panTo(latLng);
                    return marker;

                }

                

                var addMarker = google.maps.event.addListener(map, "dblclick", function (e) {
                    
                    pinIndex = index2;
                    if (!KontrolEt(pinler)) {
                        return;
                    }
                    bosalt();
                    var lat = e.latLng.lat();
                    var lng = e.latLng.lng();

                    var markedID = getMarkerUniqueId();
                    pin = markedID;
                    

                    pinIndex = parseInt(pinIndex);

                    var marker = new google.maps.Marker({
                        position: getLatLng(lat, lng),
                        map: map,
                        title: "" + (pinIndex+1) + "",
                        zoom: map.getZoom(),
                        id: "marker_" + markedID,
                        rec: false,
                        draggable: true,
                        animation: google.maps.Animation.DROP
                    });
                    $("#pinNo").val(pinIndex+1);
                    sonPinID = markedID;
                    markers[markedID] = marker;
                    
                    pinIndex++;
                    pinler[index2] = marker;

                    index2 = pinIndex;
                    google.maps.event.addListener(marker, "rightclick", function () {
                        sil[(index2 - 1)] = marker;
                        var marker = markers[markedID];

                        removeMarker(marker, markedID);
                        bosalt();

                        if (pinIndex == 1) {
                            pinIndex = 0;
                        }
                        else {
                            pinIndex--;
                        }

                        $.ajax({
                            type: "POST",
                            traditional: true,
                            url: "projeDetay.aspx/pinSil",
                            data: "{pin_id:'" + markedID + "'}",
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (msg) {

                            }
                        }); //ajax  

                    });

                    google.maps.event.addListener(marker, 'dragend', function (event) {

                        pin = markedID;

                        $("#<%=txtEnlem.ClientID%>").val(event.latLng.lat());

                        $("#<%=txtBoylam.ClientID%>").val(event.latLng.lng());

                        $("#<%=txtZoom.ClientID%>").val(map.getZoom());

                        pinSecilen = marker.get('id');

                    }); // sürükleyip bıraktığın yerin koordinatını alır.

                    google.maps.event.addListener(marker, 'click', function (event) {

                        pin = markedID;
                        pinSec();
                        $("#<%=txtEnlem.ClientID%>").val(event.latLng.lat());

                        $("#<%=txtBoylam.ClientID%>").val(event.latLng.lng());

                        $("#<%=txtZoom.ClientID%>").val(map.getZoom());
                        $("#pinNo").val(marker.get("title"))
                        document.getElementById("<%=fileToUpload.ClientID %>").disabled = false;
                        document.getElementById("<%=fileToXls.ClientID %>").disabled = false;

                        pinSecilen = pin;
                        $("#pin").text(pin);
                    }); // click

                    $("#<%=txtEnlem.ClientID%>").val(e.latLng.lat());

                    $("#<%=txtBoylam.ClientID%>").val(e.latLng.lng());

                    $("#<%=txtZoom.ClientID%>").val(map.getZoom());
                    $("#pin").text(markedID);

                    document.getElementById("<%=fileToXls.ClientID %>").disabled = false;
                    document.getElementById("<%=fileToUpload.ClientID %>").disabled = false;
                    var latLng = new google.maps.LatLng(lat, lng);
                    map.panTo(latLng);
                    return marker;
                });

                pinOlustur(refnoK);
                google.maps.event.addListener(map, 'mousemove', function (event) {

                    $("#koordinat").text(event.latLng);

                    // use pixel.x, pixel.y ... (after some rounding)
                }); // mouse move

                var homeControlDiv = document.createElement('span');
                var homeControl = new HomeControl(homeControlDiv, map);

                homeControlDiv.index = 1;
                map.controls[google.maps.ControlPosition.TOP_CENTER].push(homeControlDiv);


                $("#btnGetCoor").click(function () {


                    if (!KontrolEt(pinler)) {
                        return;
                    }

                    var boylamGir = $("#<%=txtEnlemGir.ClientID%>").val();

                    var enlemgir = $("#<%=txtBoylamGir.ClientID%>").val();

                    var zoomGir = $("#<%=txtZoomGir.ClientID%>").val();

                    var pos = new google.maps.LatLng(enlemgir, boylamGir, zoomGir);

                    placeMarker(enlemgir, boylamGir, map);

                    bosalt();

                    $("#<%=txtEnlem.ClientID%>").val(enlemgir);

                    $("#<%=txtBoylam.ClientID%>").val(boylamGir);

                    $("#<%=txtZoom.ClientID%>").val(zoomGir);

                }); // koordinata git


                $("#gotoBtn").click(function () {

                    if (!KontrolEt(pinler)) {

                        return;

                    }

                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(function (position) {

                            var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

                            placeMarker(position.coords.latitude, position.coords.longitude, map);



                            $("#<%=txtEnlem.ClientID%>").val(position.coords.latitude);

                            $("#<%=txtBoylam.ClientID%>").val(position.coords.longitude);

                            $("#<%=txtZoom.ClientID%>").val(map.getZoom());


                        }, function () {
                            handleNoGeolocation(true);
                        });
                    } else {
                        // Browser doesn't support Geolocation
                        handleNoGeolocation(false);
                    }

                    function handleNoGeolocation(errorFlag) {
                        if (errorFlag) {
                            var content = 'Error: Coğrafi lokasyon servisi çalışmıyor. ';
                            window.alert(content);
                        } else {
                            var content = 'Error: Kullandığınız browser coğrafi lokasyon bulma servisini desteklenmiyor.';
                            window.alert(content);
                        }


                    }

                    /*
                    var bounds = new google.maps.LatLngBounds(southWest, northEast);
                    map.fitBounds(bounds);
                
                    */

                });  //gotoBtn
            }
            var removeMarker = function (marker, markerid) {
                var marker = markers[markerid];
                marker.setMap(null);
                var i = pinler.indexOf(marker);
                if (i != -1) {
                    pinler.splice(i, 1);
                }
            }
            function markerCreate(lat, lng, zoom, pin_id, index, desc) {

                var styleIconClass = new StyledIcon(StyledIconTypes.CLASS, { color: "#6f3" });

                var marker = new StyledMarker({
                    styleIcon: new StyledIcon(StyledIconTypes.BUBBLE, { text: index }, styleIconClass),
                    position: new google.maps.LatLng(lat, lng),
                    id: "marker_" + pin_id,
                    description: "" + desc + "",
                    draggable: true,
                    title:index,
                    rec: true,
                    animation: google.maps.Animation.DROP,
                    map: map
                });

                markers[pin_id] = marker;
                pinler[index] = marker;

                index2 = index;
                google.maps.event.addListener(marker, "rightclick", function () {

                    var marker = markers[pin_id];

                    removeMarker(marker, pin_id);

                    $.ajax({
                        type: "POST",
                        traditional: true,
                        url: "projeDetay.aspx/pinSil",
                        data: "{pin_id:'" + pin_id + "'}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (msg) {

                        }
                    }); //ajax 

                    bosalt();
                });

                google.maps.event.addListener(marker, 'dragend', function (event) {

                    $("#<%=txtEnlem.ClientID%>").val(event.latLng.lat());

                    $("#<%=txtBoylam.ClientID%>").val(event.latLng.lng());

                    $("#<%=txtZoom.ClientID%>").val(map.getZoom());

                    $("#pinNo").val(marker.get('title'));

                    pinSecilen = marker.get('id');

                    //var ayir = new Array();

                    //ayir = marker.get('id').toString().split('_');

                    //pin = ayir[1];
                    //pinSec();

                }); // sürükleyip bıraktığın yerin koordinatını alır.

                google.maps.event.addListener(marker, 'click', function (event) {

                    $("#<%=txtEnlem.ClientID%>").val(event.latLng.lat());

                    $("#<%=txtBoylam.ClientID%>").val(event.latLng.lng());

                    $("#<%=txtZoom.ClientID%>").val(map.getZoom());

                    $("#<%=txtZoom.ClientID%>").val(map.getZoom());
                    $("#pinNo").val(marker.get("title"))
                    pin = pin_id;
                    pinSec();
                    pinSecilen = pin;
                    $("#pin").text(pin);
                    document.getElementById("<%=fileToUpload.ClientID %>").disabled = false;
                    document.getElementById("<%=fileToXls.ClientID %>").disabled = false;

                    $("#<%=TextBox1.ClientID%>").val(marker.get('description'));



                }); // click

                marker.setMap(map);
                pinSecilen = pin_id;
                AutoCenter();
            }

            function pinOlustur(refno) {

                $.ajax({
                    type: "POST",
                    traditional: true,
                    url: "projeDetay.aspx/pinOlustur",
                    data: "{proje_id:" + refno + "}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {

                        var sonuc = msg.d;
                        if (sonuc == "") {
                            return false;
                        }

                        var gelenAna = new Array();
                        gelenAna = sonuc.split('|');


                        for (var i = 0; i < gelenAna.length; i++) {

                            var gelenChild = gelenAna[i].split('*');

                            markerCreate(gelenChild[0].toString().replace(",", "."), gelenChild[1].toString().replace(",", "."), gelenChild[2], gelenChild[3], gelenChild[4], gelenChild[5]);
                        }
                    }
                }); //ajax  

            }

            function pinOlusturScalar(pin_identifier) {

                $.ajax({
                    type: "POST",
                    traditional: true,
                    url: "projeDetay.aspx/pinOlusturScalar",
                    data: "{pin_ids:'" + pin_identifier + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {

                        var sonuc = msg.d;
                        if (sonuc == "") {
                            return false;
                        }

                        var gelenAna = new Array();
                        gelenAna = sonuc.split('|');


                        for (var i = 0; i < gelenAna.length; i++) {

                            var gelenChild = gelenAna[i].split('*');

                            markerCreate(gelenChild[0].toString().replace(",", "."), gelenChild[1].toString().replace(",", "."), gelenChild[2], gelenChild[3], gelenChild[4], gelenChild[5]);
                        }
                    }
                }); //ajax  

            }


            function pinSec() {

                $.ajax({
                    type: "POST",
                    traditional: true,
                    url: "projeDetay.aspx/pinSec",
                    data: "{pin_id:'" + pin + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {

                        var sonuc = msg.d;
                                
                        $("#uploadedDivXls table>tbody").empty();
                        $("#uploadedDiv table>tbody").empty();
                        
                        $("#<%=TextBox1.ClientID%>").val(sonuc.aciklama);
                        if (sonuc.xls != 'undefined') {
                            if (sonuc.xls.length > 1) {
                                
                                ShowUploadedFiles2(sonuc.xls);
                            }
                        }
                        
                        for (var i = 0; i < sonuc.photo.length; i++) {

                            
                            ShowUploadedFiles(sonuc.photo[i].toString());

                            //if (gelenChild[i].toString().length > 1) {
                                
                            //}
                            //else {
                            //    xlsFilesize = 0;
                            //}

                        }

                        $("#<%=txtNem.ClientID%>").val(sonuc.nem);

                        $("#<%=txtSicaklik.ClientID%>").val(sonuc.sicaklik);

                        $("#<%=txtRuzgar.ClientID%>").val(sonuc.ruzgar);

                        $("#<%=txtRuzgarYonu.ClientID%>").val(sonuc.ruzgar_yonu);

                        $("#<%=txtMikrofonYuksekligi.ClientID%>").val(sonuc.myukseklik);

                        $("#<%=txtKaynagaMesafe.ClientID%>").val(sonuc.kmesafe);

                        if (sonuc.lokasyon != "") {
                            $("#<%=ddlLokasyon.ClientID%> option:contains(" + sonuc.lokasyon + ")").attr("selected", "selected");
                        } else {
                            $("#<%=ddlLokasyon.ClientID%> option:contains(-- Seçiniz --)").attr("selected", "selected");
                        }

                        if (sonuc.standart != "") {
                            $("#<%=ddlStandart.ClientID%> option:contains(" + sonuc.standart + ")").attr("selected", "selected");
                        } else {

                            $("#<%=ddlStandart.ClientID%> option:contains(-- Seçiniz --)").attr("selected", "selected");
                        }

                        

                        


                    }
                }); //ajax  
            }

            google.maps.event.addDomListener(window, 'load', initialize);
            // geolocation begin
            function bosalt() {

                $("#<%=txtNem.ClientID%>").val("");

                $("#<%=txtSicaklik.ClientID%>").val("");

                $("#<%=txtRuzgar.ClientID%>").val("");

                $("#<%=txtRuzgarYonu.ClientID%>").val("");

                $("#<%=txtMikrofonYuksekligi.ClientID%>").val("");

                $("#<%=txtKaynagaMesafe.ClientID%>").val("");

                $("#<%=ddlLokasyon.ClientID%> option:contains(-- Seçiniz --)").attr("selected", "selected");

                $("#<%=ddlStandart.ClientID%> option:contains(-- Seçiniz --)").attr("selected", "selected");
                
                $("#<%=txtEnlemGir.ClientID%>").val("");

                $("#<%=txtBoylamGir.ClientID%>").val("");

                $("#<%=txtZoomGir.ClientID%>").val("");

                $("#<%=txtEnlem.ClientID%>").val("");

                $("#<%=txtBoylam.ClientID%>").val("");

                $("#<%=txtZoom.ClientID%>").val("");
                $("#pinNo").val("");
                $("#uploadedDivXls table>tbody").empty();
                $("#uploadedDiv table>tbody").empty();
                $("#<%=TextBox1.ClientID%>").val("");
                $("#hdnCountFiles").val(0);

                document.getElementById("<%=fileToUpload.ClientID %>").disabled = true;
                document.getElementById("<%=fileToXls.ClientID %>").disabled = true;
            }

            function KontrolEt(pins) {

                var donus = true;

                 

                for (var i = 0; i < pins.length; i++) {

                    if (typeof pins[i]!='undefined') {
                        if (!pins[i].get('rec')) {

                            UyariX("Öncelikle ekrandaki pin'i kayıt etmelisiniz !");

                            donus = false;
                        }
                    }

                }

                return donus;

            }

            function AutoCenter() {
                //  Create a new viewpoint bound
                var bounds = new google.maps.LatLngBounds();
                //  Go through each...
                $.each(markers, function (index, marker) {
                    bounds.extend(marker.position);
                });
                //  Fit these bounds to the map
                map.fitBounds(bounds);
            }

            function HomeControl(controlDiv, map) {

                // Set CSS styles for the DIV containing the control
                // Setting padding to 5 px will offset the control
                // from the edge of the map.
                controlDiv.style.padding = '5px';

                // Set CSS for the control border.
                var controlUI = document.createElement('span');
                //controlUI.style.backgroundColor = 'white';

                controlUI.setAttribute("id", "koordinat");

                controlDiv.appendChild(controlUI);

                // geolocation end
            }


            function toggleBounce() {

                if (marker.getAnimation() != null) {
                    marker.setAnimation(null);
                } else {
                    marker.setAnimation(google.maps.Animation.BOUNCE);
                }
            } //toogleBounc

            function clearOverlays() {
                if (pinler) {
                    for (i in pinler) {
                        pinler[i].setMap(null);
                    }
                }
            }

            $("#btnKaydet").click(function () {


                var nem = $("#<%=txtNem.ClientID%>").val();

                var sicaklik = $("#<%=txtSicaklik.ClientID%>").val();

                var ruzgar = $("#<%=txtRuzgar.ClientID%>").val();

                var ruzgarYonu = $("#<%=txtRuzgarYonu.ClientID%>").val();

                var myukseklik = $("#<%=txtMikrofonYuksekligi.ClientID%>").val();

                var kmesafe = $("#<%=txtKaynagaMesafe.ClientID%>").val();

                var lokasyon = $("#<%=ddlLokasyon.ClientID%>").val();

                if (lokasyon == 0) {
                    lokasyon = "";
                } else {

                    lokasyon = $("#<%=ddlLokasyon.ClientID%> option:selected").text();
                }
                
                var standart = $("#<%=ddlStandart.ClientID%>").val();

                if (standart == 0) {
                    standart = "";
                } else {

                    standart = $("#<%=ddlStandart.ClientID%> option:selected").text();
                }

                var enlem = $("#<%=txtEnlem.ClientID%>").val();

                var boylam = $("#<%=txtBoylam.ClientID%>").val();

                var zoom = $("#<%=txtZoom.ClientID%>").val();

                var sira = 0;

                if ($("#pinNo").val()=="") {
                    sira = markers[sonPinID].get('title');
                }
                else {
                    sira = $("#pinNo").val();
                }


                var aciklama = $("#<%=TextBox1.ClientID%>").val();

                var xlsFile;

                if ($("#lblfileXls_1").attr("title")!='undefined') {

                    xlsFile = $("#lblfileXls_1").attr("title");
                }
                else {
                    xlsFile = null;
                }

                
                                
                if (enlem==""||boylam=="") {

                    UyariX("Kayıt işlemi öncesinden herhangi bir pin seçilmelidir ya da yeni pin oluşturulmalıdır.");
                    return false;
                }

                if (sonPinID != 0) {
                    markers[sonPinID].set('rec', true);
                }

                if ($("#pin").text() == 0) {

                    UyariX("Kayıt işlemi için projede en az bir adet pin bulunmalıdır.");

                    return;
                }


                var fotolar = "";

                var fotoAdet = parseInt($("#hdnCountFiles").val());

                for (var i = 0; i < fotoAdet; i++) {

                    if ((fotoAdet - 1) > i) {
                        fotolar += $("#lblfilePhoto_" + (i + 1) + "").attr("title") + ",";
                        //alert(fotolar);
                    }
                    else {
                        fotolar += $("#lblfilePhoto_" + (i + 1) + "").attr("title");
                        //alert(fotolar);
                    }

                }

                $.ajax({
                    type: "POST",
                    traditional: true,
                    url: "projeDetay.aspx/Gonder",
                    data: "{enlem:'" + enlem + "',boylam:'" + boylam + "',zoom:'" + zoom + "',pin_id:'" + pin + "',xlsFile:'" + xlsFile + "',aciklama:'" + aciklama + "',fotolar:'" + fotolar + "',sira:" + sira + ",lokasyon:'" + lokasyon + "',standart:'" + standart + "',kmesafe:'" + kmesafe + "',myukseklik:'" + myukseklik + "',sicaklik:'" + sicaklik + "',nem:'" + nem+ "',ruzgar:'" + ruzgar+ "',ruzgaryonu:'" + ruzgarYonu+ "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    beforeSend: function () {
                        UyariModel("Lütfen Bekleyiniz...", "info", "OK");
                    },
                    complete: function () {

                    },
                    success: function (msg) {

                        var sonuc = msg.d;

                        switch (msg.d) {
                            case "ok":
                                Uyari("Kayıt İşlemi Başarıyla Tamamlandı", "info", "OK");

                                xlsFilesize = 0;
                                bosalt();
                                                                
                                break;
                            case "siraMuk":
                                Uyari("Bu sıra numarası daha önce kullanılmış.Lütfen farklı bir numara veriniz.", "error", "Hata");
                                
                                break;
                            case "upd":
                                Uyari("Güncelleme işlemi başarıyla gerçekleşti", "info", "OK");

                                xlsFilesize = 0;
                                bosalt();

                                break;
                            default:
                                UyariX(msg.d);
                                break;
                        }
                    }
                }); //ajax  
                removeMarker(marker, pin);
                pinOlusturScalar(pin);
              //  pinOlustur(refno);
            });//btnkaydet

            //$("#pdfDownload").click(function () {

            //    $.ajax({
            //        type: "POST",
            //        url: "down.ashx",
            //        dataType: "json",
            //        cache: false,
            //        async: true,
            //        contentType: "application/json; charset=utf-8",
            //        beforeSend: function () {
            //            //UyariModel("Lütfen Bekleyiniz...", "info", "OK");
            //        },
            //        complete: function () {

            //        },
            //        success: function (msg) {

                        
            //        }
            //    }); //ajax  

            //});

        });                                                                                                                                                                  // ready
    </script>
    <script src="js/AjaxFileupload.js" type="text/javascript"></script>
    <script src="js/fotoUpload.js" type="text/javascript"></script>
    <style type="text/css">
      
                .sonucTable
        {
            width:100%;
            height:100%;
            border:1px solid black;
        }

        .sonucTable th
        {
            background:#999;
            border:1px solid black;
        }

            .sonucTable td
            {
                border:1px solid black;
            }

      #map-canvas { height: 500px }
    </style>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=true"></script>
    
    <script type="text/javascript">

        
    </script>
    <style type="text/css">
    .prjTbl td
    {
        padding:5px 2px 5px 2px ;
        
        }
        .prjTbl
        {

            width:100%;

        }
    
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <%--<asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" />--%>
<div class="icerikKapsar">
<table>
<tr>
<td>Proje Adı</td>
<td>
    <asp:DropDownList ID="ddlProje_adi" runat="server" Height="25px" Width="150px" 
        Enabled="False">
    </asp:DropDownList>
    </td>
<td>Müşteri Adı</td>
<td>
    <asp:DropDownList ID="ddlMusteri_adi" runat="server" Height="25px" 
        Width="150px" Enabled="False">
    </asp:DropDownList>
    </td>
    <td>
    <input type="button" id="gotoBtn" value=" Bulunduğum Yeri İşaretle " 
            style="width: 200px; height: 28px" />
    </td>
</tr>
    
</table>
<br />
<table>
<tr>
<td>Enlem Gir</td>
<td>
    <asp:TextBox ID="txtBoylamGir" runat="server"></asp:TextBox>
    </td>
<td>Boylam Gir</td>
<td>
<asp:TextBox ID="txtEnlemGir" runat="server"></asp:TextBox>
    
    </td>
    <td>Zoom Gir</td>
<td>

    <asp:TextBox ID="txtZoomGir" runat="server"></asp:TextBox>
    </td>
    <td>
    <input type="button" id="btnGetCoor" value=" Koordinata Git " 
            style="width: 100px; height: 28px" />
    </td>
</tr>

</table>
<br />
    <div id="map-canvas"/>
    
    </div>
<br />
    <span style="font-size: large">Seçilen Pin No: </span><input id="pinNo" type="text" style="font-size: large; color: #CC0000; font-weight: bolder;" />&nbsp;<input id="gotoPin" type="button" value="Git -->" style="width:60px;height:30px" />
 <table class="prjTbl">
     <tr>
         <td>Enlem</td>
         <td colspan="3">

             <asp:TextBox ID="txtEnlem" runat="server"></asp:TextBox>
             Boylam <asp:TextBox ID="txtBoylam" runat="server"></asp:TextBox>
             Zoom <asp:TextBox ID="txtZoom" runat="server"></asp:TextBox>
         </td>
         

     </tr>
     <tr>
         <td>Lokasyon</td>
         <td colspan="3">
             <asp:DropDownList ID="ddlLokasyon" runat="server">
                 <asp:ListItem Value="0">-- Seçiniz --</asp:ListItem>
                 <asp:ListItem Value="1">Konut</asp:ListItem>
                 <asp:ListItem Value="2">Tesis İçi</asp:ListItem>
                 <asp:ListItem Value="3">Tesis Çevresi</asp:ListItem>
                 <asp:ListItem Value="4">Okul</asp:ListItem>
                 <asp:ListItem Value="5">Hastane</asp:ListItem>
                 <asp:ListItem Value="6">Apartman Boşluğu</asp:ListItem>
             </asp:DropDownList>
         </td>
     </tr>
     <tr>
         <td>Standart</td>
         <td colspan="3">
             <asp:DropDownList ID="ddlStandart" runat="server">
                 <asp:ListItem Value="0">-- Seçiniz --</asp:ListItem>
                 <asp:ListItem Value="1">ISO 8297</asp:ListItem>
                 <asp:ListItem Value="2">ISO 1996-2</asp:ListItem>
             </asp:DropDownList>
         </td>
     </tr>
     <tr>
         <td>Kaynağa Mesafe</td>
         <td colspan="3">
             <asp:TextBox ID="txtKaynagaMesafe" runat="server"></asp:TextBox>
         </td>
     </tr>
     <tr>
         <td>Mikrofon Yuksekligi</td>
         <td colspan="3">
             <asp:TextBox ID="txtMikrofonYuksekligi" runat="server"></asp:TextBox>
         </td>
     </tr>
     <tr>
         <td>Sıcaklık</td>
         <td colspan="3">
             <asp:TextBox ID="txtSicaklik" runat="server"></asp:TextBox>
         </td>
     </tr>
     <tr>
         <td>Rüzgar</td>
         <td colspan="3">
             <asp:TextBox ID="txtRuzgar" runat="server"></asp:TextBox>
         </td>
     </tr>
     <tr>
         <td>Rüzgar Yönü</td>
         <td colspan="3">
             <asp:TextBox ID="txtRuzgarYonu" runat="server"></asp:TextBox>
         </td>
     </tr>
     <tr>
         <td>Nem</td>
         <td colspan="3">
             <asp:TextBox ID="txtNem" runat="server"></asp:TextBox>
         </td>
     </tr>

<tr>
    <td>Ölçüm Dosyası Seç</td>
<td colspan=2>
            
        <div id="uploadedDivXls" runat="server" style="width: 100%;  clear: both" clientidmode="Static">
            <table class="sonucTable"><thead><th style="width:80%">Dosya Adı</th><th>İşlemler</th></thead>
                <tbody>
            
                </tbody>
            </table>
        </div>
    <asp:FileUpload name="fileToXls" ID="fileToXls" runat="server" ClientIDMode="Static" /> 
        <asp:Label ID="lblDosya" runat="server" Text=""></asp:Label>
    </td>
    
    </tr>
<tr>
<td>Fotoğraf Ekle</td>
    <td colspan="2">
            
        <div id="uploadedDiv" runat="server" style="width: 100%;  clear: both" clientidmode="Static">
            <table class="sonucTable"><thead><th style="width:80%">Dosya Adı</th><th>İşlemler</th></thead>
                 <tbody>
            
                </tbody>
            </table>
        </div>
        <asp:HiddenField ID="pin" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnFileFolder" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnCountFiles" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnCountFilesXls" runat="server" Value="0" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnUploadFilePath" runat="server" ClientIDMode="Static" />
        <div >
            <div style="float: left;">
                <%--Show wrapper on above of fileUpload Control--%>
                <label class="file-upload">
                    <%--Make clientID static if you are using Master Page--%>
                    <asp:FileUpload name="fileToUpload" ID="fileToUpload" runat="server" ClientIDMode="Static" /> 
                </label>
            </div>
   
        </div>
                    </td>
</tr>
<tr>
<td>Açıklama</td>
<td colspan=4>
<asp:TextBox ID="TextBox1" runat="server" Height="100px" TextMode="MultiLine" 
        Width="100%"></asp:TextBox>
</td>
</tr>
<tr>
<td>
    
</td>
<td>
<input type="button" value="Kaydet" id="btnKaydet" width="150px" height="60px" 
        style="width: 150px; height: 30px" />
    
</td>
<td>
    <asp:Button ID="btnVazgeç" runat="server" Width="150px" Height="30px" 
        Text="Vazgeç" onclick="btnVazgeç_Click" />
</td>
</tr>
</table>   
 <div id="dialog-message"   style="display: none" align="center">
	
	<span>
    
	Lütfen bekleyiniz...
	</span>
    
</div>
 <div id="dialog" >
     <img id="loading" src="images/loading.gif" style="display: none;">

 </div>
</asp:Content>

