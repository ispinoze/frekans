﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using DbClass;
using System.Data.SqlClient;

public partial class Admin_GenelAyarlar : System.Web.UI.Page
{

    DbClassMysql db = new DbClassMysql();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["KullaniciRefNo"] != null)
            {
                string sql = "SELECT * FROM tbl_eposta_ayar";

                DataTable dt = db.Fill(sql);

                if (dt.Rows.Count != 0)
                {
                    txtEpostaAdresi.Text = dt.Rows[0]["EPOSTA_ADRES"].ToString();
                    txtSmtpPort.Text = dt.Rows[0]["SMTP_PORT"].ToString();
                    
                    txtEpostaSifre.Text = dt.Rows[0]["SIFRE"].ToString();
                    txtSmtpHost.Text = dt.Rows[0]["SMTP_HOST"].ToString();

                    
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            } 
        }

        string sqlBilgi = "SELECT * FROM tbl_bilgi where SAYFA_REFNO=" + Request["refno"].ToString() + "";

        DataTable dtBilgi = db.Fill(sqlBilgi);

        string bilgi = "";

        if (dtBilgi.Rows.Count != 0)
        {
            bilgi = dtBilgi.Rows[0]["ACIKLAMA"].ToString();

            bilgi = bilgi.Replace('\'', ' ');
        }
        else
        {
            bilgi = "Yardım içeriği bulunmamaktadır.";
        }

        pnlBilgi.Controls.Add(new LiteralControl("<a class='cep' title='" + bilgi + "' href='#'><img src='images/image/soru_32x.png' style='border:0; vertical-align:middle' alt='Soru' /></a>")); 

    }

    protected void Button1_Click(object sender, EventArgs e)
    {//Vazgeç
        Response.Redirect("Login.aspx");
    }
    protected void btnKaydet_Click(object sender, EventArgs e)
    {
        SqlParameter prm1 = new SqlParameter("@P1", txtEpostaAdresi.Text);
        SqlParameter prm2 = new SqlParameter("@P2", txtEpostaSifre.Text);
        SqlParameter prm3 = new SqlParameter("@P3", txtSmtpHost.Text);
        SqlParameter prm4 = new SqlParameter("@P4", txtSmtpPort.Text);
        

        try
        {

            int sqlSorgu = Convert.ToInt32(DbClassMysql.ScalarQuery2("SELECT COUNT(*) FROM tbl_eposta_ayar"));

            if (sqlSorgu <= 0)
            {
                string sqlIns = "INSERT INTO tbl_eposta_ayar(EPOSTA_ADRES,SIFRE,SMTP_HOST,SMTP_PORT)";

                sqlIns += " values(@P1,@P2,@P3,@P4)";

                db.ExecuteNonQuery(sqlIns, prm1, prm2, prm3, prm4);
               
               Page.ClientScript.RegisterStartupScript(this.GetType(), "Mesaj", "Uyari3('Kayıt İşlemi Gerçekleştirildi.', 'info', 'Bilgi');", true);
            }
            else
            {
                string sqlIns = "UPDATE tbl_eposta_ayar SET EPOSTA_ADRES=@P1,SIFRE=@P2,SMTP_HOST=@P3,SMTP_PORT=@P4";

                db.ExecuteNonQuery(sqlIns, prm1, prm2, prm3, prm4);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Mesaj", "Uyari3('Kayıt İşlemi Gerçekleştirildi.', 'info', 'Bilgi');", true);
                
            }
            
            
        }
        catch (Exception)
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Mesaj", "Uyari('Kayıt İşlemi Gerçekleştirilemedi.', 'info', 'Bilgi');", true);
            
        }

        //images/icons/
    }
}