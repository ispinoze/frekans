﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="css/Default.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.8.0.min.js" type="text/javascript"></script>
    <script src="js/jquery.watermark.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <link href="css/blitzer/jquery-ui-1.8.23.custom.css" rel="stylesheet" type="text/css" />

    <title>::: Frekans Akustik :::</title>
    <script src="js/General.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {



            $("#KullaniciAdi").watermark("Kullanıcı Adınızı Giriniz.");
            $("#txtRemember").watermark("Kullanıcı Adınızı ya da email adresinizi giriniz.");

            $("#Pass").watermark("Şifrenizi Giriniz.");


            $(".sorgula").fadeOut(100);


            $("#sifreUnut").click(function () {

                $(".icerik").fadeOut(100);
                $(".sorgula").fadeIn(100);

            });

            $("#girisEkran").click(function () {

                $(".sorgula").fadeOut(100);
                $(".icerik").fadeIn(100);

            });

            $("#btnGiris").click(function () {

                UyariModel("Lütfen Bekleyiniz...", "info", "Bilgi");

                $.ajax({
                    type: "POST",
                    url: "Default.aspx/Giris",
                    data: "{kullaniciAdi:'" + $("#KullaniciAdi").val() + "',pass:'" + $("#Pass").val() + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {


                        var donus = msg.d;

                        if (donus != "") {

                            KapatDialog();


                            var parcala = donus.split("|");

                            switch (parcala[1].toString()) {

                                case "1":
                                    UyariModel("Giriş Başarılı.Yönetim Paneline Yönlendiriliyorsunuz...", "info", "Bilgi");
                                    window.location = "Login.aspx";
                                    break;
                                case "2":
                                    UyariModel("Giriş Başarılı.Hoşgeldiniz "+parcala[2].toString()+" Harita sayfasına yönlendiriliyorsunuz...", "info", "Bilgi");
                                    window.location = "../Default.aspx";
                                    break;
                                case "3":
                                    UyariModel("Giriş Başarılı.Yönetim Paneline Yönlendiriliyorsunuz...", "info", "Bilgi");
                                    window.location = "Login.aspx";
                                    break;
                                default:

                            }

                            

                        }
                        else {

                            KapatDialog();

                            Uyari("Hatalı Kullanıcı Adı ya da Şifre Girişi", "alert", "Uyari");

                        }

                    }
                }); //ajax



            }); //btnKaydet

            $("#btnSorgula").click(function () {

                UyariModel("Lütfen Bekleyiniz...", "info", "Bilgi");

                $.ajax({
                    type: "POST",
                    url: "Default.aspx/Sorgula",
                    data: "{kullaniciAdi:'" + $("#txtRemember").val() + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {


                        var donus = Boolean(msg.d);

                        if (donus) {

                            KapatDialog();

                            Uyari("Kayıtlı e-posta adresinize kullanıcı adınız ve şifreniz gönderilmiştir.", "info", "Bilgi");


                        }
                        else {

                            KapatDialog();

                            UyariModel("Belirttiğiniz kullanıcı adı veya email adresine ait bir kayıt bulunamamıştır.", "alert", "Uyari");

                        }

                    }
                }); //ajax



            }); //btnSorgula



        });            //ready
    </script>
        
</head>
<body>
    <form id="form1" runat="server">
    <div id="main">
       <div id="dialog"></div>
<div id="logo">
<img src="images/icons/logo.png" />
</div>
<div id="baslik" 
            style="font-family: Arial, Helvetica, sans-serif; font-size: x-large; font-weight: bolder; line-height: 25px; color: #63c430">
Ölçüm Sonuçları Görüntüleme Uygulaması 
    <br style="font-family: Arial, Helvetica, sans-serif; font-size: x-large; font-weight: bold" />
Kullanıcı Girişi
<%--<img src="images/image/admin.png" />--%>
</div>
<div id="content">
    <div class="icerik">
    <input type="text" id="KullaniciAdi" /><br />
    <input type="password" id="Pass" /><br />
    <input type="button"  id="btnGiris" value="GİRİŞ"/><br />
    <a href="#" id="sifreUnut">Şifremi Unuttum</a>
    </div>
    <div class="sorgula">
    <input type="text" id="txtRemember" title="Kullanıcı adınızı ya da email adresinizi giriniz." /><br />
    <input type="button" value="SORGULA" id="btnSorgula"/><br />
    
    <a href="#" id="girisEkran">Giriş Ekranına Dön</a>
    </div>
</div>
<div id="fotter">
<a href="http://www.frekanscevre.com/" >AnaSayfa Geri Dön</a>
</div>

    </div>
    </form>
</body>
</html>
