﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DbClass;
using System.Web.Services;
using System.IO;
using System.Data.SqlClient;
using System.Collections;
using System.Data.OleDb;
using OfficeOpenXml;

public partial class kapi_projeGiris : System.Web.UI.Page
{

    DbClassMysql db = new DbClassMysql();

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request["refno"]!=null)
        {
            Session["refno"]=Request["refno"].ToString();

            Session.Add("refno",Request["refno"]);
            string sqlProjeler = "select  * from tbl_proje where id=" + Request["refno"].ToString() + " order by tarih desc";
                      
            DataTable dtPrj = db.Fill(sqlProjeler);

            ddlProje_adi.DataSource = dtPrj;
            ddlProje_adi.DataTextField = "proje_adi";
            ddlProje_adi.DataValueField = "id";
            ddlProje_adi.DataBind();
            //ddlProje_adi.Items.Insert(0, new ListItem(String.Empty, String.Empty));
            //ddlProje_adi.SelectedIndex = 0;
            string sqlFirma = "select * from tbl_kullanici where id="+dtPrj.Rows[0]["firma_id"].ToString();
            DataTable dtfirma = db.Fill(sqlFirma);
            ddlMusteri_adi.DataSource = dtfirma;
            ddlMusteri_adi.DataTextField = "musteri_adi";
            ddlMusteri_adi.DataValueField = "id";
            ddlMusteri_adi.DataBind();
            //ddlMusteri_adi.Items.Insert(0, new ListItem(String.Empty, String.Empty));
            //ddlMusteri_adi.SelectedIndex = 0;

            Session["prjFrm"] = dtPrj.Rows[0]["id"].ToString();

             fileToUpload.Attributes.Add("onchange", "javascript:return ajaxFileUpload();");

             fileToXls.Attributes.Add("onchange", "javascript:return ajaxFileUpload2();");
        }
    }

    [WebMethod]
    public static string Gonder(string enlem, string boylam, string zoom, string pin_id, string xlsFile, string aciklama,string fotolar,int sira,string kmesafe,string myukseklik,string lokasyon,string standart,string sicaklik,string nem,string ruzgar,string ruzgaryonu)
    {//
        string sonuc = "";

        try
        {
            SqlParameter prm15=new SqlParameter("@P15","");

            if (HttpContext.Current.Session["KullaniciRefno"]!=null)
	            {
		            prm15 = new SqlParameter("@P15", HttpContext.Current.Session["KullaniciRefno"]);
	            }
            else
	            {
	                HttpContext.Current.Response.Redirect("Default.aspx");
	            }

            if (HttpContext.Current.Session["prjFrm"]==null)
	            {
		            sonuc="fatal";
	            }
            SqlParameter prm1 = new SqlParameter("@P1", enlem);
            SqlParameter prm2 = new SqlParameter("@P2", boylam);
            SqlParameter prm3 = new SqlParameter("@P3", zoom);
            SqlParameter prm4 = new SqlParameter("@P4", xlsFile);
            SqlParameter prm5 = new SqlParameter("@P5", HttpContext.Current.Session["prjFrm"].ToString());
            SqlParameter prm6 = new SqlParameter("@P6", aciklama);
            SqlParameter prm7 = new SqlParameter("@P7", pin_id);
            SqlParameter prm99 = new SqlParameter("@P99", sira);
            SqlParameter prm101 = new SqlParameter("@P101", "");
            SqlParameter prm102 = new SqlParameter("@P102", "");
            // utmWrite(enlem, boylam);
           /* 
            DataSet utm = utmRead();

            for (int i = 0; i < utm.Tables[0].Rows.Count; i++)
            {
                DataTable dt=utm.Tables[0];

                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    string x = dt.Rows[j][0].ToString() + dt.Rows[j][1].ToString();
                    
                }
            }
            */

            DataTable siraMuk = DbClassMysql.Fill2("select sira,pin_id from tbl_projedetay where proje_id=" + HttpContext.Current.Session["prjFrm"].ToString()+"and sira="+sira+"");

            if (siraMuk.Rows.Count > 0)
            {

                if (pin_id!=siraMuk.Rows[0]["pin_id"].ToString())
                {
                    sonuc = "siraMuk";
                    
                    return sonuc;
                }
            }

            int dublicate = Convert.ToInt32(DbClassMysql.ScalarQuery2("select id from tbl_projedetay where pin_id=" + pin_id));
            
            string sqlIns;

            int proje_id=0;

            if (dublicate==0)
            {

                sqlIns = "INSERT INTO tbl_projedetay(enlem,boylam,zoom,xls_dosya,proje_id,aciklama,pin_id,olcen_id,sira)";
                sqlIns += " VALUES(@P1,@P2,@P3,@P4,@P5,@P6,@P7,@P15,@P99)";

                proje_id = DbClassMysql.ExecuteNonQuery2(sqlIns, prm1, prm2, prm3, prm4, prm5, prm6, prm7, prm15, prm99);
            }
            else
            {
                sqlIns = "update tbl_projedetay set enlem=@P1,boylam=@P2,zoom=@P3,xls_dosya=@P4,proje_id=@P5,aciklama=@P6,olcen_id=@P15,sira=@P99";
                sqlIns += " where pin_id=@P7";

                DbClassMysql.ExecuteNonQuery2(sqlIns, prm1, prm2, prm3, prm4, prm5, prm6, prm7, prm15, prm99);

                proje_id = dublicate;
            }

            

            if (fotolar!="")
            {
                string[] photos = fotolar.Split(',');

                for (int i = 0; i < photos.Length; i++)
                {
                    if (photos[i].ToString() != "")
                    {
                        int dubFoto = Convert.ToInt32(DbClassMysql.ScalarQuery2("select id from tbl_foto where fotograf='" + photos[i].ToString()+"'"));

                        SqlParameter prm8 = new SqlParameter("@P8", photos[i].ToString());

                        if (dubFoto==0)
                        {
                            string sqlInsFoto;
                            if (proje_id==0)
                            {
                                proje_id = dublicate;
                            }
                            sqlInsFoto = "INSERT INTO tbl_foto(proje_detay_id,fotograf)";
                            sqlInsFoto += " VALUES(" + proje_id + ",@P8)";

                            DbClassMysql.ExecuteNonQuery2(sqlInsFoto, prm8); 
                        }

                    }

                } 
            }

            /*if (xlsFile == "undefined")
            {
                sonuc = "ok";
                return sonuc;
            }*/

            if (xlsFile != "" && xlsFile != "undefined")
            {
                if (proje_id == 0)
                {
                    proje_id = dublicate;
                }

                int dubXls = Convert.ToInt32(DbClassMysql.ScalarQuery2("select id from tbl_header where proje_detay_id='" + dublicate + "'"));

                if (dubXls == 0)
                {
                    DataSet ds = getirExcelTablo(xlsFile);

                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        DataTable dt = ds.Tables[i];

                        string tableName = ds.Tables[i].TableName;
                        SqlParameter prm9 = new SqlParameter("@P9", " ");
                        SqlParameter prm10 = new SqlParameter("@P10", " ");
                        SqlParameter prm11 = new SqlParameter("@P11", " ");
                        SqlParameter prm12 = new SqlParameter("@P12", " ");
                        SqlParameter prm13 = new SqlParameter("@P13", " ");
                        SqlParameter prm14 = new SqlParameter("@P14", " ");
                        SqlParameter prm16 = new SqlParameter("@P16", " ");
                        SqlParameter prm17 = new SqlParameter("@P17", proje_id);
                        SqlParameter prm18 = new SqlParameter("@P18", " ");
                        SqlParameter prm19 = new SqlParameter("@P19", " ");
                        SqlParameter prm20 = new SqlParameter("@P20", " ");
                        SqlParameter prm21 = new SqlParameter("@P21", " ");
                        SqlParameter prm22 = new SqlParameter("@P22", " ");

                        SqlParameter prm32 = new SqlParameter("@P32", dt.Rows[39][3].ToString().Replace(',', '.'));
                        SqlParameter prm33 = new SqlParameter("@P33", dt.Rows[39][6].ToString().Replace(',', '.'));
                        SqlParameter prm34 = new SqlParameter("@P34", dt.Rows[39][9].ToString().Replace(',', '.'));
                        SqlParameter prm35 = new SqlParameter("@P35", dt.Rows[17][2]);
                        SqlParameter prm36 = new SqlParameter("@P36", dt.Rows[17][4]);
                        SqlParameter prm37 = new SqlParameter("@P37", dt.Rows[17][7]);
                        SqlParameter prm38 = new SqlParameter("@P38", dt.Rows[17][9]);
                        SqlParameter prm39 = new SqlParameter("@P39", dt.Rows[21][3].ToString());
                        

                        switch (tableName)
                        {
                            case "Form":

                                prm9 = new SqlParameter("@P9", dt.Rows[21][8].ToString()); //Device type

                                prm10 = new SqlParameter("@P10", dt.Rows[26][3].ToString()); //Original file name

                                prm14 = new SqlParameter("@P14", dt.Rows[20][8].ToString()); // factor

                                prm16 = new SqlParameter("@P16", dt.Rows[20][3].ToString()); // seri no

                                SqlParameter prm40 = new SqlParameter("@P40", dt.Rows[25][3].ToString());

                                string sqlHeader;

                                sqlHeader = "insert into tbl_header(proje_detay_id,calibration_factor,device_type,original_file_name,";
                                sqlHeader += "serial_no,lg,la,lge,h_hiz,h_arac,a_hiz,a_arac,tip,slm_no) values(@P17,@P14,@P9,@P10,@P16,@P32,@P33,@P34,@P35,@P36,@P37,@P38,@P39,@P40) ";

                                DbClassMysql.ExecuteNonQuery2(sqlHeader, prm17, prm14, prm9, prm10, prm16, prm32, prm33, prm34, prm35, prm36, prm37, prm38, prm39, prm40);

                                string sqlSonucA = "insert into tbl_sonuc(proje_detay_id,day,hour,filter,elapsed_time,max,";
                                sqlSonucA += "min,leq,sel) values(" + proje_id + ",'" + dt.Rows[24][8].ToString() + "','" + Convert.ToDateTime(dt.Rows[28][3]).ToShortTimeString() + "','A','" + dt.Rows[30][3].ToString() + "'," + dt.Rows[34][3].ToString().Replace(',', '.') + "," + dt.Rows[35][3].ToString().Replace(',', '.') + "," + dt.Rows[33][3].ToString().Replace(',', '.') + "," + dt.Rows[36][8].ToString().Replace(',', '.') + ")";

                                DbClassMysql.ExecuteNonQuery2(sqlSonucA);

                                string sqlSonucC = "insert into tbl_sonuc(proje_detay_id,day,hour,filter,elapsed_time,max,";
                                sqlSonucC += "min,leq,sel) values(" + proje_id + ",'" + dt.Rows[24][8].ToString() + "','" + Convert.ToDateTime(dt.Rows[28][3]).ToShortTimeString() + "','C','" + dt.Rows[30][3].ToString() + "'," + dt.Rows[37][3].ToString().Replace(',', '.') + "," + dt.Rows[38][3].ToString().Replace(',', '.') + "," + dt.Rows[36][3].ToString().Replace(',', '.') + "," + dt.Rows[37][8].ToString().Replace(',', '.') + ")";

                                DbClassMysql.ExecuteNonQuery2(sqlSonucC);

                                string sqlSonucLin = "insert into tbl_sonuc(proje_detay_id,day,hour,filter,elapsed_time,max,";
                                sqlSonucLin += "min,leq,sel) values(" + proje_id + ",'" + dt.Rows[24][8].ToString() + "','" + Convert.ToDateTime(dt.Rows[28][3]).ToShortTimeString() + "','Lin','" + dt.Rows[30][3].ToString() + "'," + dt.Rows[34][3].ToString().Replace(',', '.') + "," + dt.Rows[35][3].ToString().Replace(',', '.') + "," + dt.Rows[35][8].ToString().Replace(',', '.') + "," + dt.Rows[38][8].ToString().Replace(',', '.') + ")";

                                DbClassMysql.ExecuteNonQuery2(sqlSonucLin);

                                string sqlLn = "insert into tbl_ln(proje_detay_id,ln_10,ln_90";
                                sqlLn += ") values(" + proje_id + ",'" + dt.Rows[33][8].ToString().Replace(',', '.') + "','" + dt.Rows[34][8].ToString().Replace(',', '.') + "')";
                                DbClassMysql.ExecuteNonQuery2(sqlLn);

                                string sqlHava = "insert into tbl_hava(proje_detay_id,temp,nem,ruzgar,ruzgar_yonu,yukseklik,k_mesafe)";
                                sqlHava += " values(" + proje_id + ",@P20,@P21,@P22,@P23,@P24,@P25,@P101,@P102)";

                                /*prm20 = new SqlParameter("@P20", dt.Rows[28][8].ToString());//temp
                                prm21 = new SqlParameter("@P21", dt.Rows[27][8].ToString());//nem
                                prm22 = new SqlParameter("@P22", dt.Rows[30][8].ToString());//rüzgar
                                SqlParameter prm23 = new SqlParameter("@P23", dt.Rows[29][8].ToString());//rüzgar yöün
                                SqlParameter prm24 = new SqlParameter("@P24",dt.Rows[26][8]);//yukseklik
                                SqlParameter prm25 = new SqlParameter("@P25", dt.Rows[25][8]);//k_mesafe*/

                                prm20 = new SqlParameter("@P20", sicaklik);
                                prm21 = new SqlParameter("@P21", nem);
                                prm22 = new SqlParameter("@P22", ruzgar);
                                SqlParameter prm23 = new SqlParameter("@P23", ruzgaryonu);//rüzgar yöün
                                SqlParameter prm24 = new SqlParameter("@P24", myukseklik);//yukseklik
                                SqlParameter prm25 = new SqlParameter("@P25", kmesafe);//k_mesafe
                                prm101 = new SqlParameter("@P101", lokasyon);
                                prm102 = new SqlParameter("@P102", standart);

                                DbClassMysql.ExecuteNonQuery2(sqlHava, prm20, prm21, prm22, prm23, prm24, prm25, prm101, prm102);

                                break;
                            default:
                                break;
                        }

                    }
                    sonuc = "ok";
                }
                else
                {
                    DataSet ds = getirExcelTablo(xlsFile);

                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        DataTable dt = ds.Tables[i];

                        string tableName = ds.Tables[i].TableName;
                        SqlParameter prm9 = new SqlParameter("@P9", " ");
                        SqlParameter prm10 = new SqlParameter("@P10", " ");
                        SqlParameter prm11 = new SqlParameter("@P11", " ");
                        SqlParameter prm12 = new SqlParameter("@P12", " ");
                        SqlParameter prm13 = new SqlParameter("@P13", " ");
                        SqlParameter prm14 = new SqlParameter("@P14", " ");
                        SqlParameter prm16 = new SqlParameter("@P16", " ");
                        SqlParameter prm17 = new SqlParameter("@P17", proje_id);
                        SqlParameter prm18 = new SqlParameter("@P18", " ");
                        SqlParameter prm19 = new SqlParameter("@P19", " ");
                        SqlParameter prm20 = new SqlParameter("@P20", " ");
                        SqlParameter prm21 = new SqlParameter("@P21", " ");
                        SqlParameter prm22 = new SqlParameter("@P22", " ");

                        SqlParameter prm32 = new SqlParameter("@P32", dt.Rows[39][3].ToString().Replace(',', '.'));
                        SqlParameter prm33 = new SqlParameter("@P33", dt.Rows[39][6].ToString().Replace(',', '.'));
                        SqlParameter prm34 = new SqlParameter("@P34", dt.Rows[39][9].ToString().Replace(',', '.'));
                        SqlParameter prm35 = new SqlParameter("@P35", dt.Rows[17][2]);
                        SqlParameter prm36 = new SqlParameter("@P36", dt.Rows[17][4]);
                        SqlParameter prm37 = new SqlParameter("@P37", dt.Rows[17][7]);
                        SqlParameter prm38 = new SqlParameter("@P38", dt.Rows[17][9]);
                        SqlParameter prm39 = new SqlParameter("@P39", dt.Rows[21][3].ToString());
                        SqlParameter prm40 = new SqlParameter("@P40", dt.Rows[25][3].ToString());

                        prm9 = new SqlParameter("@P9", dt.Rows[21][8].ToString()); //Device type

                        prm10 = new SqlParameter("@P10", dt.Rows[26][3].ToString()); //Original file name

                        prm14 = new SqlParameter("@P14", dt.Rows[20][8].ToString()); // factor

                        prm16 = new SqlParameter("@P16", dt.Rows[20][3].ToString()); // seri no

                        string sqlHeader;

                        sqlHeader = "update tbl_header set proje_detay_id=@P17,calibration_factor=@P14,device_type=@P9,original_file_name=@P10,";
                        sqlHeader += "serial_no=@P16,lg=@P32,la=@P33,lge=@P34,h_hiz=@P35,h_arac=@P36,a_hiz=@P37,a_arac=@P38,tip=@P39,slm_no=@P40 where proje_detay_id=" + dublicate;

                        DbClassMysql.ExecuteNonQuery2(sqlHeader, prm17, prm14, prm9, prm10, prm16, prm32, prm33, prm34, prm35, prm36, prm37, prm38, prm39, prm40);

                        string sqlSonucA = "update tbl_sonuc set proje_detay_id=" + proje_id + ",day='" + dt.Rows[24][8].ToString() + "',hour='" + Convert.ToDateTime(dt.Rows[28][3]).ToShortTimeString() + "',elapsed_time='" + dt.Rows[30][3].ToString() + "',max=" + dt.Rows[34][3].ToString().Replace(',', '.') + ",";
                        sqlSonucA += "min=" + dt.Rows[35][3].ToString().Replace(',', '.') + ",leq=" + dt.Rows[33][3].ToString().Replace(',', '.') + ",sel=" + dt.Rows[36][8].ToString().Replace(',', '.') + " where proje_detay_id=" + dublicate + " and filter='A'";

                        DbClassMysql.ExecuteNonQuery2(sqlSonucA);

                        string sqlSonucC = "update tbl_sonuc set proje_detay_id=" + proje_id + ",day='" + dt.Rows[24][8].ToString() + "',hour='" + Convert.ToDateTime(dt.Rows[28][3]).ToShortTimeString() + "',filter='C',elapsed_time='" + dt.Rows[30][3].ToString() + "',max=" + dt.Rows[37][3].ToString().Replace(',', '.') + ",";
                        sqlSonucC += "min=" + dt.Rows[38][3].ToString().Replace(',', '.') + ",leq=" + dt.Rows[36][3].ToString().Replace(',', '.') + ",sel=" + dt.Rows[37][8].ToString().Replace(',', '.') + " where proje_detay_id=" + dublicate + " and filter='C'"; ;

                        DbClassMysql.ExecuteNonQuery2(sqlSonucC);

                        string sqlSonucLin = "update tbl_sonuc set proje_detay_id=" + proje_id + ",day='" + dt.Rows[24][8].ToString() + "',hour='" + Convert.ToDateTime(dt.Rows[28][3]).ToShortTimeString() + "',filter='Lin',elapsed_time='" + dt.Rows[30][3].ToString() + "',max=" + dt.Rows[34][3].ToString().Replace(',', '.') + ",";
                        sqlSonucLin += "min=" + dt.Rows[35][3].ToString().Replace(',', '.') + ",leq=" + dt.Rows[35][8].ToString().Replace(',', '.') + ",sel=" + dt.Rows[38][8].ToString().Replace(',', '.') + " where proje_detay_id=" + dublicate + " and filter='Lin'";

                        //string sqlSonucLin = "insert into tbl_sonuc(proje_detay_id,day,hour,filter,elapsed_time,max,";
                        //sqlSonucLin += "min,leq,sel) values(" + proje_id + ",'" + dt.Rows[24][8].ToString() + "','" + Convert.ToDateTime(dt.Rows[28][3]).ToShortTimeString() + "','Lin','" + dt.Rows[30][3].ToString() + "'," + dt.Rows[34][3].ToString().Replace(',', '.') + "," + dt.Rows[35][3].ToString().Replace(',', '.') + "," + dt.Rows[35][8].ToString().Replace(',', '.') + "," + dt.Rows[38][8].ToString().Replace(',', '.') + ")";

                        DbClassMysql.ExecuteNonQuery2(sqlSonucLin);

                        string sqlLn = "update tbl_ln set ln_10='" + dt.Rows[33][8].ToString().Replace(',', '.') + "',ln_90='" + dt.Rows[34][8].ToString().Replace(',', '.') + "'";
                        sqlLn += " where proje_detay_id=" + dublicate;
                        DbClassMysql.ExecuteNonQuery2(sqlLn);

                        string sqlHava = "update tbl_hava set temp=@P20,nem=@P21,ruzgar=@P22,ruzgar_yonu=@P23,yukseklik=@P24,k_mesafe=@P25,lokasyon=@P101,standart=@P102";
                        sqlHava += " where proje_detay_id=" + dublicate;

                        /*prm20 = new SqlParameter("@P20", dt.Rows[28][8].ToString());
                        prm21 = new SqlParameter("@P21", dt.Rows[30][8].ToString());
                        prm22 = new SqlParameter("@P22", dt.Rows[27][8].ToString());

                        SqlParameter prm23 = new SqlParameter("@P23", dt.Rows[29][8].ToString());//rüzgar yöün
                        SqlParameter prm24 = new SqlParameter("@P24",dt.Rows[26][8]);//yukseklik
                        SqlParameter prm25 = new SqlParameter("@P25", dt.Rows[25][8]);//k_mesafe*/

                        prm20 = new SqlParameter("@P20", sicaklik);
                        prm21 = new SqlParameter("@P21", nem);
                        prm22 = new SqlParameter("@P22", ruzgar);
                        SqlParameter prm23 = new SqlParameter("@P23", ruzgaryonu);//rüzgar yöün
                        SqlParameter prm24 = new SqlParameter("@P24", myukseklik);//yukseklik
                        SqlParameter prm25 = new SqlParameter("@P25", kmesafe);//k_mesafe
                        prm101 = new SqlParameter("@P101", lokasyon);
                        prm102 = new SqlParameter("@P102", standart);

                        DbClassMysql.ExecuteNonQuery2(sqlHava, prm20, prm21, prm22, prm23, prm24, prm25, prm101, prm102);

                    }

                    sonuc = "upd";
                }
            }
            else { 

                if (proje_id == 0)
                {
                    proje_id = dublicate;
                }

                int dubXls = Convert.ToInt32(DbClassMysql.ScalarQuery2("select id from tbl_hava where proje_detay_id='" + dublicate + "'"));

                if (dubXls == 0)
                {

                    string sqlHava = "insert into tbl_hava(proje_detay_id,temp,nem,ruzgar,ruzgar_yonu,yukseklik,k_mesafe,lokasyon,standart)";
                    sqlHava += " values(" + proje_id + ",@P20,@P21,@P22,@P23,@P24,@P25,@P101,@P102)";

                    /*prm20 = new SqlParameter("@P20", dt.Rows[28][8].ToString());//temp
                    prm21 = new SqlParameter("@P21", dt.Rows[27][8].ToString());//nem
                    prm22 = new SqlParameter("@P22", dt.Rows[30][8].ToString());//rüzgar
                    SqlParameter prm23 = new SqlParameter("@P23", dt.Rows[29][8].ToString());//rüzgar yöün
                    SqlParameter prm24 = new SqlParameter("@P24",dt.Rows[26][8]);//yukseklik
                    SqlParameter prm25 = new SqlParameter("@P25", dt.Rows[25][8]);//k_mesafe*/

                    SqlParameter prm20 = new SqlParameter("@P20", sicaklik);
                    SqlParameter prm21 = new SqlParameter("@P21", nem);
                    SqlParameter prm22 = new SqlParameter("@P22", ruzgar);
                    SqlParameter prm23 = new SqlParameter("@P23", ruzgaryonu);//rüzgar yöün
                    SqlParameter prm24 = new SqlParameter("@P24", myukseklik);//yukseklik
                    SqlParameter prm25 = new SqlParameter("@P25", kmesafe);//k_mesafe
                    prm101 = new SqlParameter("@P101", lokasyon);
                    prm102 = new SqlParameter("@P102", standart);

                    DbClassMysql.ExecuteNonQuery2(sqlHava, prm20, prm21, prm22, prm23, prm24, prm25, prm101, prm102);
                    
                    sonuc = "ok";
                }
                else {

                    string sqlHava = "update tbl_hava set temp=@P20,nem=@P21,ruzgar=@P22,ruzgar_yonu=@P23,yukseklik=@P24,k_mesafe=@P25,lokasyon=@P101,standart=@P102";
                    sqlHava += " where proje_detay_id=" + dublicate;

                    SqlParameter prm20 = new SqlParameter("@P20", sicaklik);
                    SqlParameter prm21 = new SqlParameter("@P21", nem);
                    SqlParameter prm22 = new SqlParameter("@P22", ruzgar);
                    SqlParameter prm23 = new SqlParameter("@P23", ruzgaryonu);//rüzgar yöün
                    SqlParameter prm24 = new SqlParameter("@P24", myukseklik);//yukseklik
                    SqlParameter prm25 = new SqlParameter("@P25", kmesafe);//k_mesafe
                    prm101 = new SqlParameter("@P101", lokasyon);
                    prm102 = new SqlParameter("@P102", standart);

                    DbClassMysql.ExecuteNonQuery2(sqlHava, prm20, prm21, prm22, prm23, prm24, prm25, prm101, prm102);

                    sonuc = "upd";
                }

            }
            
        }
        catch (Exception ex)
        {

            sonuc = ex.Message;

            

            //Response.Redirect("Hata.aspx");

        }

        return sonuc;

    }

    private static DateTime ParseDate(string s)
    {
        DateTime result;
        if (!DateTime.TryParse(s, out result))
        {
            result = DateTime.ParseExact(s, "HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            result = result.AddDays(1);
        }
        return result;
    }

    static DataSet getirExcelTablo(string dosyaAdi)
    {
        DataSet ds = new DataSet();
        
        string dosya_adres = "xlsFiles\\" + dosyaAdi + "";
        dosya_adres = HttpContext.Current.Server.MapPath(dosya_adres);
        OleDbConnection baglanti;
        
        if ( dosyaAdi.Substring(dosyaAdi.IndexOf('.'),(dosyaAdi.Length-dosyaAdi.IndexOf('.'))).Length==4 )
        {
            baglanti = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + dosya_adres + ";Extended Properties=Excel 8.0"); 
        }
        else
        {
            //baglanti = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + dosya_adres + ";Extended Properties=Excel 8.0;IMEX=1;HDR=YES"); 

            baglanti = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + dosya_adres + ";Extended Properties=Excel 8.0;"); 
        }

        baglanti.Open();

        //string queryHeader = "select * from [Header$] ";
        //OleDbDataAdapter oAdpHeader = new OleDbDataAdapter(queryHeader, baglanti);

        //string querySonuc = "select * from [Sonuc$] ";
        //OleDbDataAdapter oAdpSonuc = new OleDbDataAdapter(querySonuc, baglanti);

        //string queryFrekans = "select * from [Frekans$] ";
        //OleDbDataAdapter oAdpFrekans = new OleDbDataAdapter(queryFrekans, baglanti);

        //string queryLn = "select * from [Ln$] ";
        //OleDbDataAdapter oAdpLn = new OleDbDataAdapter(queryLn, baglanti);

        //string queryBuffer = "select * from [Buffer$] ";
        //OleDbDataAdapter oAdpBuffer = new OleDbDataAdapter(queryBuffer, baglanti);

        //string queryHava = "select * from [Hava$]";
        //OleDbDataAdapter oAdpHava = new OleDbDataAdapter(queryHava, baglanti);

        string queryForm = "select * from [Form$]";
        OleDbDataAdapter oAdpForm = new OleDbDataAdapter(queryForm, baglanti);

        //DataTable dtHeader=new DataTable();
        //DataTable dtSonuc=new DataTable();
        //DataTable dtFrekans=new DataTable();
        //DataTable dtLn=new DataTable();
        //DataTable dtBuffer = new DataTable();
        //DataTable dtHava = new DataTable();

        DataTable dtForm = new DataTable();

        oAdpForm.Fill(dtForm);
        ds.Tables.Add(dtForm);
        //oAdpHeader.Fill(dtHeader);
        //oAdpSonuc.Fill(dtSonuc);
        //oAdpFrekans.Fill(dtFrekans);
        //oAdpLn.Fill(dtLn);
        //oAdpBuffer.Fill(dtBuffer);
        //oAdpHava.Fill(dtHava);

        //ds.Tables.Add(dtHeader);
        //ds.Tables.Add(dtSonuc);
        //ds.Tables.Add(dtFrekans);
        //ds.Tables.Add(dtLn);
        //ds.Tables.Add(dtBuffer);
        //ds.Tables.Add(dtHava);
        //ds.Tables[0].TableName = "Header";
        //ds.Tables[1].TableName = "Sonuc";
        //ds.Tables[2].TableName = "Frekans";
        //ds.Tables[3].TableName = "Ln";
        //ds.Tables[4].TableName = "Buffer";
        //ds.Tables[5].TableName = "Hava";

        ds.Tables[0].TableName = "Form";
        return ds;

    }

    [WebMethod]
    public static string pinOlustur(int proje_id)
    {
        string parameterler = "";

        string projeDetay = "select * from tbl_projedetay where proje_id="+proje_id;

        DataTable dtDetay = DbClassMysql.Fill2(projeDetay);
        
        string enlem, boylam, zoom, pin_id,aciklama,sira;
        
        for (int i = 0; i < dtDetay.Rows.Count; i++)
        {
            enlem = dtDetay.Rows[i]["enlem"].ToString();

            boylam = dtDetay.Rows[i]["boylam"].ToString();

            zoom = dtDetay.Rows[i]["zoom"].ToString();

            pin_id = dtDetay.Rows[i]["pin_id"].ToString();

            sira = dtDetay.Rows[i]["sira"].ToString();

            aciklama = dtDetay.Rows[i]["aciklama"].ToString();

            if (dtDetay.Rows.Count-1>i)
            {
                parameterler+=enlem+"*"+boylam+"*"+zoom+"*"+pin_id+"*"+sira+"*"+aciklama+"|";
            }
            else
            {
                parameterler += enlem + "*" + boylam + "*" + zoom + "*" + pin_id + "*" + sira + "*" + aciklama;
            }
        }

        return parameterler;
    }

    [WebMethod]
    public static string pinOlusturScalar(string pin_ids)
    {
        string parameterler = "";

        string projeDetay = "select * from tbl_projedetay where pin_id=" + pin_ids;

        DataTable dtDetay = DbClassMysql.Fill2(projeDetay);

        string enlem, boylam, zoom, pin_id, aciklama, sira;

        for (int i = 0; i < dtDetay.Rows.Count; i++)
        {
            enlem = dtDetay.Rows[i]["enlem"].ToString();

            boylam = dtDetay.Rows[i]["boylam"].ToString();

            zoom = dtDetay.Rows[i]["zoom"].ToString();

            pin_id = dtDetay.Rows[i]["pin_id"].ToString();

            sira = dtDetay.Rows[i]["sira"].ToString();

            aciklama = dtDetay.Rows[i]["aciklama"].ToString();


           parameterler = enlem + "*" + boylam + "*" + zoom + "*" + pin_id + "*" + sira + "*" + aciklama;
            
        }

        return parameterler;
    }
    
    [WebMethod]
    public static void pinSilXls(string xls_file)
    {
        int getProjeDetayId = Convert.ToInt32(DbClassMysql.ScalarQuery2("select id from tbl_projedetay where xls_dosya='"+xls_file+"'"));

        string sqlXlsBlank = "update tbl_projedetay set xls_dosya=' ' where  id=" + getProjeDetayId + "";
        DbClassMysql.ExecuteNonQuery2(sqlXlsBlank);
        DbClassMysql.ExecuteNonQuery2("delete from tbl_header where proje_detay_id=" + getProjeDetayId + "");

        DbClassMysql.ExecuteNonQuery2("delete from tbl_sonuc where proje_detay_id=" + getProjeDetayId + "");

        DbClassMysql.ExecuteNonQuery2("delete from tbl_ln where proje_detay_id=" + getProjeDetayId + "");

        DbClassMysql.ExecuteNonQuery2("delete from tbl_buffer where proje_detay_id=" + getProjeDetayId + "");

        DbClassMysql.ExecuteNonQuery2("delete from tbl_frekans where proje_detay_id=" + getProjeDetayId + "");

        DbClassMysql.ExecuteNonQuery2("delete from tbl_hava where proje_detay_id=" + getProjeDetayId + "");

        //string Serverpath = "xlsFiles\\";

        
        //Serverpath = HttpContext.Current.Server.MapPath(Serverpath) + xls_file;

        //if (File.Exists(Serverpath))
        //{
        //    File.Delete(Serverpath);
        //}

    }

    //[WebMethod]
    //public static void pinSaveXls(string xls_file)
    //{
        
    //    string Serverpath = "xlsFiles\\";


    //    Serverpath = HttpContext.Current.Server.MapPath(Serverpath) + xls_file;


    //    HttpContext.Current.Response.ContentType = "image/jpeg";

    //    // Set the filename 
    //    HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + xls_file);

    //    // Stream the file to the client 
    //    HttpContext.Current.Response.WriteFile(Serverpath);
        
        
    //    /*byte[] bts = System.IO.File.ReadAllBytes(Serverpath);

    //    HttpContext.Current.Response.AddHeader("Content-Type", "Application/octet-stream");
    //    HttpContext.Current.Response.AddHeader("Content-Length", bts.Length.ToString());
    //    HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + xls_file);
    //    HttpContext.Current.Response.BinaryWrite(bts);
    //    HttpContext.Current.Response.Flush();
    //    //HttpContext.Current.Response.WriteFile(Serverpath);
    //    HttpContext.Current.Response.End();
    //   */
    //    //if (File.Exists(Serverpath))
    //    //{
    //    //    File.Delete(Serverpath);
    //    //}

    //}

    [WebMethod]
    public static void pinSilPhoto(string photo_file)
    {

        SqlParameter prm1 = new SqlParameter("@P1",photo_file);

        DbClassMysql.ExecuteNonQuery2("delete from tbl_foto where fotograf=@P1", prm1);

    }
    
    [WebMethod]
    public static void Down(string dosya)
    {

        
    }

    [WebMethod]
    public static void pinSil(string pin_id)
    {
        string xlsNames = Convert.ToString(DbClassMysql.ScalarQuery2("select xls_dosya from tbl_projedetay where pin_id="+pin_id));

        string photoNames = "select * from tbl_foto where proje_detay_id in(select id from tbl_projedetay where pin_id="+pin_id+")";

        DataTable dt = DbClassMysql.Fill2(photoNames);

        DbClassMysql.ExecuteNonQuery2("delete from tbl_projedetay where pin_id="+pin_id+"");

        string Serverpath1 = "xlsFiles\\";


        Serverpath1 = HttpContext.Current.Server.MapPath(Serverpath1) + xlsNames;

        if (File.Exists(Serverpath1))
        {
            File.Delete(Serverpath1);
        }

        string Serverpath2 = @"UploadFiles\";

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (File.Exists(HttpContext.Current.Server.MapPath(Serverpath2) + dt.Rows[i]["fotograf"].ToString()))
                {
                    File.Delete(HttpContext.Current.Server.MapPath(Serverpath2) + dt.Rows[i]["fotograf"].ToString());
                }
            }
        }
    }


    [WebMethod]
    public static object pinSec(string pin_id)
    {
        string sqlProje_id = "select * from tbl_projedetay where pin_id="+pin_id;

        DataTable dt = DbClassMysql.Fill2(sqlProje_id);

        DataTable dtPrjHava;

        ArrayList foto = new ArrayList();

        ArrayList hava = new ArrayList();

        if (dt.Rows.Count>0)
        {
            int projeID = Convert.ToInt32(dt.Rows[0]["id"]);

            string sqlHava = "select * from tbl_hava where proje_detay_id=" + projeID;

            string sqlFoto = "select * from tbl_foto where proje_detay_id=" + projeID;

            dtPrjHava = DbClassMysql.Fill2(sqlHava);

            DataTable dtPrj = DbClassMysql.Fill2(sqlFoto); 

            for (int i = 0; i < dtPrj.Rows.Count; i++)
            {
                foto.Add(dtPrj.Rows[i]["fotograf"]);
            }

        }
        else
        {
            return new
            {
                xls = "",
                aciklama = "",
                sicaklik = "",
                nem = "",
                ruzgar = "",
                ruzgar_yonu = "",
                myukseklik = "",
                kmesafe = "",
                lokasyon = "",
                standart = "",
                photo = ""
            };
        }

        if (dtPrjHava.Rows.Count > 0)
        {
            return new
            {
                xls = dt.Rows[0]["xls_dosya"],
                aciklama = dt.Rows[0]["aciklama"],
                sicaklik = dtPrjHava.Rows[0]["temp"],
                nem = dtPrjHava.Rows[0]["nem"],
                ruzgar = dtPrjHava.Rows[0]["ruzgar"],
                ruzgar_yonu = dtPrjHava.Rows[0]["ruzgar_yonu"],
                myukseklik = dtPrjHava.Rows[0]["yukseklik"],
                kmesafe = dtPrjHava.Rows[0]["k_mesafe"],
                lokasyon = dtPrjHava.Rows[0]["lokasyon"],
                standart = dtPrjHava.Rows[0]["standart"],
                photo = foto
            };
        }
        else {

            return new
            {
                xls = dt.Rows[0]["xls_dosya"],
                aciklama = dt.Rows[0]["aciklama"],
                sicaklik = "",
                nem = "",
                ruzgar = "",
                ruzgar_yonu = "",
                myukseklik = "",
                kmesafe = "",
                lokasyon = "",
                standart = "",
                photo = foto
            };
        
        }

        

        //ses = xlsFiles + "|" + foto+"|"+aciklama;
        //return ses;
    }

    [WebMethod]
    public static string GetSession(string session)
    {
        string ses = "";

        if (HttpContext.Current.Session["refno"] != null)
        {
            ses = HttpContext.Current.Session["refno"].ToString();
        }

        return ses;
    }
    [WebMethod]
    public static void Sil(string fotoAdi)
    {
        string Serverpath = System.Configuration.ConfigurationManager.AppSettings["FolderPath"];

        if (fotoAdi != "")
        {
            try
            {
                string[] fotoSlice = fotoAdi.Split('|');



                for (int i = 0; i < fotoSlice.Length; i++)
                {
                    FileInfo dosya = new FileInfo(HttpContext.Current.Server.MapPath(Serverpath) + fotoSlice[i].ToString() + ".jpg");

                    if (dosya.Exists)
                    {
                        File.Delete(HttpContext.Current.Server.MapPath(Serverpath) + fotoSlice[i].ToString());
                    }
                }
            }
            catch (Exception ex)
            {


            }
        }
    }

    protected void btnVazgeç_Click(object sender, EventArgs e)
    {
        Response.Redirect("Login.aspx");
    }
/*    [WebMethod]
  /*  public static void utmWrite(string enlem,string boylam)
    {
        string dosya_adres = HttpContext.Current.Server.MapPath("hesap\\") + @"hesap.xlsm";


        ExcelPackage pck; 
        using (var fs = new FileStream(dosya_adres, FileMode.Open))
        {
            pck = new ExcelPackage(fs);

            var ws = pck.Workbook.Worksheets[1];
            enlem = enlem.Replace('.', ',');
            boylam = boylam.Replace('.', ',');
            ws.Cells[2, 1].Value = enlem;
            ws.Cells[2, 2].Value = boylam;
            ws.Cells[18, 1].Value = enlem;
            ws.Cells[18, 2].Value = boylam;


        }
    
    }*/

    
    public static DataSet utmRead()
    {
        string dosya_adres = HttpContext.Current.Server.MapPath("hesap\\") + @"hesap.xls";

        DataSet ds = new DataSet();

        OleDbConnection baglanti;

        /*Excel.ApplicationClass oExcel;
        

        Excel.Workbooks oBooks;
        
        oExcel = new Excel.ApplicationClass();

        oExcel.Visible = true;

        oBooks = oExcel.Workbooks;

        oBooks.Open(dosya_adres);

        //oExcel.Run("test");
        oBooks.Close();

        oExcel.Quit();
        */
                
        
        baglanti = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + dosya_adres + ";Extended Properties=Excel 8.0");
        

        baglanti.Open();

        string queryHeader = "select * from [hesap$] ";
        OleDbDataAdapter oAdpHeader = new OleDbDataAdapter(queryHeader, baglanti);

        

        DataTable dtHeader = new DataTable();
        

        oAdpHeader.Fill(dtHeader);
        

        ds.Tables.Add(dtHeader);
        
        
        
        return ds;

    }

    //protected void Button1_Click(object sender, EventArgs e)
    //{

    //    string sqlProjeler = "select id from tbl_proje";

    //    DataTable projeler = db.Fill(sqlProjeler);

    //    for (int i = 0; i < projeler.Rows.Count; i++)
    //    {
    //        string sql = "select * from tbl_projedetay where proje_id="+projeler.Rows[i]["id"]+" order by tarih asc";

    //        DataTable dt = db.Fill(sql);

    //        for (int k = 0; k < dt.Rows.Count; k++)
    //        {

    //            string upd = "update tbl_projedetay set sira="+(k+1)+" where id="+dt.Rows[k]["id"];

    //           db.ExecuteNonQuery(upd);

    //        } 
    //    }
    //}
}
        