﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using DbClass;

public partial class Login : System.Web.UI.Page
{

    DbClassMysql db = new DbClassMysql();

    protected void Page_Load(object sender, EventArgs e)
    {
        string sqlKategori = "SELECT * FROM tbl_admin_menu where ACIKLAMA=''";

        DataTable dtKat = db.Fill(sqlKategori);
        

        for (int i = 0; i < dtKat.Rows.Count; i++)
        {

            string sqlDetay = "SELECT * FROM tbl_admin_menu WHERE ANA_DUZEY=" + dtKat.Rows[i]["ANA_DUZEY"].ToString() + " AND ALT_DUZEY_1<>0 AND ALT_DUZEY_2=0 ORDER BY ALT_DUZEY_1 ASC,ALT_DUZEY_2 ASC";

            DataTable dtDetay = db.Fill(sqlDetay);

            string sinif = "";


            if (i==0)
            {
                sinif = "logKapsar";    
            }
            else
            {
                sinif = "logKapsar2";
            }

            panelLogin.Controls.Add(new LiteralControl("<div class='" + sinif + "'><div class='logBaslik'>" + dtKat.Rows[i]["KATEGORI_ADI"].ToString() + "</div><div class='logIcerik'>"));

            for (int j = 0; j < dtDetay.Rows.Count; j++)
            {
                panelLogin.Controls.Add(new LiteralControl("<a href='" + dtDetay.Rows[j]["ACIKLAMA"].ToString() + "?refno=" + dtDetay.Rows[j]["ID"].ToString() + "'><input type='button' class='logButton' value='" + dtDetay.Rows[j]["KATEGORI_ADI"].ToString() + "' /></a>"));
            }

            panelLogin.Controls.Add(new LiteralControl("</div></div>"));

        }
    }

}