﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DbClass;
using System.Web.UI.HtmlControls;
using System.Data;
using DbClass;


public partial class Admin_MasterPageX : System.Web.UI.MasterPage
{

    DbClassMysql db = new DbClassMysql();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["yetki"] != null)
        {

        
        if (Session["yetki"].ToString() == "2")
        {
            Session["KullaniciRefno"] = null;
        }
        }
        if (Session["KullaniciRefno"] != null)
        {
            string sql = "SELECT * FROM tbl_kullanici where id=" + Session["KullaniciRefno"].ToString() + "";

            DataTable dt = db.Fill(sql);

            Label lbl = (Label)this.form1.FindControl("lblBaslik");

            lbl.Text="Merhaba, "+ dt.Rows[0]["kullanici_adi"].ToString();

            Session["activeUser"] = dt.Rows[0]["kullanici_adi"].ToString();

            string sqlKategori = "SELECT * FROM tbl_admin_menu where ACIKLAMA=''";

            DataTable dtKat = db.Fill(sqlKategori);

            Panel pnlwrap = (Panel)this.form1.FindControl("pnlwrap");

            pnlwrap.Controls.Add(new LiteralControl("<li><a href='Default.aspx'>ANASAYFA</a></li>"));

            for (int i = 0; i < dtKat.Rows.Count; i++)
            {

                string sqlDetay = "SELECT * FROM tbl_admin_menu WHERE ANA_DUZEY=" + dtKat.Rows[i]["ANA_DUZEY"].ToString() + " AND ALT_DUZEY_1<>0 ORDER BY ALT_DUZEY_1 ASC,ALT_DUZEY_2 ASC";

                DataTable dtDetay = db.Fill(sqlDetay);

                pnlwrap.Controls.Add(new LiteralControl("<li><a href='#'>" + dtKat.Rows[i]["KATEGORI_ADI"].ToString() + "</a><ul>"));

                for (int j = 0; j < dtDetay.Rows.Count; j++)
                {
                    pnlwrap.Controls.Add(new LiteralControl("<li><a href='" + dtDetay.Rows[j]["ACIKLAMA"].ToString() + "?refno=" + dtDetay.Rows[j]["ID"].ToString() + "'>" + dtDetay.Rows[j]["KATEGORI_ADI"].ToString() + "</a></li>"));
                }

                pnlwrap.Controls.Add(new LiteralControl("</ul>"));

            }

            
        }
        else
        {
            Response.Redirect("Default.aspx");
        }
    }
}
