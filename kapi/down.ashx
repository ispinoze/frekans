﻿<%@ WebHandler Language="C#" Class="down" %>

using System;
using System.Web;
using System.IO;

public class down : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{
    
    public void ProcessRequest (HttpContext context) {

        string fileName, filePath;

        if (context.Request.QueryString["fileName"]==null)
        {
            return;
        }
        
        else
        {
            if (context.Request.QueryString["type"]=="xls")
            {
                filePath = context.Server.MapPath(@"xlsFiles\");
            }
            else
            {
                filePath = context.Server.MapPath(@"UploadFiles\");
            }
        }
        
        fileName = context.Request.QueryString["fileName"].ToString();
        
        // Check to see if file exist
        FileInfo fi = new FileInfo(filePath+fileName);
 
        // If the file exist on the server then add it to the database
        if (fi.Exists)
        {
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment; filename=" + fi.Name);
            HttpContext.Current.Response.AppendHeader("Content-Length", fi.Length.ToString());
            HttpContext.Current.Response.ContentType = "application/octet-stream";
            HttpContext.Current.Response.TransmitFile(fi.FullName);
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();  
        }
        else
        {
            context.Response.Write("Dosya Bulunmadı");
            string refno = context.Session["refno"].ToString(); ;
            context.Response.Write("<br/><br/><a href='projeDetay.aspx?refno="+refno+"'>Geri Dön</a>");
            
        }
        
    }
    private byte[] ReadByteArryFromFile(string destPath)
    {
        byte[] buff = null;
        FileStream fs = new FileStream(destPath, FileMode.Open, FileAccess.Read);
        BinaryReader br = new BinaryReader(fs);
        long numBytes = new FileInfo(destPath).Length;
        buff = br.ReadBytes((int)numBytes);
        return buff;
    }

 
    public bool IsReusable {
        get {
            return false;
        }
    }

}