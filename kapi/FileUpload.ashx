﻿<%@ WebHandler Language="C#" Class="FileUpload" %>

using System;
using System.Web;
using System.IO;

public class FileUpload : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{
    public void ProcessRequest(HttpContext context)
    {
        try
        {
            
            if (context.Request.QueryString["path"] != null && context.Request.QueryString["file"] != null)
            {
                //for deleting existing File by file name
                string Serverpath = System.Configuration.ConfigurationManager.AppSettings["FolderPath"];

                string filename = context.Request.QueryString["file"].ToString();

               Serverpath = context.Server.MapPath(Serverpath) + filename;

                if (File.Exists(Serverpath))
                {
                    File.Delete(Serverpath);
                }
            }

            else if (context.Request.QueryString["filepath"] != null && context.Request.QueryString["file"] != null)
            {
                //for downloading existing File
                
                string filepath = context.Request.QueryString["filepath"].ToString();
                
                string file = context.Request.QueryString["file"].ToString();

                if (File.Exists(filepath + "\\" + file))
                {
                    context.Response.Clear();
                    context.Response.ContentType = "application/octet-stream";
                    context.Response.AddHeader("Content-Disposition", string.Format("attachment; filename=\"{0}\"", file));
                    context.Response.WriteFile(filepath + "\\" + file);
                    context.Response.Flush();
                }

            }
            else
            {
                //for uploading new File
                
                string Serverpath = System.Configuration.ConfigurationManager.AppSettings["FolderPath"];
                string pin = context.Request.QueryString["pin"].ToString();
                var postedFile = context.Request.Files[0];
                
                    // Get Server Folder to upload file
                    //string foldername = context.Request.QueryString["id"].ToString();
                    
                    string Savepath = context.Server.MapPath(Serverpath);
                    string file;

                    //For IE to get file name
                    if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE")
                    {
                        string[] files = postedFile.FileName.Split(new char[] { '\\' });
                        file = files[files.Length - 1];

                    }
                    //For Other Browser to get file name
                    else
                    {
                        file = postedFile.FileName;
                    }

                    if (!Directory.Exists(Savepath))
                        Directory.CreateDirectory(Savepath);
                    
                    string test =Path.GetExtension(Savepath+file);

                    string file2 = file.Substring(0,file.IndexOf(test));

                    //string fileDirectory = Savepath + "\\" + file+"_"+HttpContext.Current.Session["refno"].ToString()+".jpg";   
                    pin = pin.TrimStart('\'');
                    pin = pin.TrimEnd('\'');
                    file2 = file2 + "[" + HttpContext.Current.Session["refno"].ToString()+"]"+pin+"" + test; 
                
                    string fileDirectory = Savepath + "\\" + file2;

                    postedFile.SaveAs(fileDirectory);
                    
                    string msg = "{";
                    msg += string.Format("error:'{0}',\n", string.Empty);
                    msg += string.Format("upfile:'{0}'\n", file2);
                    msg += "}";
                    
                    context.Response.Write(msg);
            }
        }
        catch (Exception ex)
        {
            
            context.Response.Write("Error: " + ex.Message);
        }
    }



    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}