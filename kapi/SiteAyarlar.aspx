﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Kapi/AdminMasterPage.master" AutoEventWireup="true" CodeFile="SiteAyarlar.aspx.cs" Inherits="Admin_GenelAyarlar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="js/General.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script type="text/javascript">

        $(document).ready(function () {

            $("input[type='button'][value='Kaydet']").click(function () {

                $.ajax({
                    type: "POST",
                    url: "GenelAyarlar.aspx/Doldur",
                    data: "{sayfaBasligi:'" + $("input[type='text']:eq(0)").val() + "',Aciklama:'" + $("input[type='text']:eq(1)").val() + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {

                    }
                });
            });
        });

    </script>


<table class="icerikTablo">
        <tr>
            <td align="center" colspan="2">
                <br />
                Site Ayarları<br />
            </td>
        </tr>
        <tr>
            <td>
                Site Başlığı</td>
            <td>
            
                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Site Açıklaması</td>
            <td>
            
                <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Anahtar Kelimeler</td>
            <td>
            
                <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Site Simgesi</td>
            <td>
            
                <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td valign="top">
                Meta-taglar</td>
            <td>
            
                <asp:TextBox ID="TextBox5" runat="server" Height="250px" TextMode="MultiLine" 
                    Width="450px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td valign="top" colspan="2" align="center">
             <div>
            <input id="btnKaydet" type="button" value="Kaydet" />
            <input type="button" value="Cancel" />
             </div>
             
             </td>
        </tr>
        </table>



</asp:Content>

