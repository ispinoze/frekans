﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;

public partial class Default3 : System.Web.UI.Page
{

    Stream_Image_Resize img = new Stream_Image_Resize();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request["p"]!=null)
        {
            string path = Request["p"].ToString();

            int width = Convert.ToInt32(Request["w"]);

            string curFile = HttpContext.Current.Request.PhysicalApplicationPath + path + "";
                        
            if (File.Exists(curFile))
            {
                Bitmap bmp = new Bitmap(HttpContext.Current.Request.PhysicalApplicationPath + path);

                img.boyutlandir(bmp, width); 
            }

        }
    }

}