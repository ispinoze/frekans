﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using System.Net.Mail;
using System.Net;
using DbClass;

public partial class _Default : System.Web.UI.Page
{


    DbClassMysql db = new DbClassMysql();

    mailGonder mg = new mailGonder();


    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["KullaniciRefno"] != null)
        {
            Response.Redirect("Login.aspx");
        }



    }

    [WebMethod]
    public static string Giris(string kullaniciAdi,string pass)
    {
        
        string donus = "";

        DataTable dt;

        try
        {
            if (kullaniciAdi != "" && pass != "")
            {
                string sql = "SELECT * FROM tbl_kullanici WHERE kullanici_adi='" + kullaniciAdi + "' AND sifre='" + pass + "'";


                dt = DbClassMysql.Fill2(sql);


                if (dt.Rows.Count != 0)
                {

                    donus = dt.Rows[0]["kullanici_adi"].ToString() + "|" + dt.Rows[0]["yetki"].ToString()+"|"+dt.Rows[0]["musteri_adi"].ToString();

                    
                }

                HttpContext.Current.Session["KullaniciRefno"]=dt.Rows[0]["id"].ToString();
                
                
                
                HttpContext.Current.Session["yetki"] = dt.Rows[0]["yetki"].ToString();
            }
        }
        catch (Exception)
        {

            donus = "";
        }


        return donus;

    }

    [WebMethod]
    public static void Abandon()
    {

        HttpContext.Current.Session.Abandon();

        

    }

    [WebMethod]
    public static bool Sorgula(string kullaniciAdi)
    {
        bool donus = false;

        DataTable dt;

        try
        {
            if (kullaniciAdi != "")
            {
                string sql = "SELECT * FROM tbl_kullanici WHERE kullanici_adi='" + kullaniciAdi + "' OR eposta='" + kullaniciAdi + "'";


                dt = DbClassMysql.Fill2(sql);

                if (dt.Rows.Count != 0)
                {
                    string kAdi=dt.Rows[0]["kullanici_adi"].ToString();

                    string sifre = dt.Rows[0]["SIFRE"].ToString();

                    donus = true;

                    mailGonder.MailSentStatic("Kullanıcı Adı :"+kAdi+"<br/> Şifre :"+sifre+"","Bilgi", "Bilgi Mailidir", "");

                }

            }
        }
        catch (Exception)
        {

            donus = false;
        }


        return donus;

    }

}