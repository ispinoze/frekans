﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OleDb;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Services;
using System.Collections;
using DbClass;
using System.Data.SqlClient;


public partial class Yonetim_Yazi : System.Web.UI.Page
{

    DbClassMysql db = new DbClassMysql();

    protected void Page_Load(object sender, EventArgs e)
    {


        if (!IsPostBack)
        {

        string[] dosyalar = System.IO.Directory.GetFiles(HttpContext.Current.Request.PhysicalApplicationPath + "/");

        ArrayList ar = new ArrayList();

        

        for (int i = 0; i < dosyalar.Length; i++)
        {

            System.IO.FileInfo dosyaAdi2 = new System.IO.FileInfo(dosyalar[i].ToString());

            if (dosyaAdi2.Extension==".aspx")
            {
                ar.Add(dosyaAdi2.Name);
            }
        }

        ddlSayfa.DataSource = ar;

        ddlSayfa.DataBind();
            

            if (Convert.ToBoolean(Session["ADMIN_GIRIS"]))
            {

                string sql = "SELECT * FROM tbl_sitemap ORDER BY ID DESC";

                DataTable dt = db.Fill(sql);

                GridView1.DataSource = dt;
                GridView1.DataBind();

                for (int i = 0; i < GridView1.Rows.Count; i++)
                {
                    Image img = (Image)GridView1.Rows[i].FindControl("Image1");

                    string sqlDurum = "SELECT DURUM FROM tbl_sitemap WHERE ID="+dt.Rows[i]["ID"].ToString();

                    bool durum = Convert.ToBoolean(db.ScalarQuery(sqlDurum));

                    if (durum)
                    {
                        img.ImageUrl = "~/Kapi/images/icons/ok.png";
                    }
                    else
                    {
                        img.ImageUrl = "~/Kapi/images/icons/icons/ok_not.png";
                    }
                }

                Panel1.Visible = true;
                Panel2.Visible = false;
                
            }
            else
            {
                Response.Redirect("Default.aspx");
            } 
        }
        string sqlBilgi = "SELECT * FROM tbl_bilgi where SAYFA_REFNO=" + Request["refno"].ToString() + "";

        DataTable dtBilgi = db.Fill(sqlBilgi);

        string bilgi = "";

        if (dtBilgi.Rows.Count != 0)
        {
            bilgi = dtBilgi.Rows[0]["ACIKLAMA"].ToString();

            bilgi = bilgi.Replace('\'', ' ');
        }
        else
        {
            bilgi = "Yardım içeriği bulunmamaktadır.";
        }

        pnlBilgi.Controls.Add(new LiteralControl("<a class='cep' title='" + bilgi + "' href='#'><img src='images/image/soru_32x.png' style='border:0; vertical-align:middle' alt='Soru' /></a>")); 
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        Label14.Text = "Sitemap İçeriğini Güncelle";
        string haberRefNo = GridView1.SelectedDataKey.Value.ToString();

        string sql = "SELECT * FROM tbl_sitemap WHERE ID="+haberRefNo;

        DataTable dt = db.Fill(sql);


        DropDownList2.SelectedValue = dt.Rows[0]["ONCELIK"].ToString();
        DropDownList1.SelectedValue = dt.Rows[0]["YENILEME_PERIYOD"].ToString();
        CheckBox2.Checked=Convert.ToBoolean(dt.Rows[0]["DURUM"]);
        lblRef.Text = dt.Rows[0]["ID"].ToString();
        ddlSayfa.SelectedValue = dt.Rows[0]["SAYFA_ADI"].ToString();
        

        Panel1.Visible = false;
        Panel2.Visible = true;

    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        if (DropDownList1.SelectedValue=="Güncelleştirme Periyodu")
        {

            Page.ClientScript.RegisterStartupScript(this.GetType(), "Mesaj", "Uyari('Güncelleştirme periyodu seçilmedi.', 'info', 'Bilgi');", true);

            return;
        }
        else if (DropDownList2.SelectedValue=="Öncelik Belirleyiniz.")
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Mesaj", "Uyari('Öncelik seçilmedi.', 'info', 'Bilgi');", true);
            return;
        }



        string sql = "";

            SqlParameter prm1 = new SqlParameter("@P1", ddlSayfa.SelectedValue);

            SqlParameter prm2 = new SqlParameter("@P2",DropDownList1.SelectedValue);

            SqlParameter prm3 = new SqlParameter("@P3", CheckBox2.Checked);

            SqlParameter prm4 = new SqlParameter("@P4", DropDownList2.SelectedValue.Replace(',', '.'));

            SqlParameter prm5 = new SqlParameter("@P5", lblRef.Text);


            if (lblRef.Text != "")
        {


            sql = "UPDATE tbl_sitemap SET SAYFA_ADI=@P1,YENILEME_PERIYOD=@P2,DURUM=@P3,ONCELIK=@P4 WHERE ID=@P5";

            db.ExecuteNonQuery(sql, prm1, prm2, prm3, prm4, prm5);
                
        }

        else
        {
            sql = "INSERT INTO tbl_sitemap(SAYFA_ADI,YENILEME_PERIYOD,DURUM,ONCELIK) VALUES(@P1,@P2,@P3,@P4)";

            db.ExecuteNonQuery(sql, prm1, prm2, prm3, prm4);

        }

        string sql1 = "SELECT * FROM tbl_sitemap ORDER BY ID DESC";
        DataTable dt1 = db.Fill(sql1);

        GridView1.DataSource = dt1;
        GridView1.DataBind();

        int page = 0;

        for (int i = 0; i < GridView1.Rows.Count; i++)
        {
            Image img = (Image)GridView1.Rows[i].FindControl("Image1");

            string sqlDurum = "SELECT DURUM FROM tbl_sitemap WHERE ID=" + dt1.Rows[i]["ID"].ToString();

            bool durum = Convert.ToBoolean(db.ScalarQuery(sqlDurum));

            if (durum)
            {
                img.ImageUrl = "~/Kapi/images/icons/ok.png";
            }
            else
            {
                img.ImageUrl = "~/Kapi/images/icons/ok_not.png";
            }
        }

        Panel1.Visible = true;
        Panel2.Visible = false;

    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        Panel1.Visible = true;
        Panel2.Visible = false;
    }

    protected void Button5_Click(object sender, EventArgs e)
    {
        string yaziRefNo = GridView1.SelectedDataKey.Value.ToString();

        string sql = "DELETE FROM tbl_sitemap WHERE ID="+yaziRefNo;

        db.ExecuteNonQuery(sql);

        string sql1 = "SELECT * FROM tbl_sitemap ORDER BY ID DESC";
        DataTable dt1 = db.Fill(sql1);

        GridView1.DataSource = dt1;
        GridView1.DataBind();


        for (int i = 0; i < GridView1.Rows.Count; i++)
        {
            Image img = (Image)GridView1.Rows[i].FindControl("Image1");

            string sqlDurum = "SELECT DURUM FROM tbl_sitemap WHERE ID=" + dt1.Rows[i]["ID"].ToString();

            bool durum = Convert.ToBoolean(db.ScalarQuery(sqlDurum));

            if (durum)
            {
                img.ImageUrl = "~/Kapi/images/icons/ok.png";
            }
            else
            {
                img.ImageUrl = "~/Kapi/images/icons/ok_not.png";
            }
        }

        Panel1.Visible = true;
        Panel2.Visible = false;

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        Label14.Text = "Yeni Sitemap İçeriği Ekle";
        DropDownList2.SelectedIndex = -1;
        DropDownList1.SelectedIndex = -1;
        ddlSayfa.SelectedIndex = -1;
        CheckBox2.Checked = false;
        lblRef.Text = "";
        Panel2.Visible = true;
        Panel1.Visible = false;
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string yaziRefNo = GridView1.DataKeys[e.RowIndex].Values[0].ToString();


        string sql = "DELETE FROM tbl_sitemap WHERE ID=" + yaziRefNo;

        db.ExecuteNonQuery(sql);

        string sql1 = "SELECT * FROM tbl_sitemap ORDER BY ID DESC";
        DataTable dt1 = db.Fill(sql1);

        GridView1.DataSource = dt1;
        GridView1.DataBind();

        int page = 0;

        if (dt1.Rows.Count < 10)
        {
            page = dt1.Rows.Count;
        }
        else
        {
            page = 10;
        }

        for (int i = 0; i < dt1.Rows.Count; i++)
        {
            Image img = (Image)GridView1.Rows[i].FindControl("Image1");

            string sqlDurum = "SELECT DURUM FROM tbl_sitemap WHERE ID=" + dt1.Rows[i]["ID"].ToString();

            bool durum = Convert.ToBoolean(db.ScalarQuery(sqlDurum));

            if (durum)
            {
                img.ImageUrl = "~/Kapi/images/icons/ok.png";
            }
            else
            {
                img.ImageUrl = "~/Kapi/images/icons/ok_not.png";
            }
        }

        Panel1.Visible = true;
        Panel2.Visible = false;
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;

        string sql1 = "SELECT * FROM tbl_sitemap ORDER BY ID DESC";
        DataTable dt1 = db.Fill(sql1);

        GridView1.DataSource = dt1;
        GridView1.DataBind();



        int i = 0;

        int sayfa = 0;

        if (GridView1.PageIndex == 0)
        {


            i = sayfa;
        }
        else if (GridView1.PageIndex == 1)
        {
            sayfa = 10;

        }
        else
        {
            sayfa = 10;

            sayfa *= GridView1.PageIndex;


        }
     
        for (; i < GridView1.Rows.Count; i++)
        {
            Image img = (Image)GridView1.Rows[i].FindControl("Image1");

            bool durum = Convert.ToBoolean(dt1.Rows[sayfa]["DURUM"]);

            if (durum)
            {
                img.ImageUrl = "~/Kapi/images/icons/ok.png";
            }
            else
            {
                img.ImageUrl = "~/Kapi/images/icons/ok_not.png";
            }
            sayfa++;
        }

        Panel1.Visible = true;
        Panel2.Visible = false;
    }

    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        for (int j = 0; j < GridView1.Rows.Count; j++)
        {
            CheckBox chk = GridView1.Rows[j].FindControl("CheckBox1") as CheckBox;

            if (chk.Checked == true)
            {
                string sql = "DELETE  FROM tbl_sitemap WHERE ID=" + chk.ToolTip + "";
                db.ExecuteNonQuery(sql);
            }
        }

        string sql1 = "SELECT * FROM tbl_sitemap ORDER BY ID DESC";
        DataTable dt1 = db.Fill(sql1);

        GridView1.DataSource = dt1;
        GridView1.DataBind();

        for (int i = 0; i < GridView1.Rows.Count; i++)
        {
            Image img = (Image)GridView1.Rows[i].FindControl("Image1");

            string sqlDurum = "SELECT DURUM FROM tbl_sitemap WHERE ID=" + dt1.Rows[i]["ID"].ToString();

            bool durum = Convert.ToBoolean(db.ScalarQuery(sqlDurum));

            if (durum)
            {
                img.ImageUrl = "~/Kapi/images/icons/ok.png";
            }
            else
            {
                img.ImageUrl = "~/Kapi/images/icons/ok_not.png";
            }
        }

        Panel1.Visible = true;
        Panel2.Visible = false;
    }

    

    
}