﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OleDb;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Services;
using DbClass;
using System.IO;
using System.Data.SqlClient;


public partial class Yonetim_Yazi : System.Web.UI.Page
{

    DbClassMysql db = new DbClassMysql();

    protected void Page_Load(object sender, EventArgs e)
    {
        pnlMus.Visible = false;

        if (!IsPostBack)
        {
            string sqlYetki = "SELECT * FROM tbl_yetki";

            DataTable dtYetki = db.Fill(sqlYetki);

            DropDownList1.DataSource = dtYetki;
            DropDownList1.DataValueField = "id";
            DropDownList1.DataTextField = "yetki_adi";
            DropDownList1.DataBind();


                string sql = "SELECT * FROM tbl_kullanici";

                DataTable dt = db.Fill(sql);

                GridView1.DataSource = dt;
                GridView1.DataBind();

                for (int i = 0; i < GridView1.Rows.Count; i++)
                {
                    Image img = (Image)GridView1.Rows[i].FindControl("Image1");

                    string sqlDurum = "SELECT durum FROM tbl_kullanici WHERE id="+dt.Rows[i]["id"].ToString();

                    bool durum = Convert.ToBoolean(db.ScalarQuery(sqlDurum));

                    if (durum)
                    {
                        img.ImageUrl = "~/Kapi/images/icons/ok.png";
                    }
                    else
                    {
                        img.ImageUrl = "~/Kapi/images/icons/icons/ok_not.png";
                    }
                }

                Panel1.Visible = true;
                Panel2.Visible = false;
                lblBaslik.Text = "Kullanıcı ve Müşteri Bilgileri";
            

            
        }

        string sqlBilgi = "SELECT * FROM tbl_bilgi where SAYFA_REFNO=" + Request["refno"].ToString() + "";

        DataTable dtBilgi = db.Fill(sqlBilgi);

        string bilgi = "";

        if (dtBilgi.Rows.Count != 0)
        {
            bilgi = dtBilgi.Rows[0]["ACIKLAMA"].ToString();

            bilgi = bilgi.Replace('\'', ' ');
        }
        else
        {
            bilgi = "Yardım içeriği bulunmamaktadır.";
        }

        pnlBilgi.Controls.Add(new LiteralControl("<a class='cep' title='" + bilgi + "' href='#'><img src='images/image/soru_32x.png' style='border:0; vertical-align:middle' alt='Soru' /></a>")); 
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        Label14.Text = "Kullanıcı Bilgisi Güncelle";
        string haberRefNo = GridView1.SelectedDataKey.Value.ToString();

        string sql = "SELECT * FROM tbl_kullanici WHERE id="+haberRefNo;

        DataTable dt = db.Fill(sql);
        
        TextBox2.Text=dt.Rows[0]["sifre"].ToString();
        DropDownList1.SelectedValue = dt.Rows[0]["yetki"].ToString();

        if (DropDownList1.SelectedItem.Text=="Müşteri")
        {
            pnlMus.Visible = true;
        }

        txtFirmaAdi.Text = dt.Rows[0]["kullanici_adi"].ToString();
        CheckBox2.Checked=Convert.ToBoolean(dt.Rows[0]["durum"]);
        lblRef.Text = dt.Rows[0]["id"].ToString();
        txtMusteri.Text =dt.Rows[0]["musteri_adi"].ToString();
        Session["lflRefID"] = dt.Rows[0]["id"].ToString();
        Panel1.Visible = false;
        Panel2.Visible = true;

    }
    protected void Button3_Click(object sender, EventArgs e)
    {
    
            string sql = "";

            SqlParameter prm1 = new SqlParameter("@P1", txtFirmaAdi.Text);

            SqlParameter prm2 = new SqlParameter("@P2", TextBox2.Text);

            SqlParameter prm3 = new SqlParameter("@P3", DropDownList1.SelectedValue);

            SqlParameter prm4 = new SqlParameter("@P4", CheckBox2.Checked);

            SqlParameter prm5 = new SqlParameter("@P5", lblRef.Text);
            SqlParameter prm6 = new SqlParameter("@P6", txtMusteri.Text);


            if (lblRef.Text != "")
            {


                sql = "UPDATE tbl_kullanici SET kullanici_adi=@P1,sifre=@P2,yetki=@P3,durum=@P4,musteri_adi=@P6 WHERE id=@P5";

                db.ExecuteNonQuery(sql, prm1, prm2, prm3, prm4, prm5, prm6);

            }

            else
            {
                sql = "INSERT INTO tbl_kullanici(kullanici_adi,sifre,yetki,durum,musteri_adi) VALUES(@P1,@P2,@P3,@P4,@P6)";

                db.ExecuteNonQuery(sql, prm1, prm2, prm3, prm4, prm6);



            }

            string sql1 = "SELECT * FROM tbl_kullanici";
            DataTable dt1 = db.Fill(sql1);

            GridView1.DataSource = dt1;
            GridView1.DataBind();

            int page = 0;

            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                Image img = (Image)GridView1.Rows[i].FindControl("Image1");

                string sqlDurum = "SELECT durum FROM tbl_kullanici WHERE id=" + dt1.Rows[i]["id"].ToString();

                bool durum = Convert.ToBoolean(db.ScalarQuery(sqlDurum));

                if (durum)
                {
                    img.ImageUrl = "~/Kapi/images/icons/ok.png";
                }
                else
                {
                    img.ImageUrl = "~/Kapi/images/icons/ok_not.png";
                }
            }

            Panel1.Visible = true;
            Panel2.Visible = false;
            Response.Redirect(Request.RawUrl);

    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        Panel1.Visible = true;
        Panel2.Visible = false;
    }

    

    protected void Button5_Click(object sender, EventArgs e)
    {
        string yaziRefNo = GridView1.SelectedDataKey.Value.ToString();

        

        string sql = "DELETE FROM tbl_kullanici WHERE id=" + yaziRefNo;

        db.ExecuteNonQuery(sql);

        string sql1 = "SELECT * FROM tbl_kullanici";
        DataTable dt1 = db.Fill(sql1);

        GridView1.DataSource = dt1;
        GridView1.DataBind();


        for (int i = 0; i < GridView1.Rows.Count; i++)
        {
            Image img = (Image)GridView1.Rows[i].FindControl("Image1");

            string sqlDurum = "SELECT DURUM FROM tbl_kullanici WHERE id=" + dt1.Rows[i]["id"].ToString();

            bool durum = Convert.ToBoolean(db.ScalarQuery(sqlDurum));

            if (durum)
            {
                img.ImageUrl = "~/Kapi/images/icons/ok.png";
            }
            else
            {
                img.ImageUrl = "~/Kapi/images/icons/ok_not.png";
            }
        }

        Panel1.Visible = true;
        Panel2.Visible = false;

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        Label14.Text = "Yeni Kullanıcı veya Müşteri Ekle";

        Random rnd = new Random();
        lblHata.Text = "";
        string ranSayi = "";
        for (int i = 0; i < 7; i++)
        {
            ranSayi += rnd.Next(1, 9);
        }
        Session["lflRefID"] = null;
        
        txtFirmaAdi.Text = "";
        CheckBox2.Checked = false;

        int son = 0;    
        
        lblRef.Text = "";
        Panel2.Visible = true;
        Panel1.Visible = false;
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string yaziRefNo = GridView1.DataKeys[e.RowIndex].Values[0].ToString();
                
        string sql = "DELETE FROM tbl_kullanici WHERE id=" + yaziRefNo;

        db.ExecuteNonQuery(sql);

        string sql1 = "SELECT * FROM tbl_kullanici";
        DataTable dt1 = db.Fill(sql1);

        GridView1.DataSource = dt1;
        GridView1.DataBind();

        int page = 0;

        if (dt1.Rows.Count < 10)
        {
            page = dt1.Rows.Count;
        }
        else
        {
            page = 10;
        }

        for (int i = 0; i < dt1.Rows.Count; i++)
        {
            Image img = (Image)GridView1.Rows[i].FindControl("Image1");

            string sqlDurum = "SELECT durum FROM tbl_kullanici WHERE id=" + dt1.Rows[i]["id"].ToString();

            bool durum = Convert.ToBoolean(db.ScalarQuery(sqlDurum));

            if (durum)
            {
                img.ImageUrl = "~/Kapi/images/icons/ok.png";
            }
            else
            {
                img.ImageUrl = "~/Kapi/images/icons/ok_not.png";
            }
        }

        Panel1.Visible = true;
        Panel2.Visible = false;
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;

        string sql1 = "SELECT * FROM tbl_kullanici";
        DataTable dt1 = db.Fill(sql1);

        GridView1.DataSource = dt1;
        GridView1.DataBind();



        int i = 0;

        int sayfa = 0;

        if (GridView1.PageIndex == 0)
        {


            i = sayfa;
        }
        else if (GridView1.PageIndex == 1)
        {
            sayfa = 10;

        }
        else
        {
            sayfa = 10;

            sayfa *= GridView1.PageIndex;


        }
     
        for (; i < GridView1.Rows.Count; i++)
        {
            Image img = (Image)GridView1.Rows[i].FindControl("Image1");

            bool durum = Convert.ToBoolean(dt1.Rows[sayfa]["durum"]);

            if (durum)
            {
                img.ImageUrl = "~/Kapi/images/icons/ok.png";
            }
            else
            {
                img.ImageUrl = "~/Kapi/images/icons/ok_not.png";
            }
            sayfa++;
        }

        Panel1.Visible = true;
        Panel2.Visible = false;
    }

    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        for (int j = 0; j < GridView1.Rows.Count; j++)
        {
            CheckBox chk = GridView1.Rows[j].FindControl("CheckBox1") as CheckBox;

            if (chk.Checked == true)
            {
                

                string sql = "DELETE FROM tbl_kullanici WHERE id=" + chk.ToolTip;
                db.ExecuteNonQuery(sql);
            }
        }

        string sql1 = "SELECT * FROM tbl_kullanici";
        DataTable dt1 = db.Fill(sql1);

        GridView1.DataSource = dt1;
        GridView1.DataBind();

        for (int i = 0; i < GridView1.Rows.Count; i++)
        {
            Image img = (Image)GridView1.Rows[i].FindControl("Image1");

            string sqlDurum = "SELECT DURUM FROM tbl_kullanici WHERE id=" + dt1.Rows[i]["id"].ToString();

            bool durum = Convert.ToBoolean(db.ScalarQuery(sqlDurum));

            if (durum)
            {
                img.ImageUrl = "~/Kapi/images/icons/ok.png";
            }
            else
            {
                img.ImageUrl = "~/Kapi/images/icons/ok_not.png";
            }
        }

        Panel1.Visible = true;
        Panel2.Visible = false;

        Response.Redirect(Request.RawUrl);
    }




    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DropDownList1.SelectedItem.Text=="Müşteri")
        {
            pnlMus.Visible = true;    
        }
    }
}