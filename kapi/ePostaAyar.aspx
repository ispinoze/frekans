﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Kapi/AdminMasterPage.master" ValidateRequest="false" AutoEventWireup="true" CodeFile="ePostaAyar.aspx.cs" Inherits="Admin_GenelAyarlar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div id="dialog"></div>

    <div class="icerikKapsar">
<table class="icerikTablo">
        <tr>
                <td>
        <asp:Panel ID="pnlBilgi" runat="server"></asp:Panel>
        </td>
            <td align="center">
                <br />
                <span>E Posta Ayarları</span>&nbsp;<span style="position: relative; float:right; text-align: right;"><a href="javascript:history.go(-1)" class="geriDon" title="Bir Önceki Sayfaya Geri Dön" style="font-size: small">« Geri Dön</a></span>
                
            </td>
        </tr>
        <tr>
            <td>
                E Posta Adresi</td>
            <td>
            
                <asp:TextBox ID="txtEpostaAdresi" runat="server"></asp:TextBox>
            &nbsp;<asp:Label ID="lblID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                E Posta Şifre</td>
            <td>
            
                <asp:TextBox ID="txtEpostaSifre" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Smtp Host</td>
            <td>
            
                <asp:TextBox ID="txtSmtpHost" runat="server"></asp:TextBox>
            </td>
        </tr>
        
        <tr>
            <td>
                Smtp Port</td>
            <td>
            
                <asp:TextBox ID="txtSmtpPort" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td valign="top" colspan="2" align="center">
             <div>
                 <asp:Button ID="btnKaydet" runat="server" Text="Kaydet" CssClass="logButton" 
                     onclick="btnKaydet_Click" />
                 <asp:Button ID="Button1" runat="server" Text="Vazgeç" CssClass="logButton" onclick="Button1_Click" 
                     />       
             </div>
             
             </td>
        </tr>
        </table>

        </div>

</asp:Content>

