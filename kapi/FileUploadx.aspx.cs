﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.Services;
using System.Data.SqlClient;
using System.Net.Mail;




public partial class FileUpload : System.Web.UI.Page
{
    DbClass db = new DbClass();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["refno"] = "653448127";
        
        if (Session["refno"]!=null)
        {
            fileToUpload.Attributes.Add("onchange", "javascript:return ajaxFileUpload();");     
        }
    }
       

    [WebMethod]
    public static string GetSession(string session)
    {
        string ses = "";

        if (HttpContext.Current.Session[session]!=null)
        {
            ses=HttpContext.Current.Session[session].ToString(); 
        }

        return ses;
    }

    public static bool Kontrol(string emailaddress)
    {
        try
        {
            MailAddress m = new MailAddress(emailaddress);
            return true;
        }
        catch (FormatException)
        {
            return false;
        }
    }

    [WebMethod]
    public static string Gonder(string adi, string soyadi, string email, string cepTel,string fotoAdi)
    {
        string sonuc = "";

              
        
        if (adi=="")
        {
            sonuc = "1";
            return sonuc;
        }
        else if (soyadi == "")
        {

            sonuc = "2";

            return sonuc;

        }
        else if (email == "")
        {

            sonuc = "3";

            return sonuc;

        }
        else if (cepTel == "")
        {

            sonuc = "4";

            return sonuc;

        }
        else if (fotoAdi == "")
        {

            sonuc = "noFoto";

            return sonuc;

        }
        else
        {
            if (Kontrol(email) == false)
            {
                sonuc = "5";
                return sonuc;
            }
        }


        
        

        SqlParameter prm1 = new SqlParameter("@P1", adi);
        SqlParameter prm2 = new SqlParameter("@P2", soyadi);
        SqlParameter prm3 = new SqlParameter("@P3", email);
        SqlParameter prm4 = new SqlParameter("@P4", cepTel);
        SqlParameter prm5 = new SqlParameter("@P5", HttpContext.Current.Session["refno"].ToString());
        SqlParameter prm6 = new SqlParameter("@P6", HttpContext.Current.Request.UserHostAddress.ToString());
        

        //Kullanıcı var mı kontrol ediliyor. Varsa sadece fotoğraf kayıt edilecek.

        int sqlUserCheck = Convert.ToInt32(DbClass.ScalarQuery2("SELECT * FROM tblKisi where FB_ID=" + HttpContext.Current.Session["refno"].ToString() + ""));
        //Bu fotoğraf daha önce kayıt edildi mi?Varsa edilemeyecek
        string sqlfotoCheck = Convert.ToString(DbClass.ScalarQuery2("SELECT FOTO_ADI FROM tblFoto where FOTO_ADI='"+fotoAdi+"'"));

        if (sqlfotoCheck!="")
        {
            return sonuc = "dub";
        }

        string sqlIns = "";

        try
        {
        
        if (sqlUserCheck==0)
        {
            sqlIns = "INSERT INTO tblKisi(ADI,SOYADI,EMAIL,CEPTEL,FB_ID,IP) ";
            sqlIns += "VALUES(@P1,@P2,@P3,@P4,@P5,@P6)";

            DbClass.Fill2(sqlIns, prm1, prm2, prm3, prm4, prm5, prm6);

            string[] fotoSlice = fotoAdi.Split('|');

            
                for (int i = 0; i < fotoSlice.Length; i++)
                {
                    SqlParameter prm7 = new SqlParameter("@P7", fotoSlice[i].ToString());
                    SqlParameter prm8 = new SqlParameter("@P8", HttpContext.Current.Session["refno"].ToString());
                    SqlParameter prm9 = new SqlParameter("@P9", HttpContext.Current.Request.UserHostAddress.ToString());
                    sqlIns = "INSERT INTO tblFoto(FOTO_ADI,FB_ID,IP) VALUES(@P7,@P8,@P9)";

                    DbClass.Fill2(sqlIns, prm7, prm8, prm9);
                }
                sonuc = "ok";
        }
        else
        {
            string[] fotoSlice = fotoAdi.Split('|');

                for (int i = 0; i < fotoSlice.Length; i++)
                {
                    SqlParameter prm7 = new SqlParameter("@P7", fotoSlice[i].ToString());
                    SqlParameter prm8 = new SqlParameter("@P8", HttpContext.Current.Session["refno"].ToString());
                    SqlParameter prm9 = new SqlParameter("@P9", HttpContext.Current.Request.UserHostAddress.ToString());
                    sqlIns = "INSERT INTO tblFoto(FOTO_ADI,FB_ID,IP) VALUES(@P7,@P8,@P9)";

                    DbClass.Fill2(sqlIns, prm7, prm8, prm9);
                }

                sonuc = "ok";
        }
  
        }
        catch (Exception ex)
        {
            
            sonuc="fatal";

            SqlParameter prm1_1 = new SqlParameter("@P1_1", HttpContext.Current.Request.UserHostAddress.ToString());
            SqlParameter prm2_1 = new SqlParameter("@P2_1", HttpContext.Current.Session["refno"].ToString());

            SqlParameter prm3_1 = new SqlParameter("@P3_1", ex.Message.ToString());

            string sqlInsHata = "INSERT INTO tblHata(IP,FB_ID,HATA) ";
            sqlInsHata += "VALUES(@P1_1,@P2_1,@P3_1)";

            DbClass.Fill2(sqlInsHata, prm1_1, prm2_1, prm3_1);

            //Response.Redirect("Hata.aspx");

        }

        return sonuc;

    }

    [WebMethod]
    public static void Sil(string fotoAdi)
    {
        string Serverpath = System.Configuration.ConfigurationManager.AppSettings["FolderPath"];

        if (fotoAdi!="")
        {
            try
            {
                string[] fotoSlice = fotoAdi.Split('|');

                

                for (int i = 0; i < fotoSlice.Length; i++)
                {
                    FileInfo dosya = new FileInfo(HttpContext.Current.Server.MapPath(Serverpath) + fotoSlice[i].ToString() + ".jpg");

                    if (dosya.Exists)
                    {
                        File.Delete(HttpContext.Current.Server.MapPath(Serverpath) + fotoSlice[i].ToString());
                    }
                }
            }
            catch (Exception ex)
            {

                SqlParameter prm1_1 = new SqlParameter("@P1_1", HttpContext.Current.Request.UserHostAddress.ToString());
                SqlParameter prm2_1 = new SqlParameter("@P2_1", HttpContext.Current.Session["refno"].ToString());

                SqlParameter prm3_1 = new SqlParameter("@P3_1", ex.Message.ToString());

                string sqlInsHata = "INSERT INTO tblHata(IP,FB_ID,HATA) ";
                sqlInsHata += "VALUES(@P1_1,@P2_1,@P3_1)";

                DbClass.Fill2(sqlInsHata, prm1_1, prm2_1, prm3_1);
            } 
        }

    }

}