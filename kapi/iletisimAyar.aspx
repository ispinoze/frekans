﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Kapi/AdminMasterPage.master" ValidateRequest="false"  AutoEventWireup="true" CodeFile="iletisimAyar.aspx.cs" Inherits="Yonetim_Yazi" %>





<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">

        window.onload = function () {

            load();
        }
    
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
                          <style type="text/css">
            @import url("http://www.google.com/uds/css/gsearch.css");
            @import url("http://www.google.com/uds/solutions/localsearch/gmlocalsearch.css");
        </style>
    
    <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=AIzaSyCGOkWsx6j-Kt5Wv8IDZpgYaPNtHY4qDoM" type="text/javascript"></script>

            
         <script src="http://www.google.com/uds/solutions/localsearch/gmlocalsearch.js" type="text/javascript"></script>
  
        <script src="http://www.google.com/uds/api?file=uds.js&amp;v=1.0" type="text/javascript"></script>

      <script type="text/javascript">

          var map;




          function load() {
              if (GBrowserIsCompatible()) {
                  //Harita nesnesi
                  map = new GMap2(document.getElementById("map_canvas"));

                  map.addControl(new GSmallMapControl());

                  map.addControl(new google.maps.LocalSearch(), new GControlPosition(G_ANCHOR_BOTTOM_RIGHT, new GSize(10, 20)));

                  GSearch.setOnLoadCallback(load);
                  map.addControl(new GMenuMapTypeControl());

                  //Harita Tipi
                  map.setMapType(G_SATELLITE_MAP);


                  var dur = 0;

                  if ($('#<%=txtLatitude.ClientID %>').val()=="") {
                      //Baslangic koordinat ve zoom değerleri
                      map.setCenter(new GLatLng(38.75408327579141, 36.38671875), 5);

                      //Belirtilen koordinatlara koyulan işağretçi
                      var Koordinat = new GLatLng(38.75408327579141, 36.38671875);
                      var Isagretci = new GMarker(Koordinat);
                      map.addOverlay(Isagretci);
                      
                      //Açıklama baloncuğu eklemek
                      map.openInfoWindow(map.getCenter(),
                    document.createTextNode("Adresinizi giriniz"));
                  }
                  else {

                      //var lat = "39.91875941802741";
                      //var lng = "32.85460352897644";
                      var lat = $('#<%=txtLatitude.ClientID %>').val();
                      var lng = $('#<%=txtLongitude.ClientID %>').val();
                      var name = $('#<%=txtDescription.ClientID %>').val();
                      var zoom = $('#<%=txtLongitude0.ClientID %>').val();
                      var lat2 = lat.replace(",", ".");
                      var lng2 = lng.replace(",", ".");
                      var zoom2 = parseInt(zoom);

                      map.panTo(new GLatLng(lat2, lng2));
                      map.setCenter(new GLatLng(lat2, lng2), zoom2);
                      marker = new GMarker(map.getCenter());
                      map.addOverlay(marker);
                      dur = 1;
                      var bilgi = "";
                      if (name.indexOf('images') != -1) {
                          bilgi="<img src=\"../"+name+"\" style=\"vertical-align: middle\"/>";
                      }
                      else {
                          bilgi = name;
                        }

                                    var infoTabs = [
                                new GInfoWindowTab("",bilgi )
                                
                             ];
                      marker.openInfoWindowTabsHtml(infoTabs);

                      GEvent.addListener(marker, 'click', function () {
                          // When clicked, open an Info Window
                          marker.openInfoWindowTabsHtml(infoTabs);
                      });
                  }


                  
                  var center;
                  //Harita üzerinde çift tıklanınca çalışan olaylar
                  GEvent.addListener(map, "dblclick", function (overlay, point) {

                      //Isagretci = new GMarker(point);
                      //Eklenen isagretcinin tıklanan yere kaydırılması
                      if (dur==1) {
                          marker.setPoint(point);

                          var infoTabs = [
                                new GInfoWindowTab("", bilgi)

                             ];
                          marker.openInfoWindowTabsHtml(infoTabs);
                      }
                      else {

                          Isagretci.setPoint(point);
                          //Açıklama baloncuğu eklemek
                          map.openInfoWindow(point,
                    document.createTextNode("Adresinizi giriniz"));
                      }
                     



                      center = map.getCenter();



                      //  isteğe göre aktif / pasif edilebilir
                      //  Eğer sayfa yüklenirken datadan koordinatları çekecekseniz ilgili txt lere bu koordinatları yazdırmanız lazım ki o koordinatlara gitsin,
                      Yaz(center.lat(), center.lng(), map.getZoom());
                  });
                  map.disableDoubleClickZoom();
                
                  //Haritaya çift tıklayınca zoom yapması önleniyor.
               

              }
              function Yaz(enlem, boylam, zoom) {
                  $(document).ready(function () {

                      $('#<%=txtLatitude.ClientID %>').val(enlem);
                      $("#<%=txtLongitude.ClientID%>").val(boylam);
                      $("#<%=txtLongitude0.ClientID%>").val(zoom);

                      
                  });



              }


          }
      </script>


           <div class="icerikKapsar">
    
    <asp:Panel ID="Panel1" runat="server">
        <table>
                            <tr>
                <td style="padding-top: 20px;">
                    &nbsp;<asp:Button ID="Button1" runat="server" Text="Yeni" onclick="Button1_Click" 
                        CssClass="logButton2" /><span style="position:relative;right:0;left:580px"><a href="javascript:history.go(-1)" class="geriDon" title="Bir Önceki Sayfaya Geri Dön" style="font-size: small">« Geri Dön</a></span>
                        
                </td>
            </tr>
                    
 
            <tr>
                <td>
                    <div class="grid">
                        <div class="rounded">
                            <div class="top-outer">
                                <div class="top-inner">
                                    <div class="top">
                                        <h2 align="center">
                                            <asp:Label ID="lblBaslik" runat="server" Font-Names="Arial" Font-Size="11pt" 
                                                Text="Adres Yönetimi"></asp:Label>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                            <div class="mid-outer">
                                <div class="mid-inner">
                                    <div class="mid">
                                        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                                            AllowSorting="True" AutoGenerateColumns="False" CssClass="datatable" 
                                            DataKeyNames="ID" EnableModelValidation="True" GridLines="None" 
                                            onpageindexchanging="GridView1_PageIndexChanging" 
                                            onrowdeleting="GridView1_RowDeleting" 
                                            onselectedindexchanged="GridView1_SelectedIndexChanged" Width="840px">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Seç">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="CheckBox1" runat="server" 
                                                            ToolTip='<%# Eval("ID") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Sil" ShowHeader="False">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" 
                                                            CommandName="Delete" ImageUrl="~/Kapi/images/icons/Eraser.png" onclick="ImageButton1_Click" 
                                                            onclientclick="return confirm(&quot;Seçilen Satır Silinsin mi?&quot;)" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField ButtonType="Image" HeaderText="Düzenle" 
                                                    SelectImageUrl="~/Kapi/images/icons/Edit.png" SelectText="Düzenle" 
                                                    ShowSelectButton="True">
                                                <ControlStyle CssClass="ortala" />
                                                <ControlStyle CssClass="ortala" />
                                                <FooterStyle HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                </asp:CommandField>
                                                <asp:BoundField DataField="ADRES" HeaderText="Adres" >
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TEL_1" HeaderText="Telefon 1" />
                                                <asp:BoundField DataField="TEL_2" HeaderText="Telefon 2" />
                                                <asp:BoundField DataField="FAX" HeaderText="Fax" />
                                                <asp:BoundField DataField="EMAIL" HeaderText="E-Mail" />
                                                <asp:TemplateField HeaderText="Onay">
                                                    <ItemTemplate>
                                                        <asp:Image ID="Image1" runat="server" />
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("KURUMSAL_DURUM") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings FirstPageText="«" LastPageText="»" />
                                            <PagerStyle CssClass="pager-row" />
                                            <RowStyle CssClass="row" HorizontalAlign="Center" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                            <div class="bottom-outer">
                                <div class="bottom-inner">
                                    <div class="bottom">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel2" runat="server">
        <table class="icerikTablo">

            <tr>
                    <td>
        <asp:Panel ID="pnlBilgi" runat="server"></asp:Panel>
        </td>
                <td align="center">
                    <asp:Label ID="Label14" runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Ref No" Visible="False"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblRef" runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label19" runat="server" Text="Adres Adı"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtAdres" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Adres"></asp:Label>
                </td>
                <td>
                    
                    <asp:TextBox ID="txtYAZI_KONUSU" runat="server"></asp:TextBox>
                    
                    <br />
                    
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="txtYAZI_KONUSU" ErrorMessage="Bu Alan Boş Bırakılamaz." 
                        ValidationGroup="kaydet"></asp:RequiredFieldValidator>
                    
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label4" runat="server" Text="Telefon 1"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtTlf" runat="server"></asp:TextBox>
                    <br />
                </td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="Label17" runat="server" Text="Telefon 2"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtTlf0" runat="server" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="Label18" runat="server" Text="Açıklama"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtAciklama" runat="server" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="Label15" runat="server" Text="Fax"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtFax" runat="server" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="Label16" runat="server" Text="E-mail"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtEmail" runat="server" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="Label13" runat="server" Text="Onay"></asp:Label>
                </td>
                <td>
                    <asp:CheckBox ID="CheckBox2" runat="server" />
                </td>
            </tr>

        </table>
        <div id="Harita" 
            >
        <br />
        <div style="font-family: Arial, Helvetica, sans-serif; font-size: large" align="center">Harita Bilgisi</div>
        <br />
        <div style="width: 100%" align="center">
           <div id="map_canvas" style="width: 500px; height: 500px" align="center">  </div>     
        </div>
        <div style="margin-top: 20px">
        <table cellspacing="10px" cellpadding="20">
        <tr style="padding-top: 10px; padding-bottom: 10px"><td width="140px">Bilgi Penceresi</td><td><asp:TextBox ID="txtDescription" runat="server"></asp:TextBox>
            <asp:FileUpload ID="FileUpload1" runat="server" />
            </td></tr>
        <tr><td>Kuzey(Y)</td><td><asp:TextBox ID="txtLatitude" runat="server"></asp:TextBox></td></tr>
        <tr><td>Doğu(X)</td><td><asp:TextBox ID="txtLongitude" runat="server"></asp:TextBox></td></tr>
        <tr><td>Yaklaşma Ölçeği</td><td><asp:TextBox ID="txtLongitude0" runat="server"></asp:TextBox></td></tr>
        </table>
        
         
         
         
        </div>
        <div align="right" style="margin-bottom: 20px">
        <asp:Button ID="Button3" runat="server"  CssClass="logButton" 
                        onclick="Button3_Click" Text="Kaydet" ValidationGroup="kaydet" />
                    
                    
                    <asp:Button ID="Button4" runat="server" onclick="Button4_Click" Text="Vazgeç" 
                        CssClass="logButton" />
                    
                    
                    <asp:Button ID="Button5" runat="server"  onclick="Button5_Click" Text="Sil" 
                       CssClass="logButton" />
        </div>
        
        </div>
        
      

        
    </asp:Panel>
        <div style="clear:both"></div>
        <br />
    </div>
    <div style="clear:both"></div>
</asp:Content>

