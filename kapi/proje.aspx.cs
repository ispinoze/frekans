﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OleDb;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Services;
using DbClass;
using System.IO;
using System.Data.SqlClient;


public partial class Yonetim_Yazi : System.Web.UI.Page
{

    DbClassMysql db = new DbClassMysql();

    protected void Page_Load(object sender, EventArgs e)
    {


        if (!IsPostBack)
        {
            string sqlFirma = "select * from tbl_kullanici where musteri_adi<>''";

            DataTable dtFirma = db.Fill(sqlFirma);

            ddlFirma.DataSource = dtFirma;
            ddlFirma.DataValueField = "id";
            ddlFirma.DataTextField = "musteri_adi";
            ddlFirma.DataBind();

            if (Session["KullaniciRefno"]!=null)
            {

                string sql = "SELECT * FROM tbl_proje";

                DataTable dt = db.Fill(sql);

                GridView1.DataSource = dt;
                GridView1.DataBind();

                for (int i = 0; i < GridView1.Rows.Count; i++)
                {
                    Image img = (Image)GridView1.Rows[i].FindControl("Image1");

                    string sqlDurum = "SELECT durum FROM tbl_proje WHERE id="+dt.Rows[i]["id"].ToString();

                    bool durum = Convert.ToBoolean(db.ScalarQuery(sqlDurum));

                    

                    Label lblAralik = GridView1.Rows[i].FindControl("lblAralik") as Label;

                    /*switch (dt.Rows[i]["measure_range"].ToString())
                    {
                        case "h":
                            lblAralik.Text = "Saatlik";
                            break;
                        case "hh":
                            lblAralik.Text = "Y. Saatlik";
                            break;
                        default:
                            break;
                    }*/

                    Label lblfirma = GridView1.Rows[i].FindControl("lblFirma") as Label;

                    lblfirma.Text = Convert.ToString(db.ScalarQuery("SELECT musteri_adi from tbl_kullanici where id="+dt.Rows[i]["firma_id"].ToString()));

                    if (durum)
                    {
                        img.ImageUrl = "~/Kapi/images/icons/ok.png";
                    }
                    else
                    {
                        img.ImageUrl = "~/Kapi/images/icons/ok_not.png";
                    }
                }

                Panel1.Visible = true;
                Panel2.Visible = false;
                lblBaslik.Text = "Proje Bilgileri";
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
            
        }

        string sqlBilgi = "SELECT * FROM tbl_bilgi where SAYFA_REFNO=" + Request["refno"].ToString() + "";

        DataTable dtBilgi = db.Fill(sqlBilgi);

        string bilgi = "";

        if (dtBilgi.Rows.Count != 0)
        {
            bilgi = dtBilgi.Rows[0]["ACIKLAMA"].ToString();

            bilgi = bilgi.Replace('\'', ' ');
        }
        else
        {
            bilgi = "Yardım içeriği bulunmamaktadır.";
        }

        pnlBilgi.Controls.Add(new LiteralControl("<a class='cep' title='" + bilgi + "' href='#'><img src='images/image/soru_32x.png' style='border:0; vertical-align:middle' alt='Soru' /></a>")); 
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        Label14.Text = "Proje Bilgisi Güncelle";
        string haberRefNo = GridView1.SelectedDataKey.Value.ToString();

        string sql = "SELECT * FROM tbl_proje WHERE id="+haberRefNo;

        DataTable dt = db.Fill(sql);
        
        
        
        txtProje.Text = dt.Rows[0]["proje_adi"].ToString();
        ddlFirma.SelectedValue = dt.Rows[0]["firma_id"].ToString();
        
        CheckBox2.Checked=Convert.ToBoolean(dt.Rows[0]["durum"]);
        lblRef.Text = dt.Rows[0]["id"].ToString();
                
        Session["lflRefID"] = dt.Rows[0]["id"].ToString();
        Panel1.Visible = false;
        Panel2.Visible = true;

    }
    protected void Button3_Click(object sender, EventArgs e)
    {
    
            string sql = "";

            SqlParameter prm1 = new SqlParameter("@P1", txtProje.Text);

            SqlParameter prm2 = new SqlParameter("@P2", CheckBox2.Checked);

            SqlParameter prm3 = new SqlParameter("@P3", ddlFirma.SelectedValue);

            SqlParameter prm4 = new SqlParameter("@P4", lblRef.Text);

            SqlParameter prm5 = new SqlParameter("@P5", ddlmeasure.SelectedValue);


            if (lblRef.Text != "")
            {

                sql = "UPDATE tbl_proje SET proje_adi=@P1,durum=@P2,firma_id=@P3,measure_range=@P5 WHERE id=@P4";

                db.ExecuteNonQuery(sql, prm1, prm2, prm3, prm4, prm5);
            }

            else
            {
                sql = "INSERT INTO tbl_proje(proje_adi,durum,firma_id,measure_range) VALUES(@P1,@P2,@P3,@P5)";

                db.ExecuteNonQuery(sql, prm1, prm2, prm3, prm5);
            }

            string sql1 = "SELECT * FROM tbl_proje";
            DataTable dt1 = db.Fill(sql1);

            GridView1.DataSource = dt1;
            GridView1.DataBind();

            int page = 0;

            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                Image img = (Image)GridView1.Rows[i].FindControl("Image1");

                string sqlDurum = "SELECT durum FROM tbl_proje WHERE id=" + dt1.Rows[i]["id"].ToString();

                bool durum = Convert.ToBoolean(db.ScalarQuery(sqlDurum));

                if (durum)
                {
                    img.ImageUrl = "~/Kapi/images/icons/ok.png";
                }
                else
                {
                    img.ImageUrl = "~/Kapi/images/icons/ok_not.png";
                }
            }

            Panel1.Visible = true;
            Panel2.Visible = false;
            Response.Redirect(Request.RawUrl);

    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        Panel1.Visible = true;
        Panel2.Visible = false;
    }

    

    protected void Button5_Click(object sender, EventArgs e)
    {
        string yaziRefNo = GridView1.SelectedDataKey.Value.ToString();

        

        string sql = "DELETE FROM tbl_proje WHERE id=" + yaziRefNo;

        db.ExecuteNonQuery(sql);

        string sql1 = "SELECT * FROM tbl_proje";
        DataTable dt1 = db.Fill(sql1);

        GridView1.DataSource = dt1;
        GridView1.DataBind();


        for (int i = 0; i < GridView1.Rows.Count; i++)
        {
            Image img = (Image)GridView1.Rows[i].FindControl("Image1");

            string sqlDurum = "SELECT DURUM FROM tbl_proje WHERE id=" + dt1.Rows[i]["id"].ToString();

            bool durum = Convert.ToBoolean(db.ScalarQuery(sqlDurum));

            if (durum)
            {
                img.ImageUrl = "~/Kapi/images/icons/ok.png";
            }
            else
            {
                img.ImageUrl = "~/Kapi/images/icons/ok_not.png";
            }
        }

        Panel1.Visible = true;
        Panel2.Visible = false;

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        Label14.Text = "Yeni Proje Ekle";
        ddlFirma.SelectedIndex = -1;
        Random rnd = new Random();
        lblHata.Text = "";
        string ranSayi = "";
        for (int i = 0; i < 7; i++)
        {
            ranSayi += rnd.Next(1, 9);
        }
        Session["lflRefID"] = null;
        
        txtProje.Text = "";
        CheckBox2.Checked = false;

        int son = 0;    
        
        lblRef.Text = "";
        Panel2.Visible = true;
        Panel1.Visible = false;
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string yaziRefNo = GridView1.DataKeys[e.RowIndex].Values[0].ToString();
                
        string sql = "DELETE FROM tbl_proje WHERE id=" + yaziRefNo;

        db.ExecuteNonQuery(sql);

        string sql1 = "SELECT * FROM tbl_proje";
        DataTable dt1 = db.Fill(sql1);

        GridView1.DataSource = dt1;
        GridView1.DataBind();

        int page = 0;

        if (dt1.Rows.Count < 10)
        {
            page = dt1.Rows.Count;
        }
        else
        {
            page = 10;
        }

        for (int i = 0; i < dt1.Rows.Count; i++)
        {
            Image img = (Image)GridView1.Rows[i].FindControl("Image1");

            string sqlDurum = "SELECT durum FROM tbl_proje WHERE id=" + dt1.Rows[i]["id"].ToString();

            bool durum = Convert.ToBoolean(db.ScalarQuery(sqlDurum));

            if (durum)
            {
                img.ImageUrl = "~/Kapi/images/icons/ok.png";
            }
            else
            {
                img.ImageUrl = "~/Kapi/images/icons/ok_not.png";
            }
        }

        Panel1.Visible = true;
        Panel2.Visible = false;
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;

        string sql1 = "SELECT * FROM tbl_proje";
        DataTable dt1 = db.Fill(sql1);

        GridView1.DataSource = dt1;
        GridView1.DataBind();



        int i = 0;

        int sayfa = 0;

        if (GridView1.PageIndex == 0)
        {


            i = sayfa;
        }
        else if (GridView1.PageIndex == 1)
        {
            sayfa = 10;

        }
        else
        {
            sayfa = 10;

            sayfa *= GridView1.PageIndex;


        }
     
        for (; i < GridView1.Rows.Count; i++)
        {
            Image img = (Image)GridView1.Rows[i].FindControl("Image1");

            Label lblAralik = (Label)GridView1.Rows[i].FindControl("lblAralik");

            bool durum = Convert.ToBoolean(dt1.Rows[sayfa]["durum"]);

            switch (dt1.Rows[sayfa]["measure_range"].ToString())
            {
                case "h":
                    lblAralik.Text = "Saatlik";
                    break;
                case "hh":
                    lblAralik.Text = "Y. Saatlik";
                    break;
                default:
                    break;
            }

            Label lblfirma = GridView1.Rows[i].FindControl("lblFirma") as Label;

            lblfirma.Text = Convert.ToString(db.ScalarQuery("SELECT musteri_adi from tbl_kullanici where id=" + dt1.Rows[sayfa]["firma_id"].ToString()));

            if (durum)
            {
                img.ImageUrl = "~/Kapi/images/icons/ok.png";
            }
            else
            {
                img.ImageUrl = "~/Kapi/images/icons/ok_not.png";
            }
            sayfa++;
        }

        Panel1.Visible = true;
        Panel2.Visible = false;
    }

    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        for (int j = 0; j < GridView1.Rows.Count; j++)
        {
            CheckBox chk = GridView1.Rows[j].FindControl("CheckBox1") as CheckBox;

            if (chk.Checked == true)
            {
                

                string sql = "DELETE FROM tbl_proje WHERE id=" + chk.ToolTip;
                db.ExecuteNonQuery(sql);
            }
        }

        string sql1 = "SELECT * FROM tbl_proje";
        DataTable dt1 = db.Fill(sql1);

        GridView1.DataSource = dt1;
        GridView1.DataBind();

        for (int i = 0; i < GridView1.Rows.Count; i++)
        {
            Image img = (Image)GridView1.Rows[i].FindControl("Image1");

            string sqlDurum = "SELECT DURUM FROM tbl_proje WHERE id=" + dt1.Rows[i]["id"].ToString();

            bool durum = Convert.ToBoolean(db.ScalarQuery(sqlDurum));

            if (durum)
            {
                img.ImageUrl = "~/Kapi/images/icons/ok.png";
            }
            else
            {
                img.ImageUrl = "~/Kapi/images/icons/ok_not.png";
            }
        }

        Panel1.Visible = true;
        Panel2.Visible = false;

        Response.Redirect(Request.RawUrl);
    }

    

    
}