﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Kapi/AdminMasterPage.master" ValidateRequest="false"  AutoEventWireup="true" CodeFile="kullanici.aspx.cs" Inherits="Yonetim_Yazi" %>





<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript">
    $(document).ready(function(){
             $.datepicker.setDefaults($.datepicker.regional["tr"]);
             $(".TarihPicker").datepicker();
             $(".TarihPicker").datepicker("option", "showAnim", "slideDown");
             $(".TarihPicker").datepicker("option", "dateFormat", "dd/mm/yy");
            });
    </script>
    
        <div class="icerikKapsar">
    
    <asp:Panel ID="Panel1" runat="server">
        <table>
                            <tr>
                <td style="padding-top: 20px;">
                    &nbsp;<asp:Button ID="Button1" runat="server" Text="Yeni" onclick="Button1_Click" 
                        CssClass="logButton2"  /><span style="position:relative;right:0;left:580px"><a href="javascript:history.go(-1)" class="geriDon" title="Bir Önceki Sayfaya Geri Dön" style="font-size: small">« Geri Dön</a></span>
                        
                </td>
            </tr>
                    
 
            <tr>
                <td>
                    <div class="grid">
                        <div class="rounded">
                            <div class="top-outer">
                                <div class="top-inner">
                                    <div class="top">
                                        <h2 align="center">
                                            <asp:Label ID="lblBaslik" runat="server" Font-Names="Arial" Font-Size="11pt"></asp:Label>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                            <div class="mid-outer">
                                <div class="mid-inner">
                                    <div class="mid">
                                        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                                            AllowSorting="True" AutoGenerateColumns="False" CssClass="datatable" 
                                            DataKeyNames="ID" GridLines="None" 
                                            onpageindexchanging="GridView1_PageIndexChanging" 
                                            onrowdeleting="GridView1_RowDeleting" 
                                            onselectedindexchanged="GridView1_SelectedIndexChanged" Width="840px">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Seç">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="CheckBox1" runat="server" 
                                                            ToolTip='<%# Eval("ID") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Sil" ShowHeader="False">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" 
                                                            CommandName="Delete" ImageUrl="~/Kapi/images/icons/Eraser.png" onclick="ImageButton1_Click" 
                                                            onclientclick="return confirm(&quot;Seçilen Satır Silinsin mi?&quot;)" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField ButtonType="Image" HeaderText="Düzenle" 
                                                    SelectImageUrl="~/Kapi/images/icons//Edit.png" SelectText="Düzenle" 
                                                    ShowSelectButton="True">
                                                <ControlStyle CssClass="ortala" />
                                                <ControlStyle CssClass="ortala" />
                                                <FooterStyle HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                </asp:CommandField>
                                                <asp:BoundField DataField="kullanici_adi" HeaderText="Kullanıcı Adı" />
                                                <asp:BoundField DataField="musteri_adi" HeaderText="Müşteri Adı" />
                                                <asp:TemplateField HeaderText="Onay">
                                                    <ItemTemplate>
                                                        <asp:Image ID="Image1" runat="server" />
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("KURUMSAL_DURUM") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings FirstPageText="«" LastPageText="»" />
                                            <PagerStyle CssClass="pager-row" />
                                            <RowStyle CssClass="row" HorizontalAlign="Center" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                            <div class="bottom-outer">
                                <div class="bottom-inner">
                                    <div class="bottom">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel2" runat="server">
        <table class="icerikTablo">

            <tr>
                    <td>
        <asp:Panel ID="pnlBilgi" runat="server"></asp:Panel>
        </td>
                <td align="center">
                    <asp:Label ID="Label14" runat="server" ></asp:Label><span style="position:relative;right:0;float:right"><a href="javascript:history.go(-1)" class="geriDon" title="Bir Önceki Sayfaya Geri Dön" style="font-size: small">« Geri Dön</a></span>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Label ID="lblRef" runat="server" Visible="False"></asp:Label>
                </td>
            </tr>

            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Kullanıcı Adı"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtFirmaAdi" runat="server"></asp:TextBox>
                    &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="txtFirmaAdi" ErrorMessage="Bu alan boş bırakılmaz." 
                        ValidationGroup="kaydet"></asp:RequiredFieldValidator>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Şifre"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                    &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="TextBox2" ErrorMessage="Bu alan boş bırakılmaz." 
                        ValidationGroup="kaydet"></asp:RequiredFieldValidator>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Yetki"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList1" runat="server" Height="26px" Width="126px" 
                        AutoPostBack="True" onselectedindexchanged="DropDownList1_SelectedIndexChanged">
                        <asp:ListItem></asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ControlToValidate="DropDownList1" ErrorMessage="Bu alan boş bırakılmaz." 
                        ValidationGroup="kaydet"></asp:RequiredFieldValidator>
                    <br />
                </td>
            </tr>

            <tr>
                <asp:Panel ID="pnlMus" runat="server" Visible="False">
    
                <td>
                    Müşteri Adı</td>
                <td>
                    <asp:TextBox ID="txtMusteri" runat="server"></asp:TextBox>
                </td>
               </asp:Panel>
            </tr>

            <tr>
                <td class="style2">
                    <asp:Label ID="Label13" runat="server" Text="Onay"></asp:Label>
                </td>
                <td>
                    <asp:CheckBox ID="CheckBox2" runat="server" />
                </td>
            </tr>

            <tr>
                <td class="style2">
                    &nbsp;</td>
                <td>
                    <asp:Label ID="lblHata" runat="server" ForeColor="#CC0000"></asp:Label>
                </td>
            </tr>

            <tr>
                <td colspan="2" align="left">
                    <asp:Button ID="Button3" runat="server" CssClass="logButton" 
                        onclick="Button3_Click" Text="Kaydet" ValidationGroup="kaydet" 
                        EnableViewState="False" />
                    <asp:Button ID="Button4" runat="server" CssClass="logButton" 
                        onclick="Button4_Click" Text="Vazgeç" />
                    <asp:Button ID="Button5" runat="server" CssClass="logButton" 
                        onclick="Button5_Click" Text="Sil" />
                </td>
            </tr>

        </table>
             
    </asp:Panel>
    </div>
</asp:Content>

